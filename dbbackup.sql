-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: localhost    Database: ultimateloanmanager
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `ultimateloanmanager`
--



USE `ultimateloanmanager`;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint unsigned DEFAULT NULL,
  `subject_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint unsigned DEFAULT NULL,
  `causer_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `activity_log_log_name_index` (`log_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'default','Create User',2,'Modules\\User\\Entities\\User',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 08:56:52','2021-02-26 08:56:52'),(2,'default','Create Currency',2,'Modules\\Core\\Entities\\Currency',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 08:57:25','2021-02-26 08:57:25'),(3,'default','Update Currency',1,'Modules\\Core\\Entities\\Currency',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 08:57:37','2021-02-26 08:57:37'),(4,'default','Delete Currency',1,'Modules\\Core\\Entities\\Currency',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 08:57:42','2021-02-26 08:57:42'),(5,'default','Create Branch',2,'Modules\\Branch\\Entities\\Branch',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 12:27:05','2021-02-26 12:27:05'),(6,'default','Update Branch',1,'Modules\\Branch\\Entities\\Branch',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 12:27:50','2021-02-26 12:27:50'),(7,'default','Update Branch',1,'Modules\\Branch\\Entities\\Branch',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 12:27:51','2021-02-26 12:27:51'),(8,'default','Create Client',1,'Modules\\Client\\Entities\\Client',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 12:39:32','2021-02-26 12:39:32'),(9,'default','Update Client Status',1,'Modules\\Client\\Entities\\Client',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 12:40:19','2021-02-26 12:40:19'),(10,'default','Update Client',1,'Modules\\Client\\Entities\\Client',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 12:44:42','2021-02-26 12:44:42'),(11,'default','Create Fund',1,'Modules\\Loan\\Entities\\Fund',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 12:59:19','2021-02-26 12:59:19'),(12,'default','Update Fund',1,'Modules\\Loan\\Entities\\Fund',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 12:59:33','2021-02-26 12:59:33'),(13,'default','Create Client Type',1,'Modules\\Client\\Entities\\ClientType',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:03:01','2021-02-26 13:03:01'),(14,'default','Create Client Type',2,'Modules\\Client\\Entities\\ClientType',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:03:13','2021-02-26 13:03:13'),(15,'default','Create Client Type',3,'Modules\\Client\\Entities\\ClientType',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:03:27','2021-02-26 13:03:27'),(16,'default','Create Client Type',4,'Modules\\Client\\Entities\\ClientType',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:03:38','2021-02-26 13:03:38'),(17,'default','Create Client Title',1,'Modules\\Client\\Entities\\Title',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:04:00','2021-02-26 13:04:00'),(18,'default','Create Client Title',2,'Modules\\Client\\Entities\\Title',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:04:07','2021-02-26 13:04:07'),(19,'default','Create Client Title',3,'Modules\\Client\\Entities\\Title',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:04:19','2021-02-26 13:04:19'),(20,'default','Create Client Title',4,'Modules\\Client\\Entities\\Title',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:04:30','2021-02-26 13:04:30'),(21,'default','Create Client Title',5,'Modules\\Client\\Entities\\Title',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:04:38','2021-02-26 13:04:38'),(22,'default','Create Profession',1,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:05:05','2021-02-26 13:05:05'),(23,'default','Create Profession',2,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:05:18','2021-02-26 13:05:18'),(24,'default','Create Profession',3,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:05:26','2021-02-26 13:05:26'),(25,'default','Create Profession',4,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:05:39','2021-02-26 13:05:39'),(26,'default','Create Profession',5,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:05:54','2021-02-26 13:05:54'),(27,'default','Create Profession',6,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-02-26 13:06:17','2021-02-26 13:06:17'),(28,'default','Create Profession',7,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 13:06:34','2021-02-26 13:06:34'),(29,'default','Create Profession',8,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":8}','2021-02-26 13:06:44','2021-02-26 13:06:44'),(30,'default','Create Profession',9,'Modules\\Client\\Entities\\Profession',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 13:06:58','2021-02-26 13:06:58'),(31,'default','Create Client Identification Type',1,'Modules\\Client\\Entities\\ClientIdentificationType',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:08:00','2021-02-26 13:08:00'),(32,'default','Create Client Identification Type',2,'Modules\\Client\\Entities\\ClientIdentificationType',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:08:10','2021-02-26 13:08:10'),(33,'default','Create Loan Collateral Type',1,'Modules\\Loan\\Entities\\LoanCollateralType',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:09:36','2021-02-26 13:09:36'),(34,'default','Create Loan Collateral Type',2,'Modules\\Loan\\Entities\\LoanCollateralType',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:09:51','2021-02-26 13:09:51'),(35,'default','Create Loan Collateral Type',3,'Modules\\Loan\\Entities\\LoanCollateralType',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:09:59','2021-02-26 13:09:59'),(36,'default','Create Loan Collateral Type',4,'Modules\\Loan\\Entities\\LoanCollateralType',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:10:13','2021-02-26 13:10:13'),(37,'default','Create Loan Collateral Type',5,'Modules\\Loan\\Entities\\LoanCollateralType',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:10:25','2021-02-26 13:10:25'),(38,'default','Create Loan Collateral Type',6,'Modules\\Loan\\Entities\\LoanCollateralType',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-02-26 13:10:35','2021-02-26 13:10:35'),(39,'default','Create Loan Collateral Type',7,'Modules\\Loan\\Entities\\LoanCollateralType',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 13:11:38','2021-02-26 13:11:38'),(40,'default','Create Loan Purpose',1,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:12:53','2021-02-26 13:12:53'),(41,'default','Create Loan Purpose',2,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:13:07','2021-02-26 13:13:07'),(42,'default','Create Loan Purpose',3,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:13:16','2021-02-26 13:13:16'),(43,'default','Create Loan Purpose',4,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:13:25','2021-02-26 13:13:25'),(44,'default','Create Loan Purpose',5,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:13:33','2021-02-26 13:13:33'),(45,'default','Create Loan Purpose',6,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-02-26 13:13:51','2021-02-26 13:13:51'),(46,'default','Create Loan Purpose',7,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 13:14:10','2021-02-26 13:14:10'),(47,'default','Create Loan Purpose',8,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":8}','2021-02-26 13:15:46','2021-02-26 13:15:46'),(48,'default','Create Loan Purpose',9,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 13:15:55','2021-02-26 13:15:55'),(49,'default','Create Loan Purpose',10,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":10}','2021-02-26 13:15:56','2021-02-26 13:15:56'),(50,'default','Create Loan Purpose',11,'Modules\\Loan\\Entities\\LoanPurpose',2,'Modules\\User\\Entities\\User','{\"id\":11}','2021-02-26 13:15:57','2021-02-26 13:15:57'),(51,'default','Create Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:17:09','2021-02-26 13:17:09'),(52,'default','Create Loan Product',2,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:20:52','2021-02-26 13:20:52'),(53,'default','Update Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:21:16','2021-02-26 13:21:16'),(54,'default','Create Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:24:02','2021-02-26 13:24:02'),(55,'default','Create Loan Charge',2,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:25:52','2021-02-26 13:25:52'),(56,'default','Create Loan Charge',3,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:26:50','2021-02-26 13:26:50'),(57,'default','Create Loan Charge',4,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:27:22','2021-02-26 13:27:22'),(58,'default','Create Loan Charge',5,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:27:48','2021-02-26 13:27:48'),(59,'default','Create Loan Charge',6,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-02-26 13:28:14','2021-02-26 13:28:14'),(60,'default','Create Loan Product',3,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:34:15','2021-02-26 13:34:15'),(61,'default','Update Loan Product',2,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:34:46','2021-02-26 13:34:46'),(62,'default','Update Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:35:17','2021-02-26 13:35:17'),(63,'default','Create Loan Product',4,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:37:20','2021-02-26 13:37:20'),(64,'default','Create Loan Product',5,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:41:03','2021-02-26 13:41:03'),(65,'default','Update Loan Product',5,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:42:39','2021-02-26 13:42:39'),(66,'default','Create Loan Product',6,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-02-26 13:47:08','2021-02-26 13:47:08'),(67,'default','Update Loan Product',5,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:47:23','2021-02-26 13:47:23'),(68,'default','Update Loan Product',4,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:47:34','2021-02-26 13:47:34'),(69,'default','Update Loan Product',3,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:47:47','2021-02-26 13:47:47'),(70,'default','Update Loan Product',2,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:48:02','2021-02-26 13:48:02'),(71,'default','Update Loan Product',2,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:48:32','2021-02-26 13:48:32'),(72,'default','Update Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:48:48','2021-02-26 13:48:48'),(73,'default','Update Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:49:00','2021-02-26 13:49:00'),(74,'default','Update Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 13:49:25','2021-02-26 13:49:25'),(75,'default','Update Loan Product',2,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 13:49:35','2021-02-26 13:49:35'),(76,'default','Update Loan Product',3,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 13:49:45','2021-02-26 13:49:45'),(77,'default','Update Loan Product',4,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 13:49:54','2021-02-26 13:49:54'),(78,'default','Update Loan Product',5,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 13:50:03','2021-02-26 13:50:03'),(79,'default','Create Loan Product',7,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 13:53:12','2021-02-26 13:53:12'),(80,'default','Create Loan Product',8,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":8}','2021-02-26 13:55:40','2021-02-26 13:55:40'),(81,'default','Create Loan Product',9,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 13:57:51','2021-02-26 13:57:51'),(82,'default','Update Loan Product',9,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 13:58:14','2021-02-26 13:58:14'),(83,'default','Update Loan Product',9,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 13:58:38','2021-02-26 13:58:38'),(84,'default','Create Loan Charge',7,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 14:04:39','2021-02-26 14:04:39'),(85,'default','Create Loan Charge',8,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":8}','2021-02-26 14:06:27','2021-02-26 14:06:27'),(86,'default','Create Loan Charge',9,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 14:07:11','2021-02-26 14:07:11'),(87,'default','Update Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:19:54','2021-02-26 14:19:54'),(88,'default','Update Loan Charge',2,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 14:20:03','2021-02-26 14:20:03'),(89,'default','Update Loan Charge',3,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 14:20:15','2021-02-26 14:20:15'),(90,'default','Update Loan Charge',3,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 14:20:24','2021-02-26 14:20:24'),(91,'default','Update Loan Charge',4,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 14:20:35','2021-02-26 14:20:35'),(92,'default','Update Loan Charge',6,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-02-26 14:20:46','2021-02-26 14:20:46'),(93,'default','Update Loan Charge',7,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 14:20:56','2021-02-26 14:20:56'),(94,'default','Update Loan Charge',8,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":8}','2021-02-26 14:21:05','2021-02-26 14:21:05'),(95,'default','Update Loan Charge',9,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 14:21:14','2021-02-26 14:21:14'),(96,'default','Update Loan Charge',9,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 14:21:22','2021-02-26 14:21:22'),(97,'default','Update Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:21:32','2021-02-26 14:21:32'),(98,'default','Create Loan Charge',10,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":10}','2021-02-26 14:23:48','2021-02-26 14:23:48'),(99,'default','Update Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:24:54','2021-02-26 14:24:54'),(100,'default','Update Loan Product',2,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 14:25:20','2021-02-26 14:25:20'),(101,'default','Update Loan Product',3,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 14:25:41','2021-02-26 14:25:41'),(102,'default','Update Loan Product',3,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 14:25:42','2021-02-26 14:25:42'),(103,'default','Update Loan Product',4,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 14:26:06','2021-02-26 14:26:06'),(104,'default','Update Loan Product',4,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 14:26:18','2021-02-26 14:26:18'),(105,'default','Update Loan Product',4,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 14:26:19','2021-02-26 14:26:19'),(106,'default','Update Loan Product',5,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 14:26:42','2021-02-26 14:26:42'),(107,'default','Update Loan Product',5,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-02-26 14:26:54','2021-02-26 14:26:54'),(108,'default','Update Loan Product',7,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 14:27:12','2021-02-26 14:27:12'),(109,'default','Update Loan Product',6,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-02-26 14:27:38','2021-02-26 14:27:38'),(110,'default','Update Loan Product',7,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 14:27:51','2021-02-26 14:27:51'),(111,'default','Update Loan Product',7,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-02-26 14:27:52','2021-02-26 14:27:52'),(112,'default','Update Loan Product',8,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":8}','2021-02-26 14:28:13','2021-02-26 14:28:13'),(113,'default','Update Loan Product',9,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":9}','2021-02-26 14:28:55','2021-02-26 14:28:55'),(114,'default','Create Loan Product',10,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":10}','2021-02-26 14:36:20','2021-02-26 14:36:20'),(115,'default','Create Loan Product',11,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":11}','2021-02-26 14:38:59','2021-02-26 14:38:59'),(116,'default','Create Loan Charge',11,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":11}','2021-02-26 14:46:34','2021-02-26 14:46:34'),(117,'default','Update Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:47:07','2021-02-26 14:47:07'),(118,'default','Update Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:47:22','2021-02-26 14:47:22'),(119,'default','Update Loan Product',1,'Modules\\Loan\\Entities\\LoanProduct',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:50:40','2021-02-26 14:50:40'),(120,'default','Create Loan',1,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:55:10','2021-02-26 14:55:10'),(121,'default','Approve Loan',1,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:55:36','2021-02-26 14:55:36'),(122,'default','Undo Loan Approval',1,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:55:57','2021-02-26 14:55:57'),(123,'default','Approve Loan',1,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:56:11','2021-02-26 14:56:11'),(124,'default','Create Payment Type',1,'Modules\\Core\\Entities\\PaymentType',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:58:13','2021-02-26 14:58:13'),(125,'default','Create Payment Type',2,'Modules\\Core\\Entities\\PaymentType',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 14:58:56','2021-02-26 14:58:56'),(126,'default','Create Payment Type',3,'Modules\\Core\\Entities\\PaymentType',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-02-26 14:59:13','2021-02-26 14:59:13'),(127,'default','Create Payment Type',4,'Modules\\Core\\Entities\\PaymentType',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-02-26 14:59:27','2021-02-26 14:59:27'),(128,'default','Disburse Loan',1,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-02-26 14:59:42','2021-02-26 14:59:42'),(129,'default','Create Client',2,'Modules\\Client\\Entities\\Client',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-02-26 15:11:06','2021-02-26 15:11:06'),(130,'default','Update User',1,'Modules\\User\\Entities\\User',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 00:46:22','2021-03-08 00:46:22'),(131,'default','Create Custom Field',1,'Modules\\CustomField\\Entities\\CustomField',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 00:47:47','2021-03-08 00:47:47'),(132,'default','Create Role',3,'Spatie\\Permission\\Models\\Role',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 00:54:26','2021-03-08 00:54:26'),(133,'default','Create User',3,'Modules\\User\\Entities\\User',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 00:55:24','2021-03-08 00:55:24'),(134,'default','Update Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 00:56:46','2021-03-08 00:56:46'),(135,'default','Update Loan Charge',2,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 00:59:37','2021-03-08 00:59:37'),(136,'default','Update Loan Charge',3,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 01:00:05','2021-03-08 01:00:05'),(137,'default','Update Loan Charge',4,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":4}','2021-03-08 01:00:18','2021-03-08 01:00:18'),(138,'default','Update Loan Charge',5,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":5}','2021-03-08 01:00:31','2021-03-08 01:00:31'),(139,'default','Update Loan Charge',7,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":7}','2021-03-08 01:00:43','2021-03-08 01:00:43'),(140,'default','Update Loan Charge',9,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":9}','2021-03-08 01:00:53','2021-03-08 01:00:53'),(141,'default','Update Loan Charge',11,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":11}','2021-03-08 01:01:03','2021-03-08 01:01:03'),(142,'default','Create Client',3,'Modules\\Client\\Entities\\Client',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 01:05:21','2021-03-08 01:05:21'),(143,'default','Update Client Status',3,'Modules\\Client\\Entities\\Client',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 01:07:06','2021-03-08 01:07:06'),(144,'default','Create Loan',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 01:08:33','2021-03-08 01:08:33'),(145,'default','Approve Loan',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 01:08:39','2021-03-08 01:08:39'),(146,'default','Disburse Loan',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 01:08:59','2021-03-08 01:08:59'),(147,'default','Undo Loan Disbursement',1,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 12:49:23','2021-03-08 12:49:23'),(148,'default','Undo Loan Approval',1,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 12:49:33','2021-03-08 12:49:33'),(149,'default','Create Client',4,'Modules\\Client\\Entities\\Client',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-03-08 14:21:19','2021-03-08 14:21:19'),(150,'default','Update Client',4,'Modules\\Client\\Entities\\Client',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-03-08 14:23:29','2021-03-08 14:23:29'),(151,'default','Create User',4,'Modules\\User\\Entities\\User',2,'Modules\\User\\Entities\\User','{\"id\":4}','2021-03-08 14:25:24','2021-03-08 14:25:24'),(152,'default','Create User',5,'Modules\\User\\Entities\\User',2,'Modules\\User\\Entities\\User','{\"id\":5}','2021-03-08 14:27:59','2021-03-08 14:27:59'),(153,'default','Create User',6,'Modules\\User\\Entities\\User',2,'Modules\\User\\Entities\\User','{\"id\":6}','2021-03-08 14:32:40','2021-03-08 14:32:40'),(154,'default','Create User',7,'Modules\\User\\Entities\\User',2,'Modules\\User\\Entities\\User','{\"id\":7}','2021-03-08 14:35:36','2021-03-08 14:35:36'),(155,'default','Reject Loan',1,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 14:35:49','2021-03-08 14:35:49'),(156,'default','Update User',2,'Modules\\User\\Entities\\User',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 14:37:09','2021-03-08 14:37:09'),(157,'default','Update User',2,'Modules\\User\\Entities\\User',2,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 14:37:59','2021-03-08 14:37:59'),(158,'default','Delete User',3,'Modules\\User\\Entities\\User',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 14:38:21','2021-03-08 14:38:21'),(159,'default','Undo Loan Disbursement',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 14:46:46','2021-03-08 14:46:46'),(160,'default','Undo Loan Approval',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 14:46:52','2021-03-08 14:46:52'),(161,'default','Update Loan Product',2,'Modules\\Loan\\Entities\\LoanProduct',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 14:49:00','2021-03-08 14:49:00'),(162,'default','Update Client Status',4,'Modules\\Client\\Entities\\Client',1,'Modules\\User\\Entities\\User','{\"id\":4}','2021-03-08 14:54:36','2021-03-08 14:54:36'),(163,'default','Update Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 14:56:31','2021-03-08 14:56:31'),(164,'default','Update Role',3,'Spatie\\Permission\\Models\\Role',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 14:58:49','2021-03-08 14:58:49'),(165,'default','Update Loan Charge',11,'Modules\\Loan\\Entities\\LoanCharge',1,'Modules\\User\\Entities\\User','{\"id\":11}','2021-03-08 15:10:47','2021-03-08 15:10:47'),(166,'default','Update Loan',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 15:26:16','2021-03-08 15:26:16'),(167,'default','Update Loan Charge',1,'Modules\\Loan\\Entities\\LoanCharge',4,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 15:29:00','2021-03-08 15:29:00'),(168,'default','Create Loan',3,'Modules\\Loan\\Entities\\Loan',4,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 16:45:24','2021-03-08 16:45:24'),(169,'default','Update Role',3,'Spatie\\Permission\\Models\\Role',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 16:51:49','2021-03-08 16:51:49'),(170,'default','Approve Loan',3,'Modules\\Loan\\Entities\\Loan',4,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 16:54:41','2021-03-08 16:54:41'),(171,'default','Change Loan Officer',3,'Modules\\Loan\\Entities\\Loan',4,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 16:54:56','2021-03-08 16:54:56'),(172,'default','Undo Loan Rejection',1,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-08 16:56:04','2021-03-08 16:56:04'),(173,'default','Disburse Loan',3,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 16:56:54','2021-03-08 16:56:54'),(174,'default','Update Loan',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 17:04:15','2021-03-08 17:04:15'),(175,'default','Approve Loan',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 17:04:21','2021-03-08 17:04:21'),(176,'default','Disburse Loan',2,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-08 17:04:37','2021-03-08 17:04:37'),(177,'default','Waive Loan Interest',3,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 17:10:29','2021-03-08 17:10:29'),(178,'default','Undo Loan Disbursement',3,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 17:11:02','2021-03-08 17:11:02'),(179,'default','Disburse Loan',3,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 17:11:22','2021-03-08 17:11:22'),(180,'default','Undo Loan Disbursement',3,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 17:15:34','2021-03-08 17:15:34'),(181,'default','Disburse Loan',3,'Modules\\Loan\\Entities\\Loan',2,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-08 17:16:14','2021-03-08 17:16:14'),(182,'default','Update Client Status',2,'Modules\\Client\\Entities\\Client',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-18 08:12:28','2021-03-18 08:12:28'),(183,'default','Undo Loan Disbursement',3,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-20 12:41:27','2021-03-20 12:41:27'),(184,'default','Undo Loan Approval',3,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-23 11:45:19','2021-03-23 11:45:19'),(185,'default','Approve Loan',3,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-23 12:21:43','2021-03-23 12:21:43'),(186,'default','Disburse Loan',3,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":3}','2021-03-23 12:21:58','2021-03-23 12:21:58'),(187,'default','Create Chart Of Account',1,'Modules\\Accounting\\Entities\\ChartOfAccount',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-03-25 10:33:51','2021-03-25 10:33:51'),(188,'default','Create Chart Of Account',2,'Modules\\Accounting\\Entities\\ChartOfAccount',1,'Modules\\User\\Entities\\User','{\"id\":2}','2021-03-25 10:34:28','2021-03-25 10:34:28'),(189,'default','Update Settings',NULL,NULL,1,'Modules\\User\\Entities\\User','[]','2021-03-25 12:21:21','2021-03-25 12:21:21'),(190,'default','Approve Loan',1,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":1}','2021-05-05 14:01:23','2021-05-05 14:01:23'),(191,'default','Create Loan',4,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":4}','2021-05-10 13:08:59','2021-05-10 13:08:59'),(192,'default','Approve Loan',4,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":4}','2021-05-10 13:09:23','2021-05-10 13:09:23'),(193,'default','Disburse Loan',4,'Modules\\Loan\\Entities\\Loan',1,'Modules\\User\\Entities\\User','{\"id\":4}','2021-05-10 13:25:46','2021-05-10 13:25:46');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_depreciation`
--

DROP TABLE IF EXISTS `asset_depreciation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_depreciation` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` bigint unsigned DEFAULT NULL,
  `year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `beginning_value` decimal(65,2) DEFAULT NULL,
  `depreciation_value` decimal(65,2) DEFAULT NULL,
  `rate` decimal(65,2) DEFAULT NULL,
  `cost` decimal(65,2) DEFAULT NULL,
  `accumulated` decimal(65,2) DEFAULT NULL,
  `ending_value` decimal(65,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_depreciation`
--

LOCK TABLES `asset_depreciation` WRITE;
/*!40000 ALTER TABLE `asset_depreciation` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_depreciation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_files`
--

DROP TABLE IF EXISTS `asset_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_files` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` bigint unsigned DEFAULT NULL,
  `file` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_files`
--

LOCK TABLES `asset_files` WRITE;
/*!40000 ALTER TABLE `asset_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_maintenance`
--

DROP TABLE IF EXISTS `asset_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_maintenance` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `asset_maintenance_type_id` bigint unsigned DEFAULT NULL,
  `asset_id` bigint unsigned DEFAULT NULL,
  `performed_by` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `amount` decimal(65,2) DEFAULT NULL,
  `mileage` decimal(65,2) DEFAULT NULL,
  `record_expense` tinyint NOT NULL DEFAULT '0',
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_maintenance`
--

LOCK TABLES `asset_maintenance` WRITE;
/*!40000 ALTER TABLE `asset_maintenance` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_maintenance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_maintenance_types`
--

DROP TABLE IF EXISTS `asset_maintenance_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_maintenance_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_maintenance_types`
--

LOCK TABLES `asset_maintenance_types` WRITE;
/*!40000 ALTER TABLE `asset_maintenance_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_maintenance_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_notes`
--

DROP TABLE IF EXISTS `asset_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_notes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` int unsigned DEFAULT NULL,
  `asset_id` bigint unsigned DEFAULT NULL,
  `attachment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_notes`
--

LOCK TABLES `asset_notes` WRITE;
/*!40000 ALTER TABLE `asset_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_pictures`
--

DROP TABLE IF EXISTS `asset_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_pictures` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` bigint unsigned DEFAULT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `primary_picture` tinyint NOT NULL DEFAULT '0',
  `picture` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `date_taken` date DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_pictures`
--

LOCK TABLES `asset_pictures` WRITE;
/*!40000 ALTER TABLE `asset_pictures` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_types`
--

DROP TABLE IF EXISTS `asset_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('current','fixed','intangible','investment','other') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chart_of_account_fixed_asset_id` int DEFAULT NULL,
  `chart_of_account_asset_id` int DEFAULT NULL,
  `chart_of_account_contra_asset_id` int DEFAULT NULL,
  `chart_of_account_expense_id` int DEFAULT NULL,
  `chart_of_account_liability_id` int DEFAULT NULL,
  `chart_of_account_income_id` int DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_types`
--

LOCK TABLES `asset_types` WRITE;
/*!40000 ALTER TABLE `asset_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `assets` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `asset_type_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_price` decimal(65,2) DEFAULT NULL,
  `replacement_value` decimal(65,2) DEFAULT NULL,
  `value` decimal(65,2) DEFAULT NULL,
  `life_span` int DEFAULT NULL,
  `salvage_value` decimal(65,2) DEFAULT NULL,
  `serial_number` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `bought_from` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('active','inactive','sold','damaged','written_off') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch_users`
--

DROP TABLE IF EXISTS `branch_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branch_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  `branch_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch_users`
--

LOCK TABLES `branch_users` WRITE;
/*!40000 ALTER TABLE `branch_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branches` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` int DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `manager_id` int DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `open_date` date DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  `is_system` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,NULL,NULL,NULL,'KITENGELA BRANCH','2020-08-12','KITENGELA',1,1,'2021-02-25 17:09:27','2021-02-26 12:27:50'),(2,NULL,NULL,NULL,'RUIRU BRANCH','2020-08-12','EASTERN BYPASS NEXT TO QUICKMART',1,0,'2021-02-26 12:27:05','2021-02-26 12:27:05');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chart_of_accounts`
--

DROP TABLE IF EXISTS `chart_of_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chart_of_accounts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `gl_code` int DEFAULT NULL,
  `account_type` enum('asset','expense','equity','liability','income') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'asset',
  `allow_manual` tinyint NOT NULL DEFAULT '0',
  `active` tinyint NOT NULL DEFAULT '1',
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chart_of_accounts`
--

LOCK TABLES `chart_of_accounts` WRITE;
/*!40000 ALTER TABLE `chart_of_accounts` DISABLE KEYS */;
INSERT INTO `chart_of_accounts` VALUES (1,NULL,'FIXED ASSETS',20100,'asset',1,1,NULL,'2021-03-25 10:33:51','2021-03-25 10:33:51'),(2,1,'Furniture and Fixtures',20101,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(3,1,'Buildings',20102,'asset',1,1,NULL,'2021-03-25 10:33:51','2021-03-25 10:33:51'),(4,1,'Mortocycles',20103,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(5,1,'Safes',20104,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(6,1,'Computers, network infrastructure and servers',20105,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(7,1,'Printers,photocopier and scanners',20106,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(8,1,'Power Systems',20107,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(9,1,'NCDA Development funds',20109,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(10,NULL,'Investments',20200,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(11,NULL,'Salene Registration Fees',202000,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(12,NULL,'Cash & Bank balances',20300,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(13,12,'Cash In vault/safe',20301,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(14,12,'Petty Cash',20302,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(15,12,'cash at paybill salene credit',20303,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(16,12,'Debtors',20309,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(17,NULL,'Bank Accounts',20400,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(18,17,'Bank a/c EQUITY',20401,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(19,17,'Bank a/c ABSA',20402,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(20,17,'Bank a/c KCB',20403,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(21,NULL,'Lending Portfolio',20500,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(22,22,'Loans to Customers',20501,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(23,22,'Current Account Overdrafts',20502,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(24,NULL,'Interest and Fees Receivable',20600,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(25,NULL,'Advance Payments',20700,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(26,NULL,'Reserves for Possible Losses',20800,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(27,NULL,'Accrued Income',20900,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(28,NULL,'Assets accounts transfer(tem)',0,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(29,NULL,'Trust fund',30400,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(30,NULL,'Building funds',30600,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(31,NULL,'Cctv and fire extinguishers',30800,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(32,NULL,'Suspense Account',9999,'asset',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(33,NULL,'Equity and Share Capital',50100,'equity',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(34,NULL,'share capital',50101,'equity',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(35,NULL,'grants/general reserves',50102,'equity',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(36,NULL,'retained earnings',50103,'equity',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(37,NULL,'Capital reserve',50104,'equity',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(38,NULL,'Opening Balances Contra Account',9099,'equity',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(39,NULL,'Portfolio Expenses',40100,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(40,39,'Losses Written Off',40101,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(41,39,'Interest Paid To Depositors',40102,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(42,39,'Loan Collection Expenses',40103,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(43,NULL,'General And Administrative Expenses',40200,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(44,NULL,'Salaries and Wages',40300,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(45,44,'Salaries and Wages(NET)',40301,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(46,44,'PAYE Contribution',40302,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(47,44,'NSSF Contribution',40303,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(48,44,'NHIF Contribution',40304,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(49,NULL,'Professional Expenses',40400,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(50,NULL,'Travel and Conveyance Expenses',40500,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(51,NULL,'Training Expenses',40600,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(52,NULL,'Office Expenses',40700,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(53,52,'Printing and Stationery',40701,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(54,52,'Telephone Charges',40702,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(55,52,'Electricity Charges',40703,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(56,52,'Rent Paid',40704,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(57,52,'Internet Charges',40705,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(58,NULL,'Sundry Expenses',40800,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(59,NULL,'Moto Bike Repairs and Maintenance',40900,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(60,NULL,'Depreciation and Amortization',41000,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(61,NULL,'Board Meeting Expenses',41100,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(62,NULL,'Asset Insurance paid',41200,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(63,NULL,'Loan insurance paid',41300,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(64,NULL,'Bank charges',41400,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(65,NULL,'IncomeTax Expenses',41500,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(66,NULL,'Employee benefit expense',41600,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(67,NULL,'Project expenses',41700,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(68,NULL,'Field allowances',41800,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(69,NULL,'Staff allowances',41900,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(70,NULL,'AI expenses',42000,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(71,NULL,'Computer Repair and Maintainance Expenses',42300,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(72,NULL,'Board Travel Expenses',42400,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(73,NULL,'Finance Committee Meeting Expenses',42500,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(74,NULL,'Power System Repair and Maintainance',42800,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(75,NULL,'Procurement Committee Meeting Expenses',42900,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(76,NULL,'Loan Commettee Meeting Expenses',43000,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(77,NULL,'Motor Bike Running expenses',43100,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(78,NULL,'Publicity and Advertisement Expenses',43200,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(79,NULL,'Annual General Meeting Expenses',43300,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(80,NULL,'Computer Accessories',43400,'expense',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(81,NULL,'Loan Recovery (Temp)',0,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(82,NULL,'Portfolio Earnings',30100,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(83,NULL,'Fees and Charges',30101,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(84,NULL,'Penalties',30102,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(85,NULL,'Interest Received from Borrowers',30103,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(86,NULL,'Insurance Charges',30104,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(87,NULL,'Other Operating Income',30105,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(88,NULL,'Loan processing fee',30106,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(89,NULL,'Non Portfolio Income',30200,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(90,NULL,'Membership fee',30300,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(91,NULL,'Overdraft interest income',30700,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(92,NULL,'Income from investment',30900,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(93,NULL,'Grant Income Donor 1.(KCDF)',400100,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(94,NULL,'Grant Income Donor 2.(ADF))',400200,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(95,NULL,'Building fund',41901,'income',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(96,NULL,'Deposit Accounts Portfolio',10100,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(97,NULL,'Voluntary Savings',10101,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(98,NULL,'Client voluntary savings',0,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(99,NULL,'Self help group(SHG)',0,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(100,NULL,'Salene Savings',10102,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(101,NULL,'Term Deposits',10103,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(102,NULL,'Recurring Deposits',10104,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(103,NULL,'Overpayment Liability',10200,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(104,NULL,'Interest Payable',10300,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(105,NULL,'Pass-Through Collections',10400,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(106,NULL,'External Credits and Borrowings',10500,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(107,NULL,'Accrued Expenses and Provisions',10600,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(108,NULL,'Other Liabilities',10700,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(109,NULL,'Divided Payable',10800,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(110,NULL,'Escheat liability',10900,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(111,NULL,'Un Paid Divideds',10901,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(112,NULL,'Tax payable',10950,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(113,NULL,'Saving suspense account',11000,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(114,NULL,'Accumulative Depreciation On Fixed Assets',11100,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(115,NULL,'Liability Transfer (Temp)',0,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(116,NULL,'Life member insurance',42100,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(117,NULL,'Temporary Loan Disbursement',42200,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28'),(118,NULL,'Grant From Donor',43500,'liability',1,1,NULL,'2021-03-25 10:34:28','2021-03-25 10:34:28');
/*!40000 ALTER TABLE `chart_of_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_files`
--

DROP TABLE IF EXISTS `client_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client_files` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `size` int DEFAULT NULL,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_files`
--

LOCK TABLES `client_files` WRITE;
/*!40000 ALTER TABLE `client_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_identification`
--

DROP TABLE IF EXISTS `client_identification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client_identification` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned DEFAULT NULL,
  `client_identification_type_id` bigint unsigned DEFAULT NULL,
  `identification_value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `size` int DEFAULT NULL,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_identification`
--

LOCK TABLES `client_identification` WRITE;
/*!40000 ALTER TABLE `client_identification` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_identification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_identification_types`
--

DROP TABLE IF EXISTS `client_identification_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client_identification_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_identification_types`
--

LOCK TABLES `client_identification_types` WRITE;
/*!40000 ALTER TABLE `client_identification_types` DISABLE KEYS */;
INSERT INTO `client_identification_types` VALUES (1,'ID NUMBER'),(2,'PASSPORT NUMBER');
/*!40000 ALTER TABLE `client_identification_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_next_of_kin`
--

DROP TABLE IF EXISTS `client_next_of_kin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client_next_of_kin` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned DEFAULT NULL,
  `client_relationship_id` bigint unsigned DEFAULT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','other','unspecified') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'unspecified',
  `marital_status` enum('married','single','divorced','widowed','unspecified','other') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'unspecified',
  `country_id` bigint unsigned DEFAULT NULL,
  `title_id` bigint unsigned DEFAULT NULL,
  `profession_id` bigint unsigned DEFAULT NULL,
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_next_of_kin`
--

LOCK TABLES `client_next_of_kin` WRITE;
/*!40000 ALTER TABLE `client_next_of_kin` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_next_of_kin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_relationships`
--

DROP TABLE IF EXISTS `client_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client_relationships` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_relationships`
--

LOCK TABLES `client_relationships` WRITE;
/*!40000 ALTER TABLE `client_relationships` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_types`
--

DROP TABLE IF EXISTS `client_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_types`
--

LOCK TABLES `client_types` WRITE;
/*!40000 ALTER TABLE `client_types` DISABLE KEYS */;
INSERT INTO `client_types` VALUES (1,'EMPLOYED'),(2,'BUSINESS'),(3,'FARMERS'),(4,'MANUFACTURERS');
/*!40000 ALTER TABLE `client_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_users`
--

DROP TABLE IF EXISTS `client_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned DEFAULT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_users`
--

LOCK TABLES `client_users` WRITE;
/*!40000 ALTER TABLE `client_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `loan_officer_id` bigint unsigned DEFAULT NULL,
  `reference` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','other','unspecified') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'unspecified',
  `status` enum('pending','active','inactive','deceased','unspecified','closed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `marital_status` enum('married','single','divorced','widowed','unspecified','other') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'unspecified',
  `country_id` bigint unsigned DEFAULT NULL,
  `title_id` bigint unsigned DEFAULT NULL,
  `profession_id` bigint unsigned DEFAULT NULL,
  `client_type_id` bigint unsigned DEFAULT NULL,
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `external_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `signature` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_date` date DEFAULT NULL,
  `joined_date` date DEFAULT NULL,
  `activation_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,2,2,2,NULL,NULL,'PETER',NULL,'KARUMBI NJOROGE','male','active','married',113,NULL,NULL,NULL,'0721576867',NULL,'peterkarumbi@gmail.com','SCL001','1980-12-01','1320-10101',NULL,NULL,NULL,NULL,NULL,'Director Digital Credit\r\nRegional Manager Equity bank ltd base at Nyeri',NULL,'2021-09-24',NULL,NULL,'2021-02-26 12:39:32','2021-02-26 12:40:19'),(2,2,2,2,NULL,NULL,'MERCY',NULL,'M. KIRIMI','female','active','married',113,3,1,1,'0722689020',NULL,'mercy.kirimi09@gmail.com','MK001','1975-07-19','364-00618',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-26',NULL,NULL,'2021-02-26 15:11:06','2021-03-18 08:12:27'),(3,1,2,3,NULL,NULL,'TEST',NULL,'TEST','female','active','single',113,2,5,2,'074520000',NULL,'testcliet@gmail.com','345333','1986-11-11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08',NULL,NULL,'2021-03-08 01:05:21','2021-03-08 01:07:06'),(4,2,2,2,NULL,NULL,'NDENGA',NULL,'ANTONY GUYALI','male','active','married',113,1,5,2,'0720362158',NULL,'ndenga.guyali@gmail.com','SCL027','1983-09-29','7834-00200 NAIROBI',NULL,NULL,NULL,NULL,NULL,'The customer operates a construction company in the name of Mukova civil engineering with a current project in Syokimau community road with KERRA',NULL,'2021-03-08',NULL,NULL,'2021-03-08 14:21:19','2021-03-08 14:54:36');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communication_campaign_attachment_types`
--

DROP TABLE IF EXISTS `communication_campaign_attachment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `communication_campaign_attachment_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_trigger` tinyint NOT NULL DEFAULT '0',
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communication_campaign_attachment_types`
--

LOCK TABLES `communication_campaign_attachment_types` WRITE;
/*!40000 ALTER TABLE `communication_campaign_attachment_types` DISABLE KEYS */;
INSERT INTO `communication_campaign_attachment_types` VALUES (1,'Loan Schedule',NULL,0,1),(2,'Client Statement',NULL,0,1),(3,'Saving Mini Statement',NULL,0,0);
/*!40000 ALTER TABLE `communication_campaign_attachment_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communication_campaign_business_rules`
--

DROP TABLE IF EXISTS `communication_campaign_business_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `communication_campaign_business_rules` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  `is_trigger` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communication_campaign_business_rules`
--

LOCK TABLES `communication_campaign_business_rules` WRITE;
/*!40000 ALTER TABLE `communication_campaign_business_rules` DISABLE KEYS */;
INSERT INTO `communication_campaign_business_rules` VALUES (1,'Active Clients',NULL,'All clients with the status ‘Active’',1,0),(2,'Prospective Clients',NULL,'All clients with the status ‘Active’ who have never had a loan before',1,0),(3,'Active Loan Clients',NULL,'All clients with an outstanding loan',1,0),(4,'Loans in arrears',NULL,'All clients with an outstanding loan in arrears between X and Y days',1,0),(5,'Loans disbursed to clients',NULL,'All clients who have had a loan disbursed to them in the last X to Y days',1,0),(6,'Loan payments due',NULL,'All clients with an unpaid installment due on their loan between X and Y days',1,0),(7,'Dormant Prospects',NULL,'All individuals who have not yet received a loan but were also entered into the system more than 3 months',0,0),(8,'Loan Payments Due (Overdue Loans)',NULL,'Loan Payments Due between X to Y days for clients in arrears between X and Y days',0,0),(9,'Loan Payments Received (Active Loans)',NULL,'Payments received in the last X to Y days for any loan with the status Active (on-time)',0,0),(10,'Loan Payments Received (Overdue Loans) ',NULL,'Payments received in the last X to Y days for any loan with the status Overdue (arrears) between X and Y days',0,0),(11,'Happy Birthday',NULL,'This sends a message to all clients with the status Active on their Birthday',0,0),(12,'Loan Fully Repaid',NULL,'All loans that have been fully repaid (Closed or Overpaid) in the last X to Y days',0,0),(13,'Loans Outstanding after final instalment date',NULL,'All active loans (with an outstanding balance) between X to Y days after the final instalment date on their loan schedule',0,0),(14,'Past Loan Clients',NULL,'Past Loan Clients who have previously had a loan but do not currently have one and finished repaying their most recent loan in the last X to Y days.',0,0),(15,'Loan Submitted',NULL,'Loan and client data of submitted loan',1,1),(16,'Loan Rejected',NULL,'Loan and client data of rejected loan',1,1),(17,'Loan Approved',NULL,'Loan and client data of approved loan',1,1),(18,'Loan Disbursed',NULL,'Loan Disbursed',1,1),(19,'Loan Rescheduled',NULL,'Loan Rescheduled',1,1),(20,'Loan Closed',NULL,'Loan Closed',1,1),(21,'Loan Repayment',NULL,'Loan Repayment',1,1),(22,'Savings Submitted',NULL,'Savings and client data of submitted savings',1,1),(23,'Savings Rejected',NULL,'Savings and client data of rejected savings',1,1),(24,'Savings Approved',NULL,'Savings and client data of approved savings',1,1),(25,'Savings Activated',NULL,'Savings Activated',1,1),(26,'Savings Dormant',NULL,'Savings Dormant',1,1),(27,'Savings Inactive',NULL,'Savings Inactive',1,1),(28,'Savings Closed',NULL,'Savings Closed',1,1),(29,'Savings Deposit',NULL,'Savings Deposit',1,1),(30,'Savings Withdrawal',NULL,'Savings Withdrawal',1,1);
/*!40000 ALTER TABLE `communication_campaign_business_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communication_campaign_logs`
--

DROP TABLE IF EXISTS `communication_campaign_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `communication_campaign_logs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned DEFAULT NULL,
  `sms_gateway_id` bigint DEFAULT NULL,
  `communication_campaign_id` bigint DEFAULT NULL,
  `campaign_type` enum('sms','email') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `send_to` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `campaign_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('pending','sent','delivered','failed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communication_campaign_logs`
--

LOCK TABLES `communication_campaign_logs` WRITE;
/*!40000 ALTER TABLE `communication_campaign_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `communication_campaign_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communication_campaigns`
--

DROP TABLE IF EXISTS `communication_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `communication_campaigns` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `sms_gateway_id` bigint unsigned DEFAULT NULL,
  `communication_campaign_business_rule_id` bigint unsigned DEFAULT NULL,
  `communication_campaign_attachment_type_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `loan_officer_id` bigint unsigned DEFAULT NULL,
  `loan_product_id` bigint unsigned DEFAULT NULL,
  `subject` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `campaign_type` enum('sms','email') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'email',
  `trigger_type` enum('direct','schedule','triggered') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'direct',
  `scheduled_date` date DEFAULT NULL,
  `scheduled_time` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule_frequency` int DEFAULT NULL,
  `schedule_frequency_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'days',
  `scheduled_next_run_date` date DEFAULT NULL,
  `scheduled_last_run_date` date DEFAULT NULL,
  `from_x` int DEFAULT NULL,
  `to_y` int DEFAULT NULL,
  `cycle_x` int DEFAULT NULL,
  `cycle_y` int DEFAULT NULL,
  `overdue_x` int DEFAULT NULL,
  `overdue_y` int DEFAULT NULL,
  `active` tinyint NOT NULL DEFAULT '0',
  `status` enum('pending','active','inactive','closed','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communication_campaigns`
--

LOCK TABLES `communication_campaigns` WRITE;
/*!40000 ALTER TABLE `communication_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `communication_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sortname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'AF','Afghanistan',NULL,NULL),(2,'AL','Albania',NULL,NULL),(3,'DZ','Algeria',NULL,NULL),(4,'AS','American Samoa',NULL,NULL),(5,'AD','Andorra',NULL,NULL),(6,'AO','Angola',NULL,NULL),(7,'AI','Anguilla',NULL,NULL),(8,'AQ','Antarctica',NULL,NULL),(9,'AG','Antigua And Barbuda',NULL,NULL),(10,'AR','Argentina',NULL,NULL),(11,'AM','Armenia',NULL,NULL),(12,'AW','Aruba',NULL,NULL),(13,'AU','Australia',NULL,NULL),(14,'AT','Austria',NULL,NULL),(15,'AZ','Azerbaijan',NULL,NULL),(16,'BS','Bahamas The',NULL,NULL),(17,'BH','Bahrain',NULL,NULL),(18,'BD','Bangladesh',NULL,NULL),(19,'BB','Barbados',NULL,NULL),(20,'BY','Belarus',NULL,NULL),(21,'BE','Belgium',NULL,NULL),(22,'BZ','Belize',NULL,NULL),(23,'BJ','Benin',NULL,NULL),(24,'BM','Bermuda',NULL,NULL),(25,'BT','Bhutan',NULL,NULL),(26,'BO','Bolivia',NULL,NULL),(27,'BA','Bosnia and Herzegovina',NULL,NULL),(28,'BW','Botswana',NULL,NULL),(29,'BV','Bouvet Island',NULL,NULL),(30,'BR','Brazil',NULL,NULL),(31,'IO','British Indian Ocean Territory',NULL,NULL),(32,'BN','Brunei',NULL,NULL),(33,'BG','Bulgaria',NULL,NULL),(34,'BF','Burkina Faso',NULL,NULL),(35,'BI','Burundi',NULL,NULL),(36,'KH','Cambodia',NULL,NULL),(37,'CM','Cameroon',NULL,NULL),(38,'CA','Canada',NULL,NULL),(39,'CV','Cape Verde',NULL,NULL),(40,'KY','Cayman Islands',NULL,NULL),(41,'CF','Central African Republic',NULL,NULL),(42,'TD','Chad',NULL,NULL),(43,'CL','Chile',NULL,NULL),(44,'CN','China',NULL,NULL),(45,'CX','Christmas Island',NULL,NULL),(46,'CC','Cocos (Keeling) Islands',NULL,NULL),(47,'CO','Colombia',NULL,NULL),(48,'KM','Comoros',NULL,NULL),(49,'CG','Congo',NULL,NULL),(50,'CD','Congo The Democratic Republic Of The',NULL,NULL),(51,'CK','Cook Islands',NULL,NULL),(52,'CR','Costa Rica',NULL,NULL),(53,'CI','Cote D\'Ivoire (Ivory Coast)',NULL,NULL),(54,'HR','Croatia (Hrvatska)',NULL,NULL),(55,'CU','Cuba',NULL,NULL),(56,'CY','Cyprus',NULL,NULL),(57,'CZ','Czech Republic',NULL,NULL),(58,'DK','Denmark',NULL,NULL),(59,'DJ','Djibouti',NULL,NULL),(60,'DM','Dominica',NULL,NULL),(61,'DO','Dominican Republic',NULL,NULL),(62,'TP','East Timor',NULL,NULL),(63,'EC','Ecuador',NULL,NULL),(64,'EG','Egypt',NULL,NULL),(65,'SV','El Salvador',NULL,NULL),(66,'GQ','Equatorial Guinea',NULL,NULL),(67,'ER','Eritrea',NULL,NULL),(68,'EE','Estonia',NULL,NULL),(69,'ET','Ethiopia',NULL,NULL),(70,'XA','External Territories of Australia',NULL,NULL),(71,'FK','Falkland Islands',NULL,NULL),(72,'FO','Faroe Islands',NULL,NULL),(73,'FJ','Fiji Islands',NULL,NULL),(74,'FI','Finland',NULL,NULL),(75,'FR','France',NULL,NULL),(76,'GF','French Guiana',NULL,NULL),(77,'PF','French Polynesia',NULL,NULL),(78,'TF','French Southern Territories',NULL,NULL),(79,'GA','Gabon',NULL,NULL),(80,'GM','Gambia The',NULL,NULL),(81,'GE','Georgia',NULL,NULL),(82,'DE','Germany',NULL,NULL),(83,'GH','Ghana',NULL,NULL),(84,'GI','Gibraltar',NULL,NULL),(85,'GR','Greece',NULL,NULL),(86,'GL','Greenland',NULL,NULL),(87,'GD','Grenada',NULL,NULL),(88,'GP','Guadeloupe',NULL,NULL),(89,'GU','Guam',NULL,NULL),(90,'GT','Guatemala',NULL,NULL),(91,'XU','Guernsey and Alderney',NULL,NULL),(92,'GN','Guinea',NULL,NULL),(93,'GW','Guinea-Bissau',NULL,NULL),(94,'GY','Guyana',NULL,NULL),(95,'HT','Haiti',NULL,NULL),(96,'HM','Heard and McDonald Islands',NULL,NULL),(97,'HN','Honduras',NULL,NULL),(98,'HK','Hong Kong S.A.R.',NULL,NULL),(99,'HU','Hungary',NULL,NULL),(100,'IS','Iceland',NULL,NULL),(101,'IN','India',NULL,NULL),(102,'ID','Indonesia',NULL,NULL),(103,'IR','Iran',NULL,NULL),(104,'IQ','Iraq',NULL,NULL),(105,'IE','Ireland',NULL,NULL),(106,'IL','Israel',NULL,NULL),(107,'IT','Italy',NULL,NULL),(108,'JM','Jamaica',NULL,NULL),(109,'JP','Japan',NULL,NULL),(110,'XJ','Jersey',NULL,NULL),(111,'JO','Jordan',NULL,NULL),(112,'KZ','Kazakhstan',NULL,NULL),(113,'KE','Kenya',NULL,NULL),(114,'KI','Kiribati',NULL,NULL),(115,'KP','Korea North',NULL,NULL),(116,'KR','Korea South',NULL,NULL),(117,'KW','Kuwait',NULL,NULL),(118,'KG','Kyrgyzstan',NULL,NULL),(119,'LA','Laos',NULL,NULL),(120,'LV','Latvia',NULL,NULL),(121,'LB','Lebanon',NULL,NULL),(122,'LS','Lesotho',NULL,NULL),(123,'LR','Liberia',NULL,NULL),(124,'LY','Libya',NULL,NULL),(125,'LI','Liechtenstein',NULL,NULL),(126,'LT','Lithuania',NULL,NULL),(127,'LU','Luxembourg',NULL,NULL),(128,'MO','Macau S.A.R.',NULL,NULL),(129,'MK','Macedonia',NULL,NULL),(130,'MG','Madagascar',NULL,NULL),(131,'MW','Malawi',NULL,NULL),(132,'MY','Malaysia',NULL,NULL),(133,'MV','Maldives',NULL,NULL),(134,'ML','Mali',NULL,NULL),(135,'MT','Malta',NULL,NULL),(136,'XM','Man (Isle of)',NULL,NULL),(137,'MH','Marshall Islands',NULL,NULL),(138,'MQ','Martinique',NULL,NULL),(139,'MR','Mauritania',NULL,NULL),(140,'MU','Mauritius',NULL,NULL),(141,'YT','Mayotte',NULL,NULL),(142,'MX','Mexico',NULL,NULL),(143,'FM','Micronesia',NULL,NULL),(144,'MD','Moldova',NULL,NULL),(145,'MC','Monaco',NULL,NULL),(146,'MN','Mongolia',NULL,NULL),(147,'MS','Montserrat',NULL,NULL),(148,'MA','Morocco',NULL,NULL),(149,'MZ','Mozambique',NULL,NULL),(150,'MM','Myanmar',NULL,NULL),(151,'NA','Namibia',NULL,NULL),(152,'NR','Nauru',NULL,NULL),(153,'NP','Nepal',NULL,NULL),(154,'AN','Netherlands Antilles',NULL,NULL),(155,'NL','Netherlands The',NULL,NULL),(156,'NC','New Caledonia',NULL,NULL),(157,'NZ','New Zealand',NULL,NULL),(158,'NI','Nicaragua',NULL,NULL),(159,'NE','Niger',NULL,NULL),(160,'NG','Nigeria',NULL,NULL),(161,'NU','Niue',NULL,NULL),(162,'NF','Norfolk Island',NULL,NULL),(163,'MP','Northern Mariana Islands',NULL,NULL),(164,'NO','Norway',NULL,NULL),(165,'OM','Oman',NULL,NULL),(166,'PK','Pakistan',NULL,NULL),(167,'PW','Palau',NULL,NULL),(168,'PS','Palestinian Territory Occupied',NULL,NULL),(169,'PA','Panama',NULL,NULL),(170,'PG','Papua new Guinea',NULL,NULL),(171,'PY','Paraguay',NULL,NULL),(172,'PE','Peru',NULL,NULL),(173,'PH','Philippines',NULL,NULL),(174,'PN','Pitcairn Island',NULL,NULL),(175,'PL','Poland',NULL,NULL),(176,'PT','Portugal',NULL,NULL),(177,'PR','Puerto Rico',NULL,NULL),(178,'QA','Qatar',NULL,NULL),(179,'RE','Reunion',NULL,NULL),(180,'RO','Romania',NULL,NULL),(181,'RU','Russia',NULL,NULL),(182,'RW','Rwanda',NULL,NULL),(183,'SH','Saint Helena',NULL,NULL),(184,'KN','Saint Kitts And Nevis',NULL,NULL),(185,'LC','Saint Lucia',NULL,NULL),(186,'PM','Saint Pierre and Miquelon',NULL,NULL),(187,'VC','Saint Vincent And The Grenadines',NULL,NULL),(188,'WS','Samoa',NULL,NULL),(189,'SM','San Marino',NULL,NULL),(190,'ST','Sao Tome and Principe',NULL,NULL),(191,'SA','Saudi Arabia',NULL,NULL),(192,'SN','Senegal',NULL,NULL),(193,'RS','Serbia',NULL,NULL),(194,'SC','Seychelles',NULL,NULL),(195,'SL','Sierra Leone',NULL,NULL),(196,'SG','Singapore',NULL,NULL),(197,'SK','Slovakia',NULL,NULL),(198,'SI','Slovenia',NULL,NULL),(199,'XG','Smaller Territories of the UK',NULL,NULL),(200,'SB','Solomon Islands',NULL,NULL),(201,'SO','Somalia',NULL,NULL),(202,'ZA','South Africa',NULL,NULL),(203,'GS','South Georgia',NULL,NULL),(204,'SS','South Sudan',NULL,NULL),(205,'ES','Spain',NULL,NULL),(206,'LK','Sri Lanka',NULL,NULL),(207,'SD','Sudan',NULL,NULL),(208,'SR','Suriname',NULL,NULL),(209,'SJ','Svalbard And Jan Mayen Islands',NULL,NULL),(210,'SZ','Swaziland',NULL,NULL),(211,'SE','Sweden',NULL,NULL),(212,'CH','Switzerland',NULL,NULL),(213,'SY','Syria',NULL,NULL),(214,'TW','Taiwan',NULL,NULL),(215,'TJ','Tajikistan',NULL,NULL),(216,'TZ','Tanzania',NULL,NULL),(217,'TH','Thailand',NULL,NULL),(218,'TG','Togo',NULL,NULL),(219,'TK','Tokelau',NULL,NULL),(220,'TO','Tonga',NULL,NULL),(221,'TT','Trinidad And Tobago',NULL,NULL),(222,'TN','Tunisia',NULL,NULL),(223,'TR','Turkey',NULL,NULL),(224,'TM','Turkmenistan',NULL,NULL),(225,'TC','Turks And Caicos Islands',NULL,NULL),(226,'TV','Tuvalu',NULL,NULL),(227,'UG','Uganda',NULL,NULL),(228,'UA','Ukraine',NULL,NULL),(229,'AE','United Arab Emirates',NULL,NULL),(230,'GB','United Kingdom',NULL,NULL),(231,'US','United States',NULL,NULL),(232,'UM','United States Minor Outlying Islands',NULL,NULL),(233,'UY','Uruguay',NULL,NULL),(234,'UZ','Uzbekistan',NULL,NULL),(235,'VU','Vanuatu',NULL,NULL),(236,'VA','Vatican City State (Holy See)',NULL,NULL),(237,'VE','Venezuela',NULL,NULL),(238,'VN','Vietnam',NULL,NULL),(239,'VG','Virgin Islands (British)',NULL,NULL),(240,'VI','Virgin Islands (US)',NULL,NULL),(241,'WF','Wallis And Futuna Islands',NULL,NULL),(242,'EH','Western Sahara',NULL,NULL),(243,'YE','Yemen',NULL,NULL),(244,'YU','Yugoslavia',NULL,NULL),(245,'ZM','Zambia',NULL,NULL),(246,'ZW','Zimbabwe',NULL,NULL);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `currencies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` int DEFAULT NULL,
  `rate` decimal(65,8) DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `symbol` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` enum('left','right') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'left',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (2,NULL,NULL,'KES','KENYAN SHILLINGS','KES','left','2021-02-26 08:57:25','2021-02-26 08:57:25');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custom_fields` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `category` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `label` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `class` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `db_columns` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `default_values` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `required` tinyint NOT NULL DEFAULT '0',
  `active` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
INSERT INTO `custom_fields` VALUES (1,1,'add_client','textfield','CLIENT ID NUMBER','CLIENT ID NUMBER',NULL,NULL,NULL,NULL,NULL,1,1,'2021-03-08 00:47:47','2021-03-08 00:47:47');
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields_meta`
--

DROP TABLE IF EXISTS `custom_fields_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custom_fields_meta` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int NOT NULL,
  `custom_field_id` bigint unsigned NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields_meta`
--

LOCK TABLES `custom_fields_meta` WRITE;
/*!40000 ALTER TABLE `custom_fields_meta` DISABLE KEYS */;
INSERT INTO `custom_fields_meta` VALUES (1,'add_client',3,1,'456464646464','2021-03-08 01:05:21','2021-03-08 01:05:21'),(2,'add_client',4,1,'22876380','2021-03-08 14:21:19','2021-03-08 14:21:19');
/*!40000 ALTER TABLE `custom_fields_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense_types`
--

DROP TABLE IF EXISTS `expense_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `expense_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expense_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `asset_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense_types`
--

LOCK TABLES `expense_types` WRITE;
/*!40000 ALTER TABLE `expense_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `expenses` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `expense_type_id` bigint unsigned DEFAULT NULL,
  `expense_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `asset_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `date` date DEFAULT NULL,
  `recurring` tinyint NOT NULL DEFAULT '0',
  `recur_frequency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '31',
  `recur_start_date` date DEFAULT NULL,
  `recur_end_date` date DEFAULT NULL,
  `recur_next_date` date DEFAULT NULL,
  `recur_type` enum('day','week','month','year') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'month',
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `files` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenses`
--

LOCK TABLES `expenses` WRITE;
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funds`
--

DROP TABLE IF EXISTS `funds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `funds` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funds`
--

LOCK TABLES `funds` WRITE;
/*!40000 ALTER TABLE `funds` DISABLE KEYS */;
INSERT INTO `funds` VALUES (1,'SALENE CAPITAL');
/*!40000 ALTER TABLE `funds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income`
--

DROP TABLE IF EXISTS `income`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `income` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `income_type_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `income_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `asset_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `date` date DEFAULT NULL,
  `recurring` tinyint NOT NULL DEFAULT '0',
  `recur_frequency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '31',
  `recur_start_date` date DEFAULT NULL,
  `recur_end_date` date DEFAULT NULL,
  `recur_next_date` date DEFAULT NULL,
  `recur_type` enum('day','week','month','year') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'month',
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `files` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income`
--

LOCK TABLES `income` WRITE;
/*!40000 ALTER TABLE `income` DISABLE KEYS */;
/*!40000 ALTER TABLE `income` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income_types`
--

DROP TABLE IF EXISTS `income_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `income_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `income_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `asset_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income_types`
--

LOCK TABLES `income_types` WRITE;
/*!40000 ALTER TABLE `income_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `income_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `jobs_queue_index` (`queue`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,'default','{\"uuid\":\"9bff3a41-26d3-40d8-8f3d-62be92f0f774\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:0:\\\"\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1614344110,1614344110),(2,'default','{\"uuid\":\"022b5528-e494-47d2-85d7-aa1609d67843\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1614344136,1614344136),(3,'default','{\"uuid\":\"d0a1779e-5361-4470-bdc3-f89054d93445\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1614344157,1614344157),(4,'default','{\"uuid\":\"ecdda62f-138f-4bfb-951f-0f910c38ae64\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1614344171,1614344171),(5,'default','{\"uuid\":\"d9d6f388-33b5-47af-958f-fde11d8494d8\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1614344382,1614344382),(6,'default','{\"uuid\":\"7e4d4e0a-f474-4cdc-a763-ac63e2fc9dd9\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:0:\\\"\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615158513,1615158513),(7,'default','{\"uuid\":\"e5c3a6bc-2a1b-48ae-8149-f0d507d3f0b3\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615158519,1615158519),(8,'default','{\"uuid\":\"9559230e-ea22-46eb-a3ea-ae190300f21b\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615158539,1615158539),(9,'default','{\"uuid\":\"c3584520-748d-479e-8958-d5c112af9576\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:6:\\\"active\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615200563,1615200563),(10,'default','{\"uuid\":\"113ed671-3aa6-4072-b7fd-a165b50ab0f8\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615200573,1615200573),(11,'default','{\"uuid\":\"3660e807-b790-49a6-950e-e041556bc875\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615206949,1615206949),(12,'default','{\"uuid\":\"930546ea-f754-4857-b61a-660367d39195\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:6:\\\"active\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615207606,1615207606),(13,'default','{\"uuid\":\"6146fa77-9747-4120-82ba-06b09df8f889\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615207612,1615207612),(14,'default','{\"uuid\":\"0b1fd1a4-1d63-4eb0-b304-b788bc1dc07a\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:0:\\\"\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615214724,1615214724),(15,'default','{\"uuid\":\"066a9f11-c1cd-4db5-a322-eddccc764968\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615215281,1615215281),(16,'default','{\"uuid\":\"1f93dc1e-dc8b-4e94-bba5-56115692ebd3\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"rejected\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615215364,1615215364),(17,'default','{\"uuid\":\"e3c502f3-a211-4afc-a14c-dc78d556712a\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615215414,1615215414),(18,'default','{\"uuid\":\"48dd9e20-8e1d-4967-92c7-1faae207c3e3\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615215861,1615215861),(19,'default','{\"uuid\":\"ecc77aa2-3838-4cb8-96ca-6c1ef9b22384\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615215877,1615215877),(20,'default','{\"uuid\":\"0266ac0b-9e0a-4e25-8a61-4bcff76b5528\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:6:\\\"active\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615216262,1615216262),(21,'default','{\"uuid\":\"2d9a97ce-3d48-4842-985d-c839f0822ea3\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615216282,1615216282),(22,'default','{\"uuid\":\"418ad59a-2f5a-4567-98ec-9c96e617e0e0\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:6:\\\"active\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615216534,1615216534),(23,'default','{\"uuid\":\"62c647bc-ee22-4b1c-bf42-64c1cfa66783\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1615216574,1615216574),(24,'default','{\"uuid\":\"745c26b0-3ba6-4ed0-a9bc-733378b8b1ae\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:6:\\\"active\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1616236888,1616236888),(25,'default','{\"uuid\":\"1cd5d1a1-476f-46ea-8664-068094d7b2d9\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1616492720,1616492720),(26,'default','{\"uuid\":\"19b001bb-8094-48ed-ba6c-d568fee4d0d4\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1616494903,1616494903),(27,'default','{\"uuid\":\"dc3be740-eabd-4085-8f20-046e6da3bf69\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:3;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1616494918,1616494918),(28,'default','{\"uuid\":\"46f12919-915a-4927-8679-90ad99663a12\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1620216083,1620216083),(29,'default','{\"uuid\":\"9b530119-71be-4d3a-94f1-333445c14971\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:4;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:0:\\\"\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1620644939,1620644939),(30,'default','{\"uuid\":\"552244db-4121-4cd4-b86e-ecfd1b4fcb04\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:4;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:9:\\\"submitted\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1620644963,1620644963),(31,'default','{\"uuid\":\"291dd350-9ce9-410f-8760-c5e529804eae\",\"displayName\":\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":16:{s:5:\\\"class\\\";s:49:\\\"Modules\\\\Loan\\\\Listeners\\\\LoanStatusChangedCampaigns\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:37:\\\"Modules\\\\Loan\\\\Events\\\\LoanStatusChanged\\\":2:{s:4:\\\"loan\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:26:\\\"Modules\\\\Loan\\\\Entities\\\\Loan\\\";s:2:\\\"id\\\";i:4;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"charges\\\";i:1;s:12:\\\"loan_product\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:15:\\\"previous_status\\\";s:8:\\\"approved\\\";}}s:5:\\\"tries\\\";N;s:7:\\\"backoff\\\";N;s:10:\\\"retryUntil\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}',0,NULL,1620645946,1620645946);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `journal_entries`
--

DROP TABLE IF EXISTS `journal_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `journal_entries` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `transaction_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_detail_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `chart_of_account_id` bigint unsigned DEFAULT NULL,
  `transaction_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_sub_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `month` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` int DEFAULT NULL,
  `debit` decimal(65,4) DEFAULT NULL,
  `credit` decimal(65,4) DEFAULT NULL,
  `balance` decimal(65,4) DEFAULT NULL,
  `active` tinyint NOT NULL DEFAULT '1',
  `reversed` tinyint NOT NULL DEFAULT '0',
  `reversible` tinyint NOT NULL DEFAULT '1',
  `manual_entry` tinyint NOT NULL DEFAULT '0',
  `receipt` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `branch_id_index` (`branch_id`) USING BTREE,
  KEY `chart_of_account_id_index` (`chart_of_account_id`) USING BTREE,
  KEY `currency_id_index` (`currency_id`) USING BTREE,
  KEY `created_by_id_index` (`created_by_id`) USING BTREE,
  KEY `client_id_index` (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `journal_entries`
--

LOCK TABLES `journal_entries` WRITE;
/*!40000 ALTER TABLE `journal_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `journal_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_applications`
--

DROP TABLE IF EXISTS `loan_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_applications` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` bigint unsigned DEFAULT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned NOT NULL,
  `loan_id` bigint unsigned DEFAULT NULL,
  `loan_product_id` bigint unsigned NOT NULL,
  `amount` decimal(65,4) NOT NULL DEFAULT '0.0000',
  `status` enum('approved','pending','rejected') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_applications`
--

LOCK TABLES `loan_applications` WRITE;
/*!40000 ALTER TABLE `loan_applications` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_charge_options`
--

DROP TABLE IF EXISTS `loan_charge_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_charge_options` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_charge_options`
--

LOCK TABLES `loan_charge_options` WRITE;
/*!40000 ALTER TABLE `loan_charge_options` DISABLE KEYS */;
INSERT INTO `loan_charge_options` VALUES (1,'Flat','Flat',1),(2,'Principal due on installment','Principal due on installment',1),(3,'Principal + Interest due on installment','Principal + Interest due on installment',1),(4,'Interest due on installment','Interest due on installment',1),(5,'Total Outstanding Loan Principal','Total Outstanding Loan Principal',1),(6,'Percentage of Original Loan Principal per Installment','Percentage of Original Loan Principal per Installment',1),(7,'Original Loan Principal','Original Loan Principal',1);
/*!40000 ALTER TABLE `loan_charge_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_charge_types`
--

DROP TABLE IF EXISTS `loan_charge_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_charge_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_charge_types`
--

LOCK TABLES `loan_charge_types` WRITE;
/*!40000 ALTER TABLE `loan_charge_types` DISABLE KEYS */;
INSERT INTO `loan_charge_types` VALUES (1,'Disbursement','Disbursement',1),(2,'Specified Due Date','Specified Due Date',1),(3,'Installment Fees','Installment Fees',1),(4,'Overdue Installment Fee','Overdue Installment Fee',1),(5,'Disbursement - Paid With Repayment','Disbursement - Paid With Repayment',1),(6,'Loan Rescheduling Fee','Loan Rescheduling Fee',1),(7,'Overdue On Loan Maturity','Overdue On Loan Maturity',1),(8,'Last installment fee','Last installment fee',1);
/*!40000 ALTER TABLE `loan_charge_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_charges`
--

DROP TABLE IF EXISTS `loan_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `loan_charge_type_id` bigint unsigned NOT NULL,
  `loan_charge_option_id` bigint unsigned NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(65,2) NOT NULL,
  `min_amount` decimal(65,2) DEFAULT NULL,
  `max_amount` decimal(65,2) DEFAULT NULL,
  `payment_mode` enum('regular','account_transfer') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'regular',
  `schedule` tinyint DEFAULT '0',
  `schedule_frequency` int DEFAULT NULL,
  `schedule_frequency_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_penalty` tinyint DEFAULT '0',
  `active` tinyint NOT NULL DEFAULT '0',
  `allow_override` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_charge` bigint DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_charges`
--

LOCK TABLES `loan_charges` WRITE;
/*!40000 ALTER TABLE `loan_charges` DISABLE KEYS */;
INSERT INTO `loan_charges` VALUES (1,2,2,1,6,'LOAN APPLICATION FEES-5%',5.00,NULL,NULL,'regular',0,NULL,NULL,0,1,1,'2021-02-26 13:24:02','2021-03-08 14:56:31',NULL),(2,2,2,1,1,'CONVINIENCE FEES',1.00,NULL,NULL,'regular',0,NULL,NULL,0,1,1,'2021-02-26 13:25:52','2021-03-08 00:59:37',NULL),(3,2,2,2,1,'TRACKING FEES',20000.00,NULL,NULL,'regular',0,NULL,NULL,0,1,1,'2021-02-26 13:26:50','2021-03-08 01:00:05',NULL),(4,2,2,2,1,'CREDIT LIFE',1.00,NULL,NULL,'regular',0,NULL,NULL,0,1,1,'2021-02-26 13:27:22','2021-03-08 01:00:18',NULL),(5,2,2,2,1,'VALUATION FEES',4000.00,NULL,NULL,'regular',0,NULL,NULL,1,1,1,'2021-02-26 13:27:48','2021-03-08 01:00:31',NULL),(6,2,2,2,1,'LEGAL FEES',200.00,NULL,NULL,'regular',0,NULL,NULL,0,1,0,'2021-02-26 13:28:14','2021-02-26 14:20:46',NULL),(7,2,2,1,1,'BANK TO BANK CHARGES',150.00,NULL,NULL,'regular',0,NULL,NULL,0,1,1,'2021-02-26 14:04:39','2021-03-08 01:00:43',NULL),(8,2,2,1,1,'MOBILE TRANSFER CHARGES',200.00,NULL,NULL,'regular',0,NULL,NULL,0,1,0,'2021-02-26 14:06:27','2021-02-26 14:21:05',NULL),(9,2,2,1,1,'RTGS',650.00,NULL,NULL,'regular',0,NULL,NULL,0,1,1,'2021-02-26 14:07:11','2021-03-08 01:00:53',NULL),(10,2,2,4,2,'PENALTY',10.00,NULL,NULL,'regular',0,NULL,NULL,0,1,0,'2021-02-26 14:23:48','2021-02-26 14:23:48',NULL),(11,2,2,1,6,'LOAN APPLICATION FEES-3%',3.00,NULL,NULL,'regular',0,NULL,NULL,0,1,1,'2021-02-26 14:46:34','2021-03-08 15:10:47',NULL);
/*!40000 ALTER TABLE `loan_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_collateral`
--

DROP TABLE IF EXISTS `loan_collateral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_collateral` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `loan_id` bigint unsigned NOT NULL,
  `loan_collateral_type_id` bigint unsigned NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `value` decimal(65,6) DEFAULT NULL,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('active','repossessed','sold','closed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_collateral_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_collateral`
--

LOCK TABLES `loan_collateral` WRITE;
/*!40000 ALTER TABLE `loan_collateral` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_collateral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_collateral_types`
--

DROP TABLE IF EXISTS `loan_collateral_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_collateral_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_collateral_types`
--

LOCK TABLES `loan_collateral_types` WRITE;
/*!40000 ALTER TABLE `loan_collateral_types` DISABLE KEYS */;
INSERT INTO `loan_collateral_types` VALUES (1,'MOTORVEHICLE LOGBOOK'),(2,'CHATTELS'),(3,'SAVINGS'),(4,'TITLEDEED'),(5,'PERSONAL GUARANTEE'),(6,'MOU'),(7,'SHARES REGISTERED IN NSE');
/*!40000 ALTER TABLE `loan_collateral_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_credit_checks`
--

DROP TABLE IF EXISTS `loan_credit_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_credit_checks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `security_level` enum('block','pass','warning') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'warning',
  `rating_type` enum('boolean','score') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'boolean',
  `pass_min_amount` decimal(65,6) DEFAULT NULL,
  `pass_max_amount` decimal(65,6) DEFAULT NULL,
  `warn_min_amount` decimal(65,6) DEFAULT NULL,
  `warn_max_amount` decimal(65,6) DEFAULT NULL,
  `fail_min_amount` decimal(65,6) DEFAULT NULL,
  `fail_max_amount` decimal(65,6) DEFAULT NULL,
  `general_error_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_friendly_error_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `general_warning_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_friendly_warning_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `general_success_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_friendly_success_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_credit_checks_created_by_id_foreign` (`created_by_id`) USING BTREE,
  CONSTRAINT `loan_credit_checks_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_credit_checks`
--

LOCK TABLES `loan_credit_checks` WRITE;
/*!40000 ALTER TABLE `loan_credit_checks` DISABLE KEYS */;
INSERT INTO `loan_credit_checks` VALUES (1,NULL,'Client Written-Off Loans Check','Client Written-Off Loans Check','block','boolean',NULL,NULL,NULL,NULL,NULL,NULL,'The client has one or more written-off loans','The client has one or more written-off loans','The client has one or more written-off loans','The client has one or more written-off loans','The client has one or more written-off loans','The client has one or more written-off loans',0,NULL,NULL),(2,NULL,'Same Product Outstanding','Same Product Outstanding','block','boolean',NULL,NULL,NULL,NULL,NULL,NULL,'The client has an active loan for the same product','The client has an active loan for the same product','The client has an active loan for the same product','The client has an active loan for the same product','The client has an active loan for the same product','The client has an active loan for the same product',0,NULL,NULL),(3,NULL,'Client Arrears','Client Arrears','block','boolean',NULL,NULL,NULL,NULL,NULL,NULL,'Client has arrears on existing loans','Client has arrears on existing loans','Client has arrears on existing loans','Client has arrears on existing loans','Client has arrears on existing loans','Client has arrears on existing loans',0,NULL,NULL),(4,NULL,'Outstanding Loan Balance','Outstanding Loan Balance','block','boolean',NULL,NULL,NULL,NULL,NULL,NULL,'Client has outstanding balance on existing loans','Client has outstanding balance on existing loans','Client has outstanding balance on existing loans','Client has outstanding balance on existing loans','Client has outstanding balance on existing loans','Client has outstanding balance on existing loans',0,NULL,NULL),(5,NULL,'Rejected and withdrawn loans','Rejected and withdrawn loans','block','boolean',NULL,NULL,NULL,NULL,NULL,NULL,'This client has had one or more rejected or withdrawn loans','This client has had one or more rejected or withdrawn loans','This client has had one or more rejected or withdrawn loans','This client has had one or more rejected or withdrawn loans','This client has had one or more rejected or withdrawn loans','This client has had one or more rejected or withdrawn loans',0,NULL,NULL),(6,NULL,'Total collateral items value','Total collateral items value','block','boolean',NULL,NULL,NULL,NULL,NULL,NULL,'The total value of collateral items for this loan is less than the principal loanamount','The total value of collateral items for this loan is less than the principal loanamount','The total value of collateral items for this loan is less than the principal loanamount','The total value of collateral items for this loan is less than the principal loanamount','The total value of collateral items for this loan is less than the principal loanamount','The total value of collateral items for this loan is less than the principal loanamount',0,NULL,NULL);
/*!40000 ALTER TABLE `loan_credit_checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_disbursement_channels`
--

DROP TABLE IF EXISTS `loan_disbursement_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_disbursement_channels` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_system` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_disbursement_channels`
--

LOCK TABLES `loan_disbursement_channels` WRITE;
/*!40000 ALTER TABLE `loan_disbursement_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_disbursement_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_files`
--

DROP TABLE IF EXISTS `loan_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_files` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `loan_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `size` int DEFAULT NULL,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_files_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_files`
--

LOCK TABLES `loan_files` WRITE;
/*!40000 ALTER TABLE `loan_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_guarantors`
--

DROP TABLE IF EXISTS `loan_guarantors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_guarantors` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `loan_id` bigint unsigned DEFAULT NULL,
  `is_client` tinyint NOT NULL DEFAULT '0',
  `client_id` bigint unsigned DEFAULT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','other','unspecified') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'unspecified',
  `status` enum('pending','active','inactive','deceased','unspecified','closed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `marital_status` enum('married','single','divorced','widowed','unspecified','other') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'unspecified',
  `country_id` bigint unsigned DEFAULT NULL,
  `title_id` bigint unsigned DEFAULT NULL,
  `profession_id` bigint unsigned DEFAULT NULL,
  `client_relationship_id` bigint unsigned DEFAULT NULL,
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_date` date DEFAULT NULL,
  `joined_date` date DEFAULT NULL,
  `guaranteed_amount` decimal(65,6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_guarantors_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_guarantors`
--

LOCK TABLES `loan_guarantors` WRITE;
/*!40000 ALTER TABLE `loan_guarantors` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_guarantors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_history`
--

DROP TABLE IF EXISTS `loan_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_history` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `loan_id` bigint unsigned NOT NULL,
  `created_by_id` bigint unsigned NOT NULL,
  `action` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_history_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_history`
--

LOCK TABLES `loan_history` WRITE;
/*!40000 ALTER TABLE `loan_history` DISABLE KEYS */;
INSERT INTO `loan_history` VALUES (1,1,2,'Loan Created','MERCY KIRIMI','2021-02-26 14:55:10','2021-02-26 14:55:10'),(2,1,2,'Loan Approved','MERCY KIRIMI','2021-02-26 14:55:36','2021-02-26 14:55:36'),(3,1,2,'Loan Unapproved','MERCY KIRIMI','2021-02-26 14:55:57','2021-02-26 14:55:57'),(4,1,2,'Loan Approved','MERCY KIRIMI','2021-02-26 14:56:11','2021-02-26 14:56:11'),(5,1,2,'Loan Disbursed','MERCY KIRIMI','2021-02-26 14:59:42','2021-02-26 14:59:42'),(6,2,1,'Loan Created','Admin Admin','2021-03-08 01:08:33','2021-03-08 01:08:33'),(7,2,1,'Loan Approved','Admin Admin','2021-03-08 01:08:39','2021-03-08 01:08:39'),(8,2,1,'Loan Disbursed','Admin Admin','2021-03-08 01:08:59','2021-03-08 01:08:59'),(9,1,2,'Loan Undisbursed','MERCY KIRIMI','2021-03-08 12:49:23','2021-03-08 12:49:23'),(10,1,2,'Loan Unapproved','MERCY KIRIMI','2021-03-08 12:49:33','2021-03-08 12:49:33'),(11,1,1,'Loan Rejected','Admin Admin','2021-03-08 14:35:49','2021-03-08 14:35:49'),(12,2,1,'Loan Undisbursed','Admin Admin','2021-03-08 14:46:46','2021-03-08 14:46:46'),(13,2,1,'Loan Unapproved','Admin Admin','2021-03-08 14:46:52','2021-03-08 14:46:52'),(14,3,4,'Loan Created','BONIFACE ITABARI','2021-03-08 16:45:24','2021-03-08 16:45:24'),(15,3,4,'Loan Approved','BONIFACE ITABARI','2021-03-08 16:54:41','2021-03-08 16:54:41'),(16,1,1,'Loan Unrejected','Admin Admin','2021-03-08 16:56:04','2021-03-08 16:56:04'),(17,3,1,'Loan Disbursed','Admin Admin','2021-03-08 16:56:54','2021-03-08 16:56:54'),(18,2,1,'Loan Approved','Admin Admin','2021-03-08 17:04:21','2021-03-08 17:04:21'),(19,2,1,'Loan Disbursed','Admin Admin','2021-03-08 17:04:37','2021-03-08 17:04:37'),(20,3,2,'Loan Undisbursed','MERCY KIRIMI','2021-03-08 17:11:02','2021-03-08 17:11:02'),(21,3,2,'Loan Disbursed','MERCY KIRIMI','2021-03-08 17:11:22','2021-03-08 17:11:22'),(22,3,2,'Loan Undisbursed','MERCY KIRIMI','2021-03-08 17:15:34','2021-03-08 17:15:34'),(23,3,2,'Loan Disbursed','MERCY KIRIMI','2021-03-08 17:16:14','2021-03-08 17:16:14'),(24,3,1,'Loan Undisbursed','Admin Admin','2021-03-20 12:41:26','2021-03-20 12:41:26'),(25,3,1,'Loan Unapproved','Admin Admin','2021-03-23 11:45:19','2021-03-23 11:45:19'),(26,3,1,'Loan Approved','Admin Admin','2021-03-23 12:21:43','2021-03-23 12:21:43'),(27,3,1,'Loan Disbursed','Admin Admin','2021-03-23 12:21:57','2021-03-23 12:21:57'),(28,1,1,'Loan Approved','Admin Admin','2021-05-05 14:01:23','2021-05-05 14:01:23'),(29,4,1,'Loan Created','Admin Admin','2021-05-10 13:08:59','2021-05-10 13:08:59'),(30,4,1,'Loan Approved','Admin Admin','2021-05-10 13:09:23','2021-05-10 13:09:23'),(31,4,1,'Loan Disbursed','Admin Admin','2021-05-10 13:25:46','2021-05-10 13:25:46');
/*!40000 ALTER TABLE `loan_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_linked_charges`
--

DROP TABLE IF EXISTS `loan_linked_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_linked_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `loan_id` bigint unsigned NOT NULL,
  `loan_charge_id` bigint unsigned NOT NULL,
  `loan_charge_type_id` bigint unsigned DEFAULT NULL,
  `loan_charge_option_id` bigint unsigned DEFAULT NULL,
  `loan_transaction_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` decimal(65,6) NOT NULL,
  `calculated_amount` decimal(65,6) DEFAULT NULL,
  `amount_paid_derived` decimal(65,6) DEFAULT NULL,
  `amount_waived_derived` decimal(65,6) DEFAULT NULL,
  `amount_written_off_derived` decimal(65,6) DEFAULT NULL,
  `amount_outstanding_derived` decimal(65,6) DEFAULT NULL,
  `is_penalty` tinyint NOT NULL DEFAULT '0',
  `waived` tinyint NOT NULL DEFAULT '0',
  `is_paid` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_linked_charges_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_linked_charges`
--

LOCK TABLES `loan_linked_charges` WRITE;
/*!40000 ALTER TABLE `loan_linked_charges` DISABLE KEYS */;
INSERT INTO `loan_linked_charges` VALUES (1,1,11,1,7,NULL,'LOAN APPLICATION FEES-3%',3.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-02-26 14:55:10','2021-03-08 12:49:23'),(2,1,10,4,2,NULL,'PENALTY',10.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-02-26 14:55:10','2021-03-08 12:49:23'),(3,1,2,1,1,NULL,'CONVINIENCE FEES',1.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-02-26 14:55:10','2021-03-08 12:49:23'),(4,1,4,2,1,NULL,'CREDIT LIFE',1.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-02-26 14:55:10','2021-03-08 12:49:23'),(5,1,7,1,1,NULL,'BANK TO BANK CHARGES',150.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-02-26 14:55:10','2021-03-08 12:49:23'),(15,3,1,1,6,NULL,'LOAN APPLICATION FEES-5%',5.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 16:45:24','2021-03-20 12:41:26'),(16,3,3,2,1,NULL,'TRACKING FEES',20000.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 16:45:24','2021-03-20 12:41:26'),(17,3,4,2,1,NULL,'CREDIT LIFE',3624.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 16:45:24','2021-03-20 12:41:26'),(18,3,10,4,2,NULL,'PENALTY',10.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 16:45:24','2021-03-20 12:41:26'),(19,3,6,2,1,NULL,'LEGAL FEES',200.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 16:45:24','2021-03-20 12:41:26'),(20,3,5,2,1,NULL,'VALUATION FEES',3500.000000,NULL,NULL,NULL,NULL,NULL,1,0,0,'2021-03-08 16:45:24','2021-03-20 12:41:26'),(21,3,9,1,1,NULL,'RTGS',650.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 16:45:24','2021-03-20 12:41:26'),(22,1,6,2,1,NULL,'LEGAL FEES',200.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 16:56:23','2021-03-08 16:56:23'),(23,2,1,1,6,NULL,'LOAN APPLICATION FEES-5%',5.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 17:04:15','2021-03-08 17:04:15'),(24,2,4,2,1,NULL,'CREDIT LIFE',1.000000,NULL,NULL,NULL,NULL,NULL,0,1,0,'2021-03-08 17:04:15','2021-03-08 17:19:55'),(25,2,5,2,1,NULL,'VALUATION FEES',4000.000000,NULL,NULL,NULL,NULL,NULL,1,0,0,'2021-03-08 17:04:15','2021-03-08 17:04:15'),(26,2,7,1,1,NULL,'BANK TO BANK CHARGES',150.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-03-08 17:04:15','2021-03-08 17:04:15'),(27,4,3,2,1,NULL,'TRACKING FEES',20000.000000,NULL,NULL,NULL,NULL,NULL,0,1,0,'2021-05-10 13:08:59','2021-05-10 13:26:15'),(28,4,1,1,6,NULL,'LOAN APPLICATION FEES-5%',5.000000,NULL,NULL,NULL,NULL,NULL,0,0,0,'2021-05-10 13:08:59','2021-05-10 13:08:59'),(29,4,5,2,1,NULL,'VALUATION FEES',4000.000000,NULL,NULL,NULL,NULL,NULL,1,0,0,'2021-05-10 13:08:59','2021-05-10 13:08:59');
/*!40000 ALTER TABLE `loan_linked_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_linked_credit_checks`
--

DROP TABLE IF EXISTS `loan_linked_credit_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_linked_credit_checks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `loan_id` bigint unsigned NOT NULL,
  `loan_credit_check_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_linked_credit_checks`
--

LOCK TABLES `loan_linked_credit_checks` WRITE;
/*!40000 ALTER TABLE `loan_linked_credit_checks` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_linked_credit_checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_notes`
--

DROP TABLE IF EXISTS `loan_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_notes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `loan_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_notes_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_notes`
--

LOCK TABLES `loan_notes` WRITE;
/*!40000 ALTER TABLE `loan_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_officer_history`
--

DROP TABLE IF EXISTS `loan_officer_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_officer_history` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `loan_id` bigint unsigned NOT NULL,
  `created_by_id` bigint unsigned NOT NULL,
  `loan_officer_id` bigint unsigned NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_officer_history_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_officer_history`
--

LOCK TABLES `loan_officer_history` WRITE;
/*!40000 ALTER TABLE `loan_officer_history` DISABLE KEYS */;
INSERT INTO `loan_officer_history` VALUES (1,1,2,2,'2021-02-26',NULL,'2021-02-26 14:55:10','2021-02-26 14:55:10'),(2,2,1,3,'2021-03-08',NULL,'2021-03-08 01:08:33','2021-03-08 01:08:33'),(3,3,4,2,'2021-03-08',NULL,'2021-03-08 16:45:24','2021-03-08 16:45:24'),(4,3,4,4,'2021-03-08',NULL,'2021-03-08 16:54:56','2021-03-08 16:54:56'),(5,4,1,2,'2021-05-10',NULL,'2021-05-10 13:08:59','2021-05-10 13:08:59');
/*!40000 ALTER TABLE `loan_officer_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_product_linked_charges`
--

DROP TABLE IF EXISTS `loan_product_linked_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_product_linked_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `loan_product_id` bigint unsigned NOT NULL,
  `loan_charge_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_product_linked_charges`
--

LOCK TABLES `loan_product_linked_charges` WRITE;
/*!40000 ALTER TABLE `loan_product_linked_charges` DISABLE KEYS */;
INSERT INTO `loan_product_linked_charges` VALUES (142,3,1,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(143,3,2,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(144,3,3,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(145,3,4,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(146,3,5,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(147,3,6,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(148,3,7,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(149,3,8,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(150,3,9,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(151,3,10,'2021-02-26 14:25:42','2021-02-26 14:25:42'),(170,4,1,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(171,4,2,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(172,4,4,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(173,4,6,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(174,4,5,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(175,4,7,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(176,4,8,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(177,4,9,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(178,4,10,'2021-02-26 14:26:18','2021-02-26 14:26:18'),(187,5,1,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(188,5,2,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(189,5,4,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(190,5,6,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(191,5,7,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(192,5,8,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(193,5,9,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(194,5,10,'2021-02-26 14:26:54','2021-02-26 14:26:54'),(203,6,1,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(204,6,2,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(205,6,3,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(206,6,4,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(207,6,5,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(208,6,6,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(209,6,7,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(210,6,8,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(211,6,9,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(212,6,10,'2021-02-26 14:27:38','2021-02-26 14:27:38'),(221,7,1,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(222,7,2,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(223,7,4,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(224,7,6,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(225,7,7,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(226,7,8,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(227,7,9,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(228,7,10,'2021-02-26 14:27:52','2021-02-26 14:27:52'),(229,8,1,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(230,8,2,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(231,8,4,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(232,8,6,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(233,8,7,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(234,8,8,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(235,8,9,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(236,8,10,'2021-02-26 14:28:13','2021-02-26 14:28:13'),(237,9,4,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(238,9,1,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(239,9,2,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(240,9,3,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(241,9,7,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(242,9,8,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(243,9,9,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(244,9,10,'2021-02-26 14:28:55','2021-02-26 14:28:55'),(245,10,4,'2021-02-26 14:36:20','2021-02-26 14:36:20'),(246,10,7,'2021-02-26 14:36:20','2021-02-26 14:36:20'),(247,11,1,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(248,11,2,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(249,11,3,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(250,11,4,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(251,11,5,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(252,11,6,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(253,11,7,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(254,11,8,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(255,11,9,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(256,11,10,'2021-02-26 14:38:59','2021-02-26 14:38:59'),(257,1,1,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(258,1,2,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(259,1,3,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(260,1,4,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(261,1,5,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(262,1,6,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(263,1,10,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(264,1,7,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(265,1,8,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(266,1,9,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(267,1,11,'2021-02-26 14:50:40','2021-02-26 14:50:40'),(268,2,1,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(269,2,2,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(270,2,3,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(271,2,4,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(272,2,5,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(273,2,6,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(274,2,7,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(275,2,8,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(276,2,9,'2021-03-08 14:49:00','2021-03-08 14:49:00'),(277,2,10,'2021-03-08 14:49:00','2021-03-08 14:49:00');
/*!40000 ALTER TABLE `loan_product_linked_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_product_linked_credit_checks`
--

DROP TABLE IF EXISTS `loan_product_linked_credit_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_product_linked_credit_checks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `loan_product_id` bigint unsigned NOT NULL,
  `loan_credit_check_id` bigint unsigned NOT NULL,
  `check_order` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_product_linked_credit_checks`
--

LOCK TABLES `loan_product_linked_credit_checks` WRITE;
/*!40000 ALTER TABLE `loan_product_linked_credit_checks` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_product_linked_credit_checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_products`
--

DROP TABLE IF EXISTS `loan_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `loan_disbursement_channel_id` bigint unsigned DEFAULT NULL,
  `loan_transaction_processing_strategy_id` bigint unsigned NOT NULL,
  `fund_id` bigint unsigned NOT NULL,
  `fund_source_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `loan_portfolio_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `interest_receivable_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `penalties_receivable_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `fees_receivable_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `fees_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `overpayments_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `suspended_income_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_interest_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_penalties_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_fees_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_recovery_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `losses_written_off_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `interest_written_off_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `decimals` int DEFAULT NULL,
  `instalment_multiple_of` int DEFAULT '1',
  `minimum_principal` decimal(65,2) NOT NULL,
  `default_principal` decimal(65,2) NOT NULL,
  `maximum_principal` decimal(65,2) NOT NULL,
  `minimum_loan_term` int NOT NULL,
  `default_loan_term` int NOT NULL,
  `maximum_loan_term` int NOT NULL,
  `repayment_frequency` int NOT NULL,
  `repayment_frequency_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum_interest_rate` decimal(65,2) NOT NULL,
  `default_interest_rate` decimal(65,2) NOT NULL,
  `maximum_interest_rate` decimal(65,2) NOT NULL,
  `interest_rate_type` enum('day','week','month','year') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable_balloon_payments` tinyint NOT NULL DEFAULT '0',
  `allow_schedule_adjustments` tinyint NOT NULL DEFAULT '0',
  `grace_on_principal_paid` int NOT NULL DEFAULT '0',
  `grace_on_interest_paid` int NOT NULL DEFAULT '0',
  `grace_on_interest_charged` int NOT NULL DEFAULT '0',
  `allow_custom_grace_period` tinyint NOT NULL DEFAULT '0',
  `allow_topup` tinyint NOT NULL DEFAULT '0',
  `interest_methodology` enum('flat','declining_balance') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_recalculation` tinyint NOT NULL DEFAULT '0',
  `amortization_method` enum('equal_installments','equal_principal_payments') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interest_calculation_period_type` enum('daily','same') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days_in_year` enum('actual','360','365','364') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'actual',
  `days_in_month` enum('actual','30','31') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'actual',
  `include_in_loan_cycle` tinyint NOT NULL DEFAULT '0',
  `lock_guarantee_funds` tinyint NOT NULL DEFAULT '0',
  `auto_allocate_overpayments` tinyint NOT NULL DEFAULT '0',
  `allow_additional_charges` tinyint NOT NULL DEFAULT '0',
  `auto_disburse` tinyint NOT NULL DEFAULT '0',
  `require_linked_savings_account` tinyint NOT NULL DEFAULT '0',
  `min_amount` decimal(65,2) DEFAULT NULL,
  `max_amount` decimal(65,2) DEFAULT NULL,
  `accounting_rule` enum('none','cash','accrual_periodic','accrual_upfront') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `npa_overdue_days` int NOT NULL DEFAULT '0',
  `npa_suspend_accrued_income` tinyint NOT NULL DEFAULT '0',
  `active` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_products`
--

LOCK TABLES `loan_products` WRITE;
/*!40000 ALTER TABLE `loan_products` DISABLE KEYS */;
INSERT INTO `loan_products` VALUES (1,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'BIASHARA PLUS','BP+','ON REDUCING BALANCE ADVANCES',0,1,500000.00,1000000.00,3000000.00,1,6,12,1,'months',5.00,6.00,10.00,'month',0,0,0,0,0,0,0,'declining_balance',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:17:09','2021-02-26 13:48:48'),(2,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'BIASHARA LOAN','BL','FLAT RATE',0,1,50000.00,100000.00,3000000.00,1,6,36,1,'months',3.50,6.00,10.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:20:52','2021-02-26 13:48:02'),(3,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ASSET FINANCE','AF','ASSET ACQUISATION',0,1,100000.00,200000.00,2000000.00,1,6,36,1,'months',3.00,5.00,10.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:34:15','2021-02-26 13:47:47'),(4,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'PAY DAY ADVANCE','PD','SALARIED CLIENTS',0,1,10000.00,50000.00,200000.00,1,6,12,1,'months',5.00,6.00,10.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:37:20','2021-02-26 13:47:34'),(5,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'JIJENGE LOAN','JL','UNSECURED BUSINESS LOANS',NULL,1,5000.00,20000.00,300000.00,1,6,12,1,'months',6.00,10.00,20.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:41:03','2021-02-26 13:47:23'),(6,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'LPO FINANCING','LPO','LPO',0,1,100000.00,200000.00,2000000.00,3,6,12,1,'months',5.00,6.00,10.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:47:08','2021-02-26 13:47:08'),(7,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ELIMU LOAN','EL','FOR SCHOOL FEES',0,1,5000.00,50000.00,500000.00,1,6,12,1,'months',5.00,6.00,20.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:53:12','2021-02-26 13:53:12'),(8,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'EMERGENCY LOAN','EL','FOR MEDICAL OR OTHER NATURAL CALAMITY',0,1,10000.00,50000.00,100000.00,1,2,3,1,'months',5.00,6.00,10.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:55:40','2021-02-26 13:55:40'),(9,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'STAFF PERSONAL LOAN','SPL','FOR SALENE STAFF ONLY',0,1,10000.00,50000.00,100000.00,1,6,12,1,'months',1.00,2.00,3.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 13:57:51','2021-02-26 13:58:38'),(10,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'STAFF ASSET FINANCE LOAN','SAF','ASSET ACQUISATION',0,1,50000.00,100000.00,2000000.00,1,6,48,1,'months',1.00,1.50,2.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 14:36:20','2021-02-26 14:36:20'),(11,2,2,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'AGRICULTURAL LOANS','AL','AGRICULTURE',0,1,10000.00,50000.00,2000000.00,1,6,12,1,'months',5.00,6.00,10.00,'month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,0,NULL,NULL,'none',0,0,1,'2021-02-26 14:38:59','2021-02-26 14:38:59');
/*!40000 ALTER TABLE `loan_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_purposes`
--

DROP TABLE IF EXISTS `loan_purposes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_purposes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_purposes`
--

LOCK TABLES `loan_purposes` WRITE;
/*!40000 ALTER TABLE `loan_purposes` DISABLE KEYS */;
INSERT INTO `loan_purposes` VALUES (1,'CAR PURCHASE'),(2,'INCREASING BUSINESS STOCK'),(3,'SCHOOL FEES'),(4,'TOP UP'),(5,'BUY OFF'),(6,'LPO FINANCING'),(7,'MEDICAL'),(8,'CONSTRUCTION'),(9,'OTHERS'),(10,'OTHERS'),(11,'OTHERS');
/*!40000 ALTER TABLE `loan_purposes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_repayment_schedules`
--

DROP TABLE IF EXISTS `loan_repayment_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_repayment_schedules` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `loan_id` bigint unsigned DEFAULT NULL,
  `paid_by_date` date DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `due_date` date NOT NULL,
  `installment` int DEFAULT NULL,
  `principal` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `principal_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `principal_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_waived_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_waived_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_waived_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_due` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `month` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_repayment_schedules_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_repayment_schedules`
--

LOCK TABLES `loan_repayment_schedules` WRITE;
/*!40000 ALTER TABLE `loan_repayment_schedules` DISABLE KEYS */;
INSERT INTO `loan_repayment_schedules` VALUES (15,1,2,NULL,'2021-03-08','2021-04-08',1,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'04','2021','2021-03-08 17:04:37','2021-03-08 17:04:37'),(16,1,2,NULL,'2021-04-09','2021-05-08',2,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'05','2021','2021-03-08 17:04:37','2021-03-08 17:04:37'),(17,1,2,NULL,'2021-05-09','2021-06-08',3,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'06','2021','2021-03-08 17:04:37','2021-03-08 17:04:37'),(18,1,2,NULL,'2021-06-09','2021-07-08',4,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'07','2021','2021-03-08 17:04:37','2021-03-08 17:04:37'),(19,1,2,NULL,'2021-07-09','2021-08-08',5,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'08','2021','2021-03-08 17:04:37','2021-03-08 17:04:37'),(20,1,2,NULL,'2021-08-09','2021-09-08',6,16665.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22665.000000,'09','2021','2021-03-08 17:04:37','2021-03-08 17:04:37'),(25,1,3,NULL,'2021-03-08','2021-06-10',1,600000.000000,0.000000,0.000000,60000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,660000.000000,'06','2021','2021-03-23 12:21:57','2021-03-23 12:21:57'),(26,1,3,NULL,'2021-06-11','2021-09-10',2,600000.000000,0.000000,0.000000,60000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,660000.000000,'09','2021','2021-03-23 12:21:57','2021-03-23 12:21:57'),(27,1,4,NULL,'2021-05-10','2021-06-10',1,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'06','2021','2021-05-10 13:25:46','2021-05-10 13:25:46'),(28,1,4,NULL,'2021-06-11','2021-07-10',2,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'07','2021','2021-05-10 13:25:46','2021-05-10 13:25:46'),(29,1,4,NULL,'2021-07-11','2021-08-10',3,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'08','2021','2021-05-10 13:25:46','2021-05-10 13:25:46'),(30,1,4,NULL,'2021-08-11','2021-09-10',4,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'09','2021','2021-05-10 13:25:46','2021-05-10 13:25:46'),(31,1,4,NULL,'2021-09-11','2021-10-10',5,16667.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22667.000000,'10','2021','2021-05-10 13:25:46','2021-05-10 13:25:46'),(32,1,4,NULL,'2021-10-11','2021-11-10',6,16665.000000,0.000000,0.000000,6000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,22665.000000,'11','2021','2021-05-10 13:25:46','2021-05-10 13:25:46');
/*!40000 ALTER TABLE `loan_repayment_schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_transaction_processing_strategies`
--

DROP TABLE IF EXISTS `loan_transaction_processing_strategies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_transaction_processing_strategies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_transaction_processing_strategies`
--

LOCK TABLES `loan_transaction_processing_strategies` WRITE;
/*!40000 ALTER TABLE `loan_transaction_processing_strategies` DISABLE KEYS */;
INSERT INTO `loan_transaction_processing_strategies` VALUES (1,'Penalties, Fees, Interest, Principal order','Penalties, Fees, Interest, Principal order',1),(2,'Principal, Interest, Penalties, Fees Order','Principal, Interest, Penalties, Fees Order',1),(3,'Interest, Principal, Penalties, Fees Order','Interest, Principal, Penalties, Fees Order',1);
/*!40000 ALTER TABLE `loan_transaction_processing_strategies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_transaction_types`
--

DROP TABLE IF EXISTS `loan_transaction_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_transaction_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_transaction_types`
--

LOCK TABLES `loan_transaction_types` WRITE;
/*!40000 ALTER TABLE `loan_transaction_types` DISABLE KEYS */;
INSERT INTO `loan_transaction_types` VALUES (1,'Disbursement','Disbursement',1),(2,'Repayment','Repayment',1),(3,'Contra','Contra',1),(4,'Waive Interest','Waive Interest',1),(5,'Repayment At Disbursement','Repayment At Disbursement',1),(6,'Write Off','Write Off',1),(7,'Marked for Rescheduling','Marked for Rescheduling',1),(8,'Recovery Repayment','Recovery Repayment',1),(9,'Waive Charges','Waive Charges',1),(10,'Apply Charges','Apply Charges',1),(11,'Apply Interest','Apply Interest',1);
/*!40000 ALTER TABLE `loan_transaction_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_transactions`
--

DROP TABLE IF EXISTS `loan_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `loan_id` bigint unsigned NOT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `payment_detail_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` decimal(65,6) NOT NULL,
  `credit` decimal(65,6) DEFAULT NULL,
  `debit` decimal(65,6) DEFAULT NULL,
  `principal_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `loan_transaction_type_id` bigint unsigned NOT NULL,
  `reversed` tinyint NOT NULL DEFAULT '0',
  `reversible` tinyint NOT NULL DEFAULT '0',
  `submitted_on` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `status` enum('pending','approved','declined') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `payment_gateway_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `online_transaction` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `loan_transactions_loan_id_index` (`loan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_transactions`
--

LOCK TABLES `loan_transactions` WRITE;
/*!40000 ALTER TABLE `loan_transactions` DISABLE KEYS */;
INSERT INTO `loan_transactions` VALUES (11,2,1,2,4,'Disbursement',100000.000000,NULL,100000.000000,0.000000,0.000000,0.000000,0.000000,1,0,0,'2021-03-08',NULL,'2021-03-08',NULL,NULL,NULL,NULL,NULL,0,'2021-03-08 17:04:37','2021-03-08 17:04:37'),(12,2,1,2,NULL,'Interest Applied',36000.000000,NULL,36000.000000,0.000000,0.000000,0.000000,0.000000,11,0,0,'2021-03-08',NULL,'2021-03-08',NULL,NULL,NULL,NULL,NULL,0,'2021-03-08 17:04:37','2021-03-08 17:04:37'),(13,2,1,2,NULL,'Disbursement Charges',5150.000000,5150.000000,NULL,0.000000,0.000000,5150.000000,0.000000,5,0,0,'2021-03-08',NULL,'2021-03-08',NULL,NULL,NULL,NULL,NULL,0,'2021-03-08 17:04:37','2021-03-08 17:04:37'),(21,3,1,2,7,'Disbursement',1200000.000000,NULL,1200000.000000,0.000000,0.000000,0.000000,0.000000,1,0,0,'2021-03-08',NULL,'2021-03-23',NULL,NULL,NULL,NULL,NULL,0,'2021-03-23 12:21:57','2021-03-23 12:21:57'),(22,3,1,2,NULL,'Interest Applied',120000.000000,NULL,120000.000000,0.000000,0.000000,0.000000,0.000000,11,0,0,'2021-03-08',NULL,'2021-03-23',NULL,NULL,NULL,NULL,NULL,0,'2021-03-23 12:21:57','2021-03-23 12:21:57'),(23,3,1,2,NULL,'Disbursement Charges',60650.000000,60650.000000,NULL,0.000000,0.000000,60650.000000,0.000000,5,0,0,'2021-03-08',NULL,'2021-03-23',NULL,NULL,NULL,NULL,NULL,0,'2021-03-23 12:21:57','2021-03-23 12:21:57'),(24,4,1,2,8,'Disbursement',100000.000000,NULL,100000.000000,0.000000,0.000000,0.000000,0.000000,1,0,0,'2021-05-10',NULL,'2021-05-10',NULL,NULL,NULL,NULL,NULL,0,'2021-05-10 13:25:46','2021-05-10 13:25:46'),(25,4,1,2,NULL,'Interest Applied',36000.000000,NULL,36000.000000,0.000000,0.000000,0.000000,0.000000,11,0,0,'2021-05-10',NULL,'2021-05-10',NULL,NULL,NULL,NULL,NULL,0,'2021-05-10 13:25:46','2021-05-10 13:25:46'),(26,4,1,2,NULL,'Disbursement Charges',5000.000000,5000.000000,NULL,0.000000,0.000000,5000.000000,0.000000,5,0,0,'2021-05-10',NULL,'2021-05-10',NULL,NULL,NULL,NULL,NULL,0,'2021-05-10 13:25:46','2021-05-10 13:25:46');
/*!40000 ALTER TABLE `loan_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loans`
--

DROP TABLE IF EXISTS `loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loans` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_type` enum('client','group') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'client',
  `client_id` bigint unsigned DEFAULT NULL,
  `group_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `loan_product_id` bigint unsigned NOT NULL,
  `loan_transaction_processing_strategy_id` bigint unsigned NOT NULL,
  `fund_id` bigint unsigned NOT NULL,
  `loan_purpose_id` bigint unsigned NOT NULL,
  `loan_officer_id` bigint unsigned NOT NULL,
  `linked_savings_id` bigint unsigned DEFAULT NULL,
  `loan_disbursement_channel_id` bigint unsigned DEFAULT NULL,
  `submitted_on_date` date DEFAULT NULL,
  `submitted_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_on_date` date DEFAULT NULL,
  `approved_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `expected_disbursement_date` date DEFAULT NULL,
  `expected_first_payment_date` date DEFAULT NULL,
  `first_payment_date` date DEFAULT NULL,
  `expected_maturity_date` date DEFAULT NULL,
  `disbursed_on_date` date DEFAULT NULL,
  `disbursed_by_user_id` bigint unsigned DEFAULT NULL,
  `disbursed_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rejected_on_date` date DEFAULT NULL,
  `rejected_by_user_id` bigint unsigned DEFAULT NULL,
  `rejected_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `written_off_on_date` date DEFAULT NULL,
  `written_off_by_user_id` bigint unsigned DEFAULT NULL,
  `written_off_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `closed_on_date` date DEFAULT NULL,
  `closed_by_user_id` bigint unsigned DEFAULT NULL,
  `closed_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rescheduled_on_date` date DEFAULT NULL,
  `rescheduled_by_user_id` bigint unsigned DEFAULT NULL,
  `rescheduled_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `withdrawn_on_date` date DEFAULT NULL,
  `withdrawn_by_user_id` bigint unsigned DEFAULT NULL,
  `withdrawn_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `external_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `principal` decimal(65,6) NOT NULL,
  `applied_amount` decimal(65,6) DEFAULT NULL,
  `approved_amount` decimal(65,6) DEFAULT NULL,
  `interest_rate` decimal(65,6) NOT NULL,
  `decimals` int DEFAULT NULL,
  `instalment_multiple_of` int DEFAULT '1',
  `loan_term` int NOT NULL,
  `repayment_frequency` int NOT NULL,
  `repayment_frequency_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_rate_type` enum('day','week','month','year') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable_balloon_payments` tinyint NOT NULL DEFAULT '0',
  `allow_schedule_adjustments` tinyint NOT NULL DEFAULT '0',
  `grace_on_principal_paid` int NOT NULL DEFAULT '0',
  `grace_on_interest_paid` int NOT NULL DEFAULT '0',
  `grace_on_interest_charged` int NOT NULL DEFAULT '0',
  `allow_custom_grace_period` tinyint NOT NULL DEFAULT '0',
  `allow_topup` tinyint NOT NULL DEFAULT '0',
  `interest_methodology` enum('flat','declining_balance') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_recalculation` tinyint NOT NULL DEFAULT '0',
  `amortization_method` enum('equal_installments','equal_principal_payments') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interest_calculation_period_type` enum('daily','same') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days_in_year` enum('actual','360','365','364') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'actual',
  `days_in_month` enum('actual','30','31') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'actual',
  `include_in_loan_cycle` tinyint NOT NULL DEFAULT '0',
  `lock_guarantee_funds` tinyint NOT NULL DEFAULT '0',
  `auto_allocate_overpayments` tinyint NOT NULL DEFAULT '0',
  `allow_additional_charges` tinyint NOT NULL DEFAULT '0',
  `auto_disburse` tinyint NOT NULL DEFAULT '0',
  `status` enum('pending','approved','active','withdrawn','rejected','closed','rescheduled','written_off','overpaid','submitted') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'submitted',
  `disbursement_charges` decimal(65,6) DEFAULT NULL,
  `principal_disbursed_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `principal_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `principal_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `principal_outstanding_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_disbursed_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_waived_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `interest_outstanding_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_disbursed_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_waived_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `fees_outstanding_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_disbursed_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_waived_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `penalties_outstanding_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_disbursed_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_repaid_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_written_off_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_waived_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_outstanding_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `loans_external_id_unique` (`external_id`) USING BTREE,
  UNIQUE KEY `loans_account_number_unique` (`account_number`) USING BTREE,
  KEY `loans_client_id_index` (`client_id`) USING BTREE,
  KEY `loans_loan_officer_id_index` (`loan_officer_id`) USING BTREE,
  KEY `loans_loan_product_id_index` (`loan_product_id`) USING BTREE,
  KEY `loans_branch_id_index` (`branch_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loans`
--

LOCK TABLES `loans` WRITE;
/*!40000 ALTER TABLE `loans` DISABLE KEYS */;
INSERT INTO `loans` VALUES (1,2,'client',1,NULL,2,2,1,1,1,2,2,NULL,NULL,'2021-02-26',2,'2021-05-05',1,NULL,'2020-09-25','2021-03-26','2021-03-26','2021-09-26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,700000.000000,700000.000000,700000.000000,5.000000,NULL,1,6,1,'months','month',0,0,0,0,0,0,0,'declining_balance',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,'approved',21151.000000,700000.000000,0.000000,0.000000,0.000000,127474.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,'2021-02-26 14:55:10','2021-05-05 14:01:23'),(2,1,'client',3,NULL,2,2,2,1,1,1,3,NULL,NULL,'2021-03-08',1,'2021-03-08',1,NULL,'2021-03-08','2021-04-08','2021-04-08','2021-10-08','2021-03-08',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100000.000000,100000.000000,100000.000000,6.000000,NULL,1,6,1,'months','month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,'active',5150.000000,100000.000000,0.000000,0.000000,0.000000,36000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,'2021-03-08 01:08:33','2021-03-08 17:04:37'),(3,4,'client',4,NULL,2,2,2,1,1,6,4,NULL,NULL,'2021-03-08',4,'2021-03-23',1,NULL,'2021-03-08','2021-06-10','2021-06-10','2021-12-10','2021-03-08',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1200000.000000,1200000.000000,1200000.000000,5.000000,NULL,1,6,3,'months','month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,'active',60650.000000,1200000.000000,0.000000,0.000000,0.000000,120000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,'2021-03-08 16:45:24','2021-03-23 12:21:57'),(4,1,'client',4,NULL,2,2,2,1,1,2,2,NULL,NULL,'2021-05-10',1,'2021-05-10',1,'eligible','2021-05-10','2021-06-10','2021-06-10','2021-12-10','2021-05-10',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100000.000000,100000.000000,100000.000000,6.000000,NULL,1,6,1,'months','month',0,0,0,0,0,0,0,'flat',0,'equal_installments',NULL,'actual','actual',0,0,0,0,1,'active',5000.000000,100000.000000,0.000000,0.000000,0.000000,36000.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,'2021-05-10 13:08:59','2021-05-10 13:25:46');
/*!40000 ALTER TABLE `loans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menus` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` bigint DEFAULT NULL,
  `parent_id` bigint DEFAULT NULL,
  `parent_slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_parent` tinyint NOT NULL DEFAULT '0',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `menu_order` int DEFAULT NULL,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `permissions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,NULL,NULL,'',1,'Settings',NULL,'settings',NULL,31,'setting','setting.setting.index','fas fa-cogs','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(2,NULL,1,'settings',0,'Organisation Settings',NULL,'organisation_settings',NULL,32,'setting/organisation','setting.setting.index','far fa-circle','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(3,NULL,1,'settings',0,'General Settings',NULL,'general_settings',NULL,33,'setting/general','setting.setting.index','far fa-circle','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(4,NULL,1,'settings',0,'Email Settings',NULL,'email_settings',NULL,34,'setting/email','setting.setting.index','far fa-circle','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(5,NULL,1,'settings',0,'SMS Settings',NULL,'sms_settings',NULL,35,'setting/sms','setting.setting.index','far fa-circle','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(6,NULL,1,'settings',0,'System Settings',NULL,'system_settings',NULL,36,'setting/system','setting.setting.index','far fa-circle','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(7,NULL,1,'settings',0,'System Update',NULL,'system_update',NULL,37,'setting/system_update','setting.setting.index','far fa-circle','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(8,NULL,1,'settings',0,'Other Settings',NULL,'other_settings',NULL,38,'setting/other','setting.setting.index','far fa-circle','Setting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(9,NULL,NULL,'',1,'Manage Modules',NULL,'modules',NULL,30,'module','core.modules.index','fas fa-plug','Core','2021-02-25 17:09:28','2021-02-25 17:09:28'),(10,NULL,NULL,'',1,'Manage Menu',NULL,'menu',NULL,31,'menu','core.menu.index','fas fa-bars','Core','2021-02-25 17:09:28','2021-02-25 17:09:28'),(11,NULL,1,'settings',0,'Payment Gateways',NULL,'menu',NULL,32,'settings/payment_gateway','core.payment_gateways.index','fas fa-money-bill','Core','2021-02-25 17:09:28','2021-02-25 17:09:28'),(12,NULL,NULL,'',1,'Themes',NULL,'themes',NULL,40,'theme','core.themes.index','fas fa-palette','Core','2021-02-25 17:09:28','2021-02-25 17:09:28'),(13,NULL,NULL,'',1,'Users',NULL,'users',NULL,27,'user','','fas fa-users','User','2021-02-25 17:09:28','2021-02-25 17:09:28'),(14,NULL,13,'users',0,'View Users',NULL,'view_loans',NULL,28,'user','user.users.index','far fa-circle','User','2021-02-25 17:09:28','2021-02-25 17:09:28'),(15,NULL,13,'users',0,'Create User',NULL,'create_loan',NULL,29,'user/create','user.users.create','far fa-circle','User','2021-02-25 17:09:28','2021-02-25 17:09:28'),(16,NULL,13,'users',0,'Manage Roles',NULL,'manage_roles',NULL,30,'user/role','user.roles.index','far fa-circle','User','2021-02-25 17:09:28','2021-02-25 17:09:28'),(17,NULL,NULL,'',1,'Dashboard',NULL,'dashboard',NULL,0,'dashboard','dashboard.index','fas fa-home','Dashboard','2021-02-25 17:09:28','2021-02-25 17:09:28'),(18,NULL,NULL,'',1,'Branches',NULL,'branches',NULL,6,'branch','','fas fa-building','Branch','2021-02-25 17:09:28','2021-02-25 17:09:28'),(19,NULL,18,'branches',0,'View Branches',NULL,'view_branches',NULL,7,'branch','branch.branches.index','far fa-circle','Branch','2021-02-25 17:09:28','2021-02-25 17:09:28'),(20,NULL,18,'branches',0,'Create Branch',NULL,'create_branch',NULL,8,'branch/create','branch.branches.create','far fa-circle','Branch','2021-02-25 17:09:28','2021-02-25 17:09:28'),(21,NULL,NULL,'',1,'Accounting',NULL,'accounting',NULL,3,'accounting','','fas fa-money-bill','Accounting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(22,NULL,21,'accounting',0,'View Charts of Accounts',NULL,'view_charts_of_accounts',NULL,4,'accounting/chart_of_account','accounting.chart_of_accounts.index','far fa-circle','Accounting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(23,NULL,21,'accounting',0,'Journal Entries',NULL,'journal_entries',NULL,5,'accounting/journal_entry','accounting.journal_entries.index','far fa-circle','Accounting','2021-02-25 17:09:28','2021-02-25 17:09:28'),(24,NULL,NULL,'',1,'Assets',NULL,'assets',NULL,30,'asset','asset.assets.index','fas fa-building','Asset','2021-02-25 17:09:28','2021-02-25 17:09:28'),(25,NULL,24,'assets',0,'View Assets',NULL,'view_assets',NULL,7,'asset','asset.assets.index','far fa-circle','Asset','2021-02-25 17:09:28','2021-02-25 17:09:28'),(26,NULL,24,'assets',0,'Create Asset',NULL,'create_asset',NULL,8,'asset/create','asset.assets.create','far fa-circle','Asset','2021-02-25 17:09:28','2021-02-25 17:09:28'),(27,NULL,24,'assets',0,'Manage Asset Types',NULL,'manage_asset_types',NULL,9,'asset/type','asset.assets.types.index','far fa-circle','Asset','2021-02-25 17:09:28','2021-02-25 17:09:28'),(28,NULL,NULL,'',1,'Clients',NULL,'clients',NULL,10,'client','','fas fa-users','Client','2021-02-25 17:09:29','2021-02-25 17:09:29'),(29,NULL,28,'clients',0,'View Clients',NULL,'view_clients',NULL,11,'client','client.clients.index','far fa-circle','Client','2021-02-25 17:09:29','2021-02-25 17:09:29'),(30,NULL,28,'clients',0,'Create Client',NULL,'create_client',NULL,12,'client/create','client.clients.create','far fa-circle','Client','2021-02-25 17:09:29','2021-02-25 17:09:29'),(31,NULL,NULL,'',1,'Savings',NULL,'savings',NULL,25,'savings','','fas fa-university','Savings','2021-02-25 17:09:29','2021-02-25 17:09:29'),(32,NULL,31,'savings',0,'View Savings',NULL,'view_savings',NULL,26,'savings','savings.savings.index','far fa-circle','Savings','2021-02-25 17:09:29','2021-02-25 17:09:29'),(33,NULL,31,'savings',0,'Create Savings',NULL,'create_savings',NULL,27,'savings/create','savings.savings.create','far fa-circle','Savings','2021-02-25 17:09:29','2021-02-25 17:09:29'),(34,NULL,31,'savings',0,'Manage Products',NULL,'manage_products',NULL,28,'savings/product','savings.savings.products.index','far fa-circle','Savings','2021-02-25 17:09:29','2021-02-25 17:09:29'),(35,NULL,31,'savings',0,'Manage Charges',NULL,'manage_charges',NULL,29,'savings/charge','savings.savings.charges.index','far fa-circle','Savings','2021-02-25 17:09:29','2021-02-25 17:09:29'),(36,NULL,NULL,'report',1,'Reports',NULL,'reports',NULL,20,'report','reports.index','fas fa-chart-bar','Report','2021-02-25 17:09:29','2021-02-25 17:09:29'),(37,NULL,NULL,'',1,'Loans',NULL,'loans',NULL,18,'loan','','fas fa-money-bill','Loan','2021-02-25 17:09:30','2021-02-25 17:09:30'),(38,NULL,37,'loans',0,'View Loans',NULL,'view_loans',NULL,19,'loan','loan.loans.index','far fa-circle','Loan','2021-02-25 17:09:30','2021-02-25 17:09:30'),(39,NULL,37,'loans',0,'View Applications',NULL,'view_applications',NULL,20,'loan/application','loan.loans.index','far fa-circle','Loan','2021-02-25 17:09:30','2021-02-25 17:09:30'),(40,NULL,37,'loans',0,'Create Loan',NULL,'create_loan',NULL,21,'loan/create','loan.loans.create','far fa-circle','Loan','2021-02-25 17:09:30','2021-02-25 17:09:30'),(41,NULL,37,'loans',0,'Manage Products',NULL,'manage_products',NULL,22,'loan/product','loan.loans.products.index','far fa-circle','Loan','2021-02-25 17:09:30','2021-02-25 17:09:30'),(42,NULL,37,'loans',0,'Manage Charges',NULL,'manage_charges',NULL,23,'loan/charge','loan.loans.charges.index','far fa-circle','Loan','2021-02-25 17:09:30','2021-02-25 17:09:30'),(43,NULL,37,'loans',0,'Loan Calculator',NULL,'loan_calculator',NULL,24,'loan/calculator','loan.loans.index','far fa-circle','Loan','2021-02-25 17:09:30','2021-02-25 17:09:30'),(44,NULL,NULL,'',1,'Communication',NULL,'communication',NULL,13,'communication','','fas fa-envelope','Communication','2021-02-25 17:09:30','2021-02-25 17:09:30'),(45,NULL,44,'communication',0,'View Campaigns',NULL,'view_campaigns',NULL,14,'communication/campaign','communication.campaigns.index','far fa-circle','Communication','2021-02-25 17:09:30','2021-02-25 17:09:30'),(46,NULL,44,'communication',0,'Create Campaign',NULL,'create_campaign',NULL,15,'communication/campaign/create','communication.campaigns.create','far fa-circle','Communication','2021-02-25 17:09:30','2021-02-25 17:09:30'),(47,NULL,44,'communication',0,'View Logs',NULL,'view_logs',NULL,16,'communication/log','communication.logs.index','far fa-circle','Communication','2021-02-25 17:09:30','2021-02-25 17:09:30'),(48,NULL,44,'communication',0,'Manage SMS Gateways',NULL,'manage_sms_gateways',NULL,17,'communication/sms_gateway','communication.sms_gateways.index','far fa-circle','Communication','2021-02-25 17:09:30','2021-02-25 17:09:30'),(49,NULL,NULL,'',1,'Payroll',NULL,'payroll',NULL,10,'payroll','payroll.payroll.index','fab fa-paypal','Payroll','2021-02-25 17:09:31','2021-02-25 17:09:31'),(50,NULL,49,'payroll',0,'View Payroll',NULL,'view_payroll',NULL,11,'payroll','payroll.payroll.index','far fa-circle','Payroll','2021-02-25 17:09:31','2021-02-25 17:09:31'),(51,NULL,49,'payroll',0,'Create Payroll',NULL,'create_payroll',NULL,12,'payroll/create','payroll.payroll.create','far fa-circle','Payroll','2021-02-25 17:09:31','2021-02-25 17:09:31'),(52,NULL,49,'payroll',0,'Manage Payroll Items',NULL,'manage_payroll_items',NULL,12,'payroll/item','payroll.payroll.items.index','far fa-circle','Payroll','2021-02-25 17:09:31','2021-02-25 17:09:31'),(53,NULL,49,'payroll',0,'Manage Payroll Templates',NULL,'manage_payroll_templates',NULL,12,'payroll/template','payroll.payroll.templates.index','far fa-circle','Payroll','2021-02-25 17:09:31','2021-02-25 17:09:31'),(54,NULL,NULL,'',1,'Expenses',NULL,'expenses',NULL,20,'expense','expense.expenses.index','fas fa-share','Expense','2021-02-25 17:09:31','2021-02-25 17:09:31'),(55,NULL,54,'expenses',0,'View Expenses',NULL,'view_expenses',NULL,0,'expense','expense.expenses.index','far fa-circle','Expense','2021-02-25 17:09:31','2021-02-25 17:09:31'),(56,NULL,54,'expenses',0,'Create Expense',NULL,'create_expense',NULL,1,'expense/create','expense.expenses.create','far fa-circle','Expense','2021-02-25 17:09:31','2021-02-25 17:09:31'),(57,NULL,54,'expenses',0,'Manage Expense Types',NULL,'manage_expense_types',NULL,2,'expense/type','expense.expenses.types.index','far fa-circle','Expense','2021-02-25 17:09:31','2021-02-25 17:09:31'),(58,NULL,NULL,'',1,'Custom Fields',NULL,'custom_fields',NULL,25,'custom_field','','fas fa-list','CustomField','2021-02-25 17:09:31','2021-02-25 17:09:31'),(59,NULL,58,'custom_fields',0,'View Custom Fields',NULL,'view_custom_fields',NULL,26,'custom_field','customfield.custom_fields.index','far fa-circle','Savings','2021-02-25 17:09:31','2021-02-25 17:09:31'),(60,NULL,58,'custom_fields',0,'Create Custom Field',NULL,'create_custom_fields',NULL,27,'custom_field/create','customfield.custom_fields.create','far fa-circle','Savings','2021-02-25 17:09:31','2021-02-25 17:09:31'),(61,NULL,NULL,'',1,'Income',NULL,'income',NULL,25,'income','income.income.index','fas fa-money-bill','Income','2021-02-25 17:09:32','2021-02-25 17:09:32'),(62,NULL,61,'income',0,'View Income',NULL,'view_assets',NULL,7,'income','income.income.index','far fa-circle','Income','2021-02-25 17:09:32','2021-02-25 17:09:32'),(63,NULL,61,'income',0,'Create Income',NULL,'create_asset',NULL,8,'income/create','income.income.create','far fa-circle','Income','2021-02-25 17:09:32','2021-02-25 17:09:32'),(64,NULL,61,'income',0,'Manage Income Types',NULL,'manage_asset_types',NULL,9,'income/type','income.income.types.index','far fa-circle','Income','2021-02-25 17:09:32','2021-02-25 17:09:32'),(65,NULL,1,'settings',0,'System Upgrade',NULL,'system_upgrade',NULL,40,'upgrade','upgrade.upgrades.index','fa fa-circle-o','Setting','2021-02-25 17:09:32','2021-02-25 17:09:32'),(66,NULL,NULL,'',1,'Shares',NULL,'shares',NULL,30,'share','share.shares.index','fas fa-database','Share','2021-02-25 17:09:33','2021-02-25 17:09:33'),(67,NULL,66,'shares',0,'View Shares',NULL,'view_shares',NULL,1,'share','share.shares.index','far fa-circle','Share','2021-02-25 17:09:33','2021-02-25 17:09:33'),(68,NULL,66,'shares',0,'Create Share',NULL,'create_share',NULL,2,'share/create','share.shares.create','far fa-circle','Share','2021-02-25 17:09:33','2021-02-25 17:09:33'),(69,NULL,66,'shares',0,'Manage Products',NULL,'manage_share_products',NULL,3,'share/product','share.shares.products.index','far fa-circle','Share','2021-02-25 17:09:33','2021-02-25 17:09:33'),(70,NULL,66,'shares',0,'Manage Charges',NULL,'manage_share_charges',NULL,3,'share/charge','share.shares.charges.index','far fa-circle','Share','2021-02-25 17:09:33','2021-02-25 17:09:33'),(71,NULL,NULL,'',1,'Activity Logs',NULL,'activity_log',NULL,60,'activity_log','activitylog.activity_logs.index','fa fa-database','Activitylog','2021-02-25 17:09:33','2021-02-25 17:09:33');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2019_07_06_133107_create_settings_table',1),(2,'2018_08_08_100000_create_telescope_entries_table',2),(3,'2019_07_06_135714_create_countries_table',2),(4,'2019_07_06_140045_create_currencies_table',2),(5,'2019_07_06_140908_create_timezones_table',2),(6,'2019_07_07_110645_create_payment_types_table',2),(7,'2019_07_07_110717_create_payment_details_table',2),(8,'2019_07_10_225923_create_notifications_table',2),(9,'2019_07_27_080313_create_jobs_table',2),(10,'2019_09_07_001758_create_menus_table',2),(11,'2019_11_04_152950_create_tax_rates_table',2),(12,'2020_01_14_114056_create_failed_jobs_table',2),(13,'2014_10_12_000000_create_users_table',3),(14,'2014_10_12_100000_create_password_resets_table',3),(15,'2019_07_01_214429_create_permission_tables',3),(16,'2019_08_03_085238_create_widgets_table',4),(17,'2019_07_06_111125_create_branches_table',5),(18,'2019_07_06_111419_create_branch_users_table',5),(19,'2019_07_07_093258_create_chart_of_accounts_table',6),(20,'2019_07_07_093648_create_journal_entries_table',6),(21,'2019_09_26_153830_create_asset_types_table',7),(22,'2019_09_26_153906_create_assets_table',7),(23,'2019_09_26_153953_create_asset_notes_table',7),(24,'2019_09_26_153954_create_asset_maintenance_types_table',7),(25,'2019_09_26_154012_create_asset_maintenance_table',7),(26,'2019_09_26_154050_create_asset_files_table',7),(27,'2019_09_26_154103_create_asset_pictures_table',7),(28,'2019_09_27_063335_create_asset_depreciation_table',7),(29,'2019_07_08_130052_create_titles_table',8),(30,'2019_07_08_130053_create_client_relationships_table',8),(31,'2019_07_08_130533_create_professions_table',8),(32,'2019_07_08_150347_create_client_types_table',8),(33,'2019_07_08_151636_create_client_identification_types_table',8),(34,'2019_07_08_182818_create_clients_table',8),(35,'2019_07_08_182911_create_client_files_table',8),(36,'2019_07_08_182938_create_client_identification_table',8),(37,'2019_07_08_183031_create_client_next_of_kin_table',8),(38,'2019_07_11_180428_create_client_users_table',8),(39,'2019_08_05_093954_create_savings_charge_options_table',9),(40,'2019_08_05_094221_create_savings_charge_types_table',9),(41,'2019_08_05_094458_create_savings_charges_table',9),(42,'2019_08_05_094544_create_savings_transaction_types_table',9),(43,'2019_08_05_094956_create_savings_products_table',9),(44,'2019_08_05_095030_create_savings_table',9),(45,'2019_08_05_095031_create_savings_linked_charges_table',9),(46,'2019_08_05_095048_create_savings_transactions_table',9),(47,'2019_08_05_095148_create_savings_product_linked_charges_table',9),(48,'2019_07_15_094401_create_loan_transaction_processing_strategies_table',10),(49,'2019_07_15_100526_create_loan_charge_types_table',10),(50,'2019_07_15_100649_create_loan_charge_options_table',10),(51,'2019_07_15_104331_create_loan_credit_checks_table',10),(52,'2019_07_15_141230_create_loan_purposes_table',10),(53,'2019_07_15_201056_create_loan_transaction_types_table',10),(54,'2019_07_15_214326_create_funds_table',10),(55,'2019_07_15_214410_create_loan_charges_table',10),(56,'2019_07_15_214940_create_loan_products_table',10),(57,'2019_07_15_215017_create_loan_product_linked_charges_table',10),(58,'2019_07_15_215107_create_loan_disbursement_channels_table',10),(59,'2019_07_15_215314_create_loan_collateral_types_table',10),(60,'2019_07_15_215355_create_loans_table',10),(61,'2019_07_15_215451_create_loan_collateral_table',10),(62,'2019_07_15_215546_create_loan_repayment_schedules_table',10),(63,'2019_07_15_215604_create_loan_transactions_table',10),(64,'2019_07_15_221258_create_loan_linked_charges_table',10),(65,'2019_07_17_130522_create_loan_product_linked_credit_checks_table',10),(66,'2019_07_17_130536_create_loan_linked_credit_checks_table',10),(67,'2019_07_17_162121_create_loan_guarantors_table',10),(68,'2019_07_17_194223_create_loan_officer_history_table',10),(69,'2019_07_17_194247_create_loan_history_table',10),(70,'2019_07_17_194817_create_loan_files_table',10),(71,'2019_07_17_194827_create_loan_notes_table',10),(72,'2019_08_30_074012_create_loan_applications_table',10),(73,'2019_07_27_161835_create_communication_campaign_business_rules_table',11),(74,'2019_07_27_161902_create_communication_campaign_attachment_types_table',11),(75,'2019_07_28_150020_create_sms_gateways_table',11),(76,'2019_07_28_150053_create_communication_campaigns_table',11),(77,'2019_07_28_161940_create_communication_campaign_logs_table',11),(78,'2016_06_01_000001_create_oauth_auth_codes_table',12),(79,'2016_06_01_000002_create_oauth_access_tokens_table',12),(80,'2016_06_01_000003_create_oauth_refresh_tokens_table',12),(81,'2016_06_01_000004_create_oauth_clients_table',12),(82,'2016_06_01_000005_create_oauth_personal_access_clients_table',12),(83,'2019_09_22_080345_create_payroll_items_table',13),(84,'2019_09_22_081303_create_payroll_templates_table',13),(85,'2019_09_22_081304_create_payroll_template_items_table',13),(86,'2019_09_22_081326_create_payroll_table',13),(87,'2019_09_22_081441_create_payroll_items_meta_table',13),(88,'2019_09_22_082657_create_payroll_payments_table',13),(89,'2020_02_24_114006_create_expense_types_table',14),(90,'2020_02_24_114018_create_expenses_table',14),(91,'2019_09_15_164302_create_custom_fields_table',15),(92,'2019_09_15_164434_create_custom_fields_meta_table',15),(93,'2020_02_24_114052_create_income_types_table',16),(94,'2020_02_24_114104_create_income_table',16),(95,'2020_09_10_171351_create_share_charge_options_table',17),(96,'2020_09_10_171936_create_share_transaction_types_table',17),(97,'2020_09_10_171940_create_share_charge_types_table',17),(98,'2020_09_10_171940_create_share_charges_table',17),(99,'2020_09_10_171959_create_share_products_table',17),(100,'2020_09_10_172033_create_share_product_linked_charges_table',17),(101,'2020_09_10_172054_create_shares_table',17),(102,'2020_09_10_172110_create_share_linked_charges_table',17),(103,'2020_09_10_172120_create_share_transactions_table',17),(104,'2020_09_10_172155_create_share_market_periods_table',17),(105,'2019_07_15_125704_create_activity_log_table',18),(106,'2020_08_31_141646_create_wallets_table',19),(107,'2020_08_31_150716_create_wallet_transactions_table',19);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int unsigned NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`) USING BTREE,
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_roles` (
  `role_id` int unsigned NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`) USING BTREE,
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'Modules\\User\\Entities\\User',1),(1,'Modules\\User\\Entities\\User',2),(3,'Modules\\User\\Entities\\User',4),(3,'Modules\\User\\Entities\\User',5),(3,'Modules\\User\\Entities\\User',6),(3,'Modules\\User\\Entities\\User',7);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  `client_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oauth_access_tokens_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `client_id` int unsigned NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_clients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oauth_clients_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_details`
--

DROP TABLE IF EXISTS `payment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_details` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` int DEFAULT NULL,
  `payment_type_id` int DEFAULT NULL,
  `transaction_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` int DEFAULT NULL,
  `cheque_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `routing_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_details`
--

LOCK TABLES `payment_details` WRITE;
/*!40000 ALTER TABLE `payment_details` DISABLE KEYS */;
INSERT INTO `payment_details` VALUES (1,2,2,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-26 14:59:42','2021-02-26 14:59:42'),(2,1,3,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 01:08:59','2021-03-08 01:08:59'),(3,1,3,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 16:56:54','2021-03-08 16:56:54'),(4,1,3,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 17:04:37','2021-03-08 17:04:37'),(5,2,1,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 17:11:22','2021-03-08 17:11:22'),(6,2,1,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 17:16:14','2021-03-08 17:16:14'),(7,1,3,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-23 12:21:57','2021-03-23 12:21:57'),(8,1,3,'loan_transaction',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-10 13:25:46','2021-05-10 13:25:46');
/*!40000 ALTER TABLE `payment_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_types`
--

DROP TABLE IF EXISTS `payment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_cash` tinyint NOT NULL DEFAULT '0',
  `is_online` tinyint NOT NULL DEFAULT '0',
  `is_system` tinyint NOT NULL DEFAULT '0',
  `active` tinyint NOT NULL DEFAULT '1',
  `position` int DEFAULT NULL,
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `unique_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_types`
--

LOCK TABLES `payment_types` WRITE;
/*!40000 ALTER TABLE `payment_types` DISABLE KEYS */;
INSERT INTO `payment_types` VALUES (1,'EQUITY BANK',NULL,NULL,0,0,0,1,0,NULL,NULL,'2021-02-26 14:58:13','2021-02-26 14:58:13'),(2,'ABSA BANK',NULL,NULL,0,0,0,1,0,NULL,NULL,'2021-02-26 14:58:56','2021-02-26 14:58:56'),(3,'MPESA',NULL,NULL,0,0,0,1,0,NULL,NULL,'2021-02-26 14:59:13','2021-02-26 14:59:13'),(4,'KCB',NULL,NULL,0,0,0,1,0,NULL,NULL,'2021-02-26 14:59:27','2021-02-26 14:59:27');
/*!40000 ALTER TABLE `payment_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payroll`
--

DROP TABLE IF EXISTS `payroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payroll` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `payroll_template_id` bigint unsigned DEFAULT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `employee_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `work_duration` decimal(65,2) NOT NULL DEFAULT '0.00',
  `duration_unit` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_per_duration` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_duration_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `gross_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_allowances` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_deductions` decimal(65,2) NOT NULL DEFAULT '0.00',
  `date` date DEFAULT NULL,
  `year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recurring` tinyint DEFAULT '0',
  `recur_frequency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '31',
  `recur_start_date` date DEFAULT NULL,
  `recur_end_date` date DEFAULT NULL,
  `recur_next_date` date DEFAULT NULL,
  `recur_type` enum('day','week','month','year') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'month',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payroll`
--

LOCK TABLES `payroll` WRITE;
/*!40000 ALTER TABLE `payroll` DISABLE KEYS */;
/*!40000 ALTER TABLE `payroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payroll_items`
--

DROP TABLE IF EXISTS `payroll_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payroll_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('allowance','deduction') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_type` enum('fixed','percentage') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(65,2) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payroll_items`
--

LOCK TABLES `payroll_items` WRITE;
/*!40000 ALTER TABLE `payroll_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `payroll_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payroll_items_meta`
--

DROP TABLE IF EXISTS `payroll_items_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payroll_items_meta` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `payroll_id` bigint unsigned DEFAULT NULL,
  `payroll_item_id` bigint unsigned DEFAULT NULL,
  `percentage` decimal(65,2) DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('allowance','deduction') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_type` enum('fixed','percentage') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(65,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payroll_items_meta`
--

LOCK TABLES `payroll_items_meta` WRITE;
/*!40000 ALTER TABLE `payroll_items_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `payroll_items_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payroll_payments`
--

DROP TABLE IF EXISTS `payroll_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payroll_payments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `payroll_id` bigint unsigned DEFAULT NULL,
  `payment_type_id` bigint unsigned DEFAULT NULL,
  `payment_detail_id` bigint unsigned DEFAULT NULL,
  `amount` decimal(65,2) NOT NULL,
  `submitted_on` date DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payroll_payments`
--

LOCK TABLES `payroll_payments` WRITE;
/*!40000 ALTER TABLE `payroll_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payroll_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payroll_template_items`
--

DROP TABLE IF EXISTS `payroll_template_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payroll_template_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `payroll_template_id` bigint unsigned DEFAULT NULL,
  `payroll_item_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payroll_template_items`
--

LOCK TABLES `payroll_template_items` WRITE;
/*!40000 ALTER TABLE `payroll_template_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `payroll_template_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payroll_templates`
--

DROP TABLE IF EXISTS `payroll_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payroll_templates` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_duration` decimal(65,2) NOT NULL DEFAULT '0.00',
  `duration_unit` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_per_duration` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_duration_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `picture` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `items` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payroll_templates`
--

LOCK TABLES `payroll_templates` WRITE;
/*!40000 ALTER TABLE `payroll_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `payroll_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,NULL,'setting.setting.index','View settings','Setting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(2,NULL,'setting.setting.edit','Edit Settings','Setting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(3,NULL,'core.payment_types.index','View Payment Types','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(4,NULL,'core.payment_types.create','Create Payment Types','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(5,NULL,'core.payment_types.edit','Edit Payment Types','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(6,NULL,'core.payment_types.destroy','Delete Payment Types','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(7,NULL,'core.currencies.index','View Payment Details','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(8,NULL,'core.currencies.create','Create Currencies','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(9,NULL,'core.currencies.edit','Edit Currencies','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(10,NULL,'core.currencies.destroy','Delete Currencies','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(11,NULL,'core.modules.index','View Modules','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(12,NULL,'core.modules.create','Create Modules','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(13,NULL,'core.modules.destroy','Delete Modules','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(14,NULL,'core.menu.index','Manage Menu','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(15,NULL,'core.themes.index','Themes','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(16,NULL,'core.payment_gateways.index','View Payment Gateway','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(17,NULL,'core.payment_gateways.create','Create Payment Gateway','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(18,NULL,'core.payment_gateways.edit','Edit Payment Gateway','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(19,NULL,'core.payment_gateways.destroy','Delete Payment Gateway','Core',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(20,NULL,'user.users.index','View Users','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(21,NULL,'user.users.create','Create Users','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(22,NULL,'user.users.edit','Edit Users','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(23,NULL,'user.users.destroy','Delete Users','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(24,NULL,'user.roles.index','View Roles','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(25,NULL,'user.roles.create','Create Roles','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(26,NULL,'user.roles.edit','Edit Roles','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(27,NULL,'user.roles.destroy','Delete Roles','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(28,NULL,'user.reports.index','View Reports','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(29,NULL,'user.reports.performance','View Performance Reports','User',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(30,NULL,'dashboard.index','View Dashboard','Dashboard',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(31,NULL,'dashboard.edit','Edit Dashboard','Dashboard',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(32,NULL,'branch.branches.index','View Branches','Branch',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(33,NULL,'branch.branches.create','Create Branches','Branch',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(34,NULL,'branch.branches.edit','Edit Branches','Branch',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(35,NULL,'branch.branches.destroy','Delete Branches','Branch',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(36,NULL,'branch.branches.assign_user','Assign Users','Branch',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(37,NULL,'accounting.chart_of_accounts.index','View Chart of accounts','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(38,NULL,'accounting.chart_of_accounts.create','Create Chart of accounts','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(39,NULL,'accounting.chart_of_accounts.edit','Edit Chart of accounts','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(40,NULL,'accounting.chart_of_accounts.destroy','Delete Chart of accounts','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(41,NULL,'accounting.journal_entries.index','View Journal Entries','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(42,NULL,'accounting.journal_entries.create','Create Journal Entries','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(43,NULL,'accounting.journal_entries.edit','Edit Journal Entries','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(44,NULL,'accounting.journal_entries.reverse','Reverse Journal Entries','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(45,NULL,'accounting.reports.balance_sheet','View Balance Sheet','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(46,NULL,'accounting.reports.trial_balance','View Trial Balance','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(47,NULL,'accounting.reports.income_statement','View Income Statement','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(48,NULL,'accounting.reports.ledger','View Ledger','Accounting',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(49,NULL,'asset.assets.index','View Assets','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(50,NULL,'asset.assets.create','Create Assets','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(51,NULL,'asset.assets.edit','Edit Assets','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(52,NULL,'asset.assets.destroy','Delete Assets','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(53,NULL,'asset.assets.types.index','View Asset Types','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(54,NULL,'asset.assets.types.create','Create Asset Types','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(55,NULL,'asset.assets.types.edit','Edit Asset Types','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(56,NULL,'asset.assets.types.destroy','Delete Asset Types','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(57,NULL,'asset.assets.notes.index','View Asset Notes','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(58,NULL,'asset.assets.notes.create','Create Asset Notes','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(59,NULL,'asset.assets.notes.edit','Edit Asset Notes','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(60,NULL,'asset.assets.notes.destroy','Delete Asset Notes','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(61,NULL,'asset.assets.maintenance.index','View Asset Maintenance','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(62,NULL,'asset.assets.maintenance.create','Create Asset Maintenance','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(63,NULL,'asset.assets.maintenance.edit','Edit Asset Maintenance','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(64,NULL,'asset.assets.maintenance.destroy','Delete Asset Maintenance','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(65,NULL,'asset.assets.files.index','View Asset Files','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(66,NULL,'asset.assets.files.create','Create Asset Files','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(67,NULL,'asset.assets.files.edit','Edit Asset Files','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(68,NULL,'asset.assets.files.destroy','Delete Asset Files','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(69,NULL,'asset.assets.pictures.index','View Asset Pictures','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(70,NULL,'asset.assets.pictures.create','Create Asset Pictures','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(71,NULL,'asset.assets.pictures.edit','Edit Asset Pictures','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(72,NULL,'asset.assets.pictures.destroy','Delete Asset Pictures','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(73,NULL,'asset.assets.valuations.index','View Asset Valuations','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(74,NULL,'asset.assets.valuations.create','Create Asset Valuations','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(75,NULL,'asset.assets.valuations.edit','Edit Asset Valuations','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(76,NULL,'asset.assets.valuations.destroy','Delete Asset Valuations','Asset',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(77,NULL,'client.clients.index','View Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(78,NULL,'client.clients.index_own','View Own Clients Only','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(79,NULL,'client.clients.create','Create Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(80,NULL,'client.clients.edit','Edit Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(81,NULL,'client.clients.edit_own','Edit Own Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(82,NULL,'client.clients.activate','Activate Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(83,NULL,'client.clients.activate_own','Activate Own Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(84,NULL,'client.clients.destroy','Delete Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(85,NULL,'client.clients.destroy_own','Delete Own Clients','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(86,NULL,'client.clients.user.create','Create Client Users','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(87,NULL,'client.clients.user.destroy','Delete Client Users','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(88,NULL,'client.clients.titles.index','View Titles','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(89,NULL,'client.clients.titles.create','Create Titles','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(90,NULL,'client.clients.titles.edit','Edit Titles','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(91,NULL,'client.clients.titles.destroy','Delete Titles','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(92,NULL,'client.clients.professions.index','View Professions','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(93,NULL,'client.clients.professions.create','Create Professions','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(94,NULL,'client.clients.professions.edit','Edit Professions','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(95,NULL,'client.clients.professions.destroy','Delete Professions','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(96,NULL,'client.clients.client_relationships.index','View Client Relationships','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(97,NULL,'client.clients.client_relationships.create','Create Client Relationships','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(98,NULL,'client.clients.client_relationships.edit','Edit Client Relationships','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(99,NULL,'client.clients.client_relationships.destroy','Delete Client Relationships','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(100,NULL,'client.clients.client_types.index','View Client Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(101,NULL,'client.clients.client_types.create','Create Client Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(102,NULL,'client.clients.client_types.edit','Edit Client Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(103,NULL,'client.clients.client_types.destroy','Delete Client Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(104,NULL,'client.clients.identification_types.index','View Identification Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(105,NULL,'client.clients.identification_types.create','Create Identification Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(106,NULL,'client.clients.identification_types.edit','Edit Identification Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(107,NULL,'client.clients.identification_types.destroy','Delete Identification Types','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(108,NULL,'client.clients.files.index','View Files','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(109,NULL,'client.clients.files.create','Create Files','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(110,NULL,'client.clients.files.edit','Edit Files','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(111,NULL,'client.clients.files.destroy','Delete Files','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(112,NULL,'client.clients.next_of_kin.index','View Next of kin','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(113,NULL,'client.clients.next_of_kin.create','Create Next of kin','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(114,NULL,'client.clients.next_of_kin.edit','Edit Next of kin','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(115,NULL,'client.clients.next_of_kin.destroy','Delete Next of kins','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(116,NULL,'client.clients.identification.index','View Identification','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(117,NULL,'client.clients.identification.create','Create Identification','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(118,NULL,'client.clients.identification.edit','Edit Identification','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(119,NULL,'client.clients.identification.destroy','Delete Identification','Client',NULL,'web','2021-02-25 17:09:28','2021-02-25 17:09:28'),(120,NULL,'savings.savings.index','View Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(121,NULL,'savings.savings.create','Create Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(122,NULL,'savings.savings.edit','Edit Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(123,NULL,'savings.savings.destroy','Delete Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(124,NULL,'savings.savings.approve_savings','Approve/Reject Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(125,NULL,'savings.savings.activate_savings','Activate Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(126,NULL,'savings.savings.withdraw_savings','Withdraw Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(127,NULL,'savings.savings.inactive_savings','Inactive Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(128,NULL,'savings.savings.dormant_savings','Dormant Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(129,NULL,'savings.savings.close_savings','Close Savings','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(130,NULL,'savings.savings.products.index','View savings Products','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(131,NULL,'savings.savings.products.create','Create savings Products','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(132,NULL,'savings.savings.products.edit','Edit savings Products','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(133,NULL,'savings.savings.products.destroy','Delete savings Products','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(134,NULL,'savings.savings.transactions.index','View Transactions','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(135,NULL,'savings.savings.transactions.create','Create Transactions','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(136,NULL,'savings.savings.transactions.edit','Edit Transactions','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(137,NULL,'savings.savings.transactions.destroy','Delete/Reverse Transactions','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(138,NULL,'savings.savings.charges.index','View Charges','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(139,NULL,'savings.savings.charges.create','Create Charges','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(140,NULL,'savings.savings.charges.edit','Edit Charges','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(141,NULL,'savings.savings.charges.destroy','Delete Charges','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(142,NULL,'savings.savings.reports.transactions','View Transactions Report','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(143,NULL,'savings.savings.reports.balances','View Balances Report','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(144,NULL,'savings.savings.reports.accounts','View Accounts Report','Savings',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(145,NULL,'reports.index','View Reports','Report',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(146,NULL,'loan.loans.index','View Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(147,NULL,'loan.loans.create','Create Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(148,NULL,'loan.loans.edit','Edit Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(149,NULL,'loan.loans.destroy','Delete Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(150,NULL,'loan.loans.approve_loan','Approve/Reject Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(151,NULL,'loan.loans.disburse_loan','Disburse Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(152,NULL,'loan.loans.withdraw_loan','Withdraw Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(153,NULL,'loan.loans.write_off_loan','Write Off Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(154,NULL,'loan.loans.reschedule_loan','Reschedule Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(155,NULL,'loan.loans.close_loan','Close Loans','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(156,NULL,'loan.loans.calculator','Use Loan Calculator','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(157,NULL,'loan.loans.loan_history','View Loan History','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(158,NULL,'loan.loans.products.index','View Loan Products','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(159,NULL,'loan.loans.products.create','Create Loan Products','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(160,NULL,'loan.loans.products.edit','Edit Loan Products','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(161,NULL,'loan.loans.products.destroy','Delete Loan Products','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(162,NULL,'loan.loans.transactions.index','View Transactions','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(163,NULL,'loan.loans.transactions.create','Create Transactions','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(164,NULL,'loan.loans.transactions.edit','Edit Transactions','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(165,NULL,'loan.loans.transactions.destroy','Delete/Reverse Transactions','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(166,NULL,'loan.loans.charges.index','View Charges','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(167,NULL,'loan.loans.charges.create','Create Charges','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(168,NULL,'loan.loans.charges.edit','Edit Charges','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(169,NULL,'loan.loans.charges.destroy','Delete Charges','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(170,NULL,'loan.loans.funds.index','View Funds','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(171,NULL,'loan.loans.funds.create','Create Funds','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(172,NULL,'loan.loans.funds.edit','Edit Funds','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(173,NULL,'loan.loans.funds.destroy','Delete Funds','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(174,NULL,'loan.loans.credit_checks.index','View Credit Checks','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(175,NULL,'loan.loans.credit_checks.create','Create Credit Checks','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(176,NULL,'loan.loans.credit_checks.edit','Edit Credit Checks','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(177,NULL,'loan.loans.credit_checks.destroy','Delete Credit Checks','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(178,NULL,'loan.loans.collateral.index','View Collateral','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(179,NULL,'loan.loans.collateral.create','Create Collateral','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(180,NULL,'loan.loans.collateral.edit','Edit Collateral','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(181,NULL,'loan.loans.collateral.destroy','Delete Collateral','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(182,NULL,'loan.loans.collateral_types.index','View Collateral Types','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(183,NULL,'loan.loans.collateral_types.create','Create Collateral Types','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(184,NULL,'loan.loans.collateral_types.edit','Edit Collateral Types','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(185,NULL,'loan.loans.collateral_types.destroy','Delete Collateral Types','Loan',NULL,'web','2021-02-25 17:09:29','2021-02-25 17:09:29'),(186,NULL,'loan.loans.purposes.index','View Purposes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(187,NULL,'loan.loans.purposes.create','Create Purposes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(188,NULL,'loan.loans.purposes.edit','Edit Purposes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(189,NULL,'loan.loans.purposes.destroy','Delete Purposes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(190,NULL,'loan.loans.files.index','View Files','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(191,NULL,'loan.loans.files.create','Create Files','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(192,NULL,'loan.loans.files.edit','Edit Files','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(193,NULL,'loan.loans.files.destroy','Delete Files','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(194,NULL,'loan.loans.guarantors.index','View Guarantors','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(195,NULL,'loan.loans.guarantors.create','Create Guarantors','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(196,NULL,'loan.loans.guarantors.edit','Edit Guarantors','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(197,NULL,'loan.loans.guarantors.destroy','Delete Guarantors','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(198,NULL,'loan.loans.notes.index','View Notes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(199,NULL,'loan.loans.notes.create','Create Notes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(200,NULL,'loan.loans.notes.edit','Edit Notes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(201,NULL,'loan.loans.notes.destroy','Delete Notes','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(202,NULL,'loan.loans.reports.collection_sheet','View Collection Sheet Reports','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(203,NULL,'loan.loans.reports.repayments','View Repayments Report','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(204,NULL,'loan.loans.reports.expected_repayments','View Expected Repayments Report','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(205,NULL,'loan.loans.reports.arrears','View Arrears Reports','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(206,NULL,'loan.loans.reports.disbursements','View Disbursements Report','Loan',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(207,NULL,'communication.index','View Communication','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(208,NULL,'communication.campaigns.index','View Campaigns','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(209,NULL,'communication.campaigns.create','Create Campaigns','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(210,NULL,'communication.campaigns.edit','Edit Campaigns','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(211,NULL,'communication.campaigns.destroy','Delete Campaigns','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(212,NULL,'communication.logs.index','View Logs','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(213,NULL,'communication.logs.create','Create Logs','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(214,NULL,'communication.logs.edit','Edit Logs','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(215,NULL,'communication.logs.destroy','Delete Logs','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(216,NULL,'communication.sms_gateways.index','View SMS Gateways','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(217,NULL,'communication.sms_gateways.create','Create SMS Gateways','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(218,NULL,'communication.sms_gateways.edit','Edit SMS Gateways','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(219,NULL,'communication.sms_gateways.destroy','Delete SMS Gateways','Communication',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(220,NULL,'payroll.payroll.index','View Payroll','Payroll',NULL,'web','2021-02-25 17:09:30','2021-02-25 17:09:30'),(221,NULL,'payroll.payroll.create','Create Payroll','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(222,NULL,'payroll.payroll.edit','Edit Payroll','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(223,NULL,'payroll.payroll.destroy','Delete Payroll','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(224,NULL,'payroll.payroll.items.index','View Payroll Items','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(225,NULL,'payroll.payroll.items.create','Create Payroll Items','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(226,NULL,'payroll.payroll.items.edit','Edit Payroll Items','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(227,NULL,'payroll.payroll.items.destroy','Delete Payroll Items','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(228,NULL,'payroll.payroll.templates.index','View Templates','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(229,NULL,'payroll.payroll.templates.create','Create Templates','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(230,NULL,'payroll.payroll.templates.edit','Edit Templates','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(231,NULL,'payroll.payroll.templates.destroy','Delete Templates','Payroll',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(232,NULL,'expense.expenses.index','View Expenses','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(233,NULL,'expense.expenses.create','Create Expenses','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(234,NULL,'expense.expenses.edit','Edit Expenses','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(235,NULL,'expense.expenses.destroy','Delete Expenses','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(236,NULL,'expense.expenses.types.index','View Expense Types','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(237,NULL,'expense.expenses.types.create','Create Expense Types','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(238,NULL,'expense.expenses.types.edit','Edit Expense Types','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(239,NULL,'expense.expenses.types.destroy','Delete Expense Types','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(240,NULL,'expense.expenses.notes.index','View Expense Notes','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(241,NULL,'expense.expenses.notes.create','Create Expense Notes','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(242,NULL,'expense.expenses.notes.edit','Edit Expense Notes','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(243,NULL,'expense.expenses.notes.destroy','Delete Expense Notes','Expense',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(244,NULL,'customfield.custom_fields.index','View Custom Fields','CustomField',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(245,NULL,'customfield.custom_fields.create','Create Custom Field','CustomField',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(246,NULL,'customfield.custom_fields.edit','Edit Custom Field','CustomField',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(247,NULL,'customfield.custom_fields.destroy','Delete Custom Field','CustomField',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(248,NULL,'income.income.index','View Income','Income',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(249,NULL,'income.income.create','Create Income','Income',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(250,NULL,'income.income.edit','Edit Income','Income',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(251,NULL,'income.income.destroy','Delete Income','Income',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(252,NULL,'income.income.types.index','View Income Types','Income',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(253,NULL,'income.income.types.create','Create Income Types','Income',NULL,'web','2021-02-25 17:09:31','2021-02-25 17:09:31'),(254,NULL,'income.income.types.edit','Edit Income Types','Income',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(255,NULL,'income.income.types.destroy','Delete Income Types','Income',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(256,NULL,'income.income.notes.index','View Income Notes','Income',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(257,NULL,'income.income.notes.create','Create Income Notes','Income',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(258,NULL,'income.income.notes.edit','Edit Income Notes','Income',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(259,NULL,'income.income.notes.destroy','Delete Income Notes','Income',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(260,NULL,'upgrade.upgrades.index','View Upgrade Page','Upgrade',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(261,NULL,'upgrade.upgrades.manage','Manage Upgrades','Upgrade',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(262,NULL,'share.shares.index','View Shares','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(263,NULL,'share.shares.create','Create Shares','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(264,NULL,'share.shares.edit','Edit Shares','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(265,NULL,'share.shares.destroy','Delete Shares','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(266,NULL,'share.shares.approve_shares','Approve Shares','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(267,NULL,'share.shares.activate_shares','Activate Shares','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(268,NULL,'share.shares.close_shares','Close Shares','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(269,NULL,'share.shares.products.index','View Share Products','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(270,NULL,'share.shares.products.create','Create Share Products','Share',NULL,'web','2021-02-25 17:09:32','2021-02-25 17:09:32'),(271,NULL,'share.shares.products.edit','Edit Share Products','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(272,NULL,'share.shares.products.destroy','Delete Share Products','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(273,NULL,'share.shares.transactions.index','View Transactions','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(274,NULL,'share.shares.transactions.create','Create Transactions','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(275,NULL,'share.shares.transactions.edit','Edit Transactions','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(276,NULL,'share.shares.transactions.destroy','Delete/Reverse Transactions','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(277,NULL,'share.shares.notes.index','View Share Notes','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(278,NULL,'share.shares.notes.create','Create Share Notes','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(279,NULL,'share.shares.notes.edit','Edit Share Notes','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(280,NULL,'share.shares.notes.destroy','Delete Share Notes','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(281,NULL,'share.shares.charges.index','View Share Charges','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(282,NULL,'share.shares.charges.create','Create Share Charges','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(283,NULL,'share.shares.charges.edit','Edit Share Charges','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(284,NULL,'share.shares.charges.destroy','Delete Share Charges','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(285,NULL,'share.shares.files.index','View Share Files','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(286,NULL,'share.shares.files.create','Create Share Files','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(287,NULL,'share.shares.files.edit','Edit Share Files','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(288,NULL,'share.shares.files.destroy','Delete Share Files','Share',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(289,NULL,'activitylog.activity_logs.index','View Activity Logs','Activitylog',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33'),(290,NULL,'activitylog.activity_logs.destroy','Delete Activity Logs','Activitylog',NULL,'web','2021-02-25 17:09:33','2021-02-25 17:09:33');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professions`
--

DROP TABLE IF EXISTS `professions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professions`
--

LOCK TABLES `professions` WRITE;
/*!40000 ALTER TABLE `professions` DISABLE KEYS */;
INSERT INTO `professions` VALUES (1,'BANKER'),(2,'FARMER'),(3,'TEACHER'),(4,'DOCTOR'),(5,'BUSINESS'),(6,'PROFFESSOR'),(7,'MANUFACTURER'),(8,'PHAMARSIST'),(9,'OTHERS');
/*!40000 ALTER TABLE `professions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int unsigned NOT NULL,
  `role_id` int unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`) USING BTREE,
  KEY `role_has_permissions_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1),(78,1),(79,1),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(90,1),(91,1),(92,1),(93,1),(94,1),(95,1),(96,1),(97,1),(98,1),(99,1),(100,1),(101,1),(102,1),(103,1),(104,1),(105,1),(106,1),(107,1),(108,1),(109,1),(110,1),(111,1),(112,1),(113,1),(114,1),(115,1),(116,1),(117,1),(118,1),(119,1),(120,1),(121,1),(122,1),(123,1),(124,1),(125,1),(126,1),(127,1),(128,1),(129,1),(130,1),(131,1),(132,1),(133,1),(134,1),(135,1),(136,1),(137,1),(138,1),(139,1),(140,1),(141,1),(142,1),(143,1),(144,1),(145,1),(146,1),(147,1),(148,1),(149,1),(150,1),(151,1),(152,1),(153,1),(154,1),(155,1),(156,1),(157,1),(158,1),(159,1),(160,1),(161,1),(162,1),(163,1),(164,1),(165,1),(166,1),(167,1),(168,1),(169,1),(170,1),(171,1),(172,1),(173,1),(174,1),(175,1),(176,1),(177,1),(178,1),(179,1),(180,1),(181,1),(182,1),(183,1),(184,1),(185,1),(186,1),(187,1),(188,1),(189,1),(190,1),(191,1),(192,1),(193,1),(194,1),(195,1),(196,1),(197,1),(198,1),(199,1),(200,1),(201,1),(202,1),(203,1),(204,1),(205,1),(206,1),(207,1),(208,1),(209,1),(210,1),(211,1),(212,1),(213,1),(214,1),(215,1),(216,1),(217,1),(218,1),(219,1),(220,1),(221,1),(222,1),(223,1),(224,1),(225,1),(226,1),(227,1),(228,1),(229,1),(230,1),(231,1),(232,1),(233,1),(234,1),(235,1),(236,1),(237,1),(238,1),(239,1),(240,1),(241,1),(242,1),(243,1),(244,1),(245,1),(246,1),(247,1),(248,1),(249,1),(250,1),(251,1),(252,1),(253,1),(254,1),(255,1),(256,1),(257,1),(258,1),(259,1),(260,1),(261,1),(262,1),(263,1),(264,1),(265,1),(266,1),(267,1),(268,1),(269,1),(270,1),(271,1),(272,1),(273,1),(274,1),(275,1),(276,1),(277,1),(278,1),(279,1),(280,1),(281,1),(282,1),(283,1),(284,1),(285,1),(286,1),(287,1),(288,1),(289,1),(290,1),(30,3),(31,3),(32,3),(77,3),(78,3),(79,3),(80,3),(81,3),(82,3),(83,3),(86,3),(88,3),(92,3),(96,3),(100,3),(104,3),(108,3),(112,3),(116,3),(145,3),(146,3),(147,3),(148,3),(150,3),(153,3),(154,3),(156,3),(157,3),(158,3),(162,3),(163,3),(166,3),(167,3),(168,3),(170,3),(174,3),(175,3),(176,3),(178,3),(179,3),(182,3),(186,3),(190,3),(191,3),(194,3),(195,3),(198,3),(199,3),(200,3),(202,3),(203,3),(204,3),(205,3),(206,3);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `is_system` tinyint NOT NULL DEFAULT '0',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,1,'admin','web','2021-02-25 17:09:27','2021-02-25 17:09:27'),(2,1,'client','web','2021-02-25 17:09:27','2021-02-25 17:09:27'),(3,0,'Loan Officer','web','2021-03-08 00:54:25','2021-03-08 00:54:25');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings`
--

DROP TABLE IF EXISTS `savings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `savings_officer_id` bigint unsigned DEFAULT NULL,
  `savings_product_id` bigint unsigned DEFAULT NULL,
  `client_type` enum('client','group') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'client',
  `client_id` bigint unsigned DEFAULT NULL,
  `group_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `external_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decimals` int DEFAULT NULL,
  `interest_rate` decimal(65,6) NOT NULL,
  `interest_rate_type` enum('day','week','month','year') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'year',
  `compounding_period` enum('daily','weekly','monthly','biannual','annually') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_posting_period_type` enum('daily','weekly','monthly','biannual','annually') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_calculation_type` enum('daily_balance','average_daily_balance') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'daily_balance',
  `balance_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_deposits_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_withdrawals_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `total_interest_posted_derived` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `minimum_balance_for_interest_calculation` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `lockin_period` int NOT NULL DEFAULT '0',
  `lockin_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'days',
  `automatic_opening_balance` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `allow_overdraft` tinyint NOT NULL DEFAULT '0',
  `overdraft_limit` decimal(65,6) DEFAULT NULL,
  `overdraft_interest_rate` decimal(65,6) DEFAULT NULL,
  `minimum_overdraft_for_interest` decimal(65,6) DEFAULT NULL,
  `submitted_on_date` date DEFAULT NULL,
  `submitted_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_on_date` date DEFAULT NULL,
  `approved_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `activated_on_date` date DEFAULT NULL,
  `activated_by_user_id` bigint unsigned DEFAULT NULL,
  `activated_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rejected_on_date` date DEFAULT NULL,
  `rejected_by_user_id` bigint unsigned DEFAULT NULL,
  `rejected_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `dormant_on_date` date DEFAULT NULL,
  `dormant_by_user_id` bigint unsigned DEFAULT NULL,
  `dormant_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `closed_on_date` date DEFAULT NULL,
  `closed_by_user_id` bigint unsigned DEFAULT NULL,
  `closed_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `inactive_on_date` date DEFAULT NULL,
  `inactive_by_user_id` bigint unsigned DEFAULT NULL,
  `inactive_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `withdrawn_on_date` date DEFAULT NULL,
  `withdrawn_by_user_id` bigint unsigned DEFAULT NULL,
  `withdrawn_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('pending','approved','active','withdrawn','rejected','closed','inactive','dormant','submitted') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'submitted',
  `start_interest_posting_date` date DEFAULT NULL,
  `next_interest_posting_date` date DEFAULT NULL,
  `start_interest_calculation_date` date DEFAULT NULL,
  `next_interest_calculation_date` date DEFAULT NULL,
  `last_interest_calculation_date` date DEFAULT NULL,
  `last_interest_posting_date` date DEFAULT NULL,
  `calculated_interest` decimal(65,6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `savings_client_id_index` (`client_id`) USING BTREE,
  KEY `savings_branch_id_index` (`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings`
--

LOCK TABLES `savings` WRITE;
/*!40000 ALTER TABLE `savings` DISABLE KEYS */;
/*!40000 ALTER TABLE `savings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_charge_options`
--

DROP TABLE IF EXISTS `savings_charge_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_charge_options` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_charge_options`
--

LOCK TABLES `savings_charge_options` WRITE;
/*!40000 ALTER TABLE `savings_charge_options` DISABLE KEYS */;
INSERT INTO `savings_charge_options` VALUES (1,'Flat','Flat',1),(2,'Percentage of amount','Percentage of amount',1),(3,'Percentage of savings balance','Percentage of savings balance',1);
/*!40000 ALTER TABLE `savings_charge_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_charge_types`
--

DROP TABLE IF EXISTS `savings_charge_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_charge_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_charge_types`
--

LOCK TABLES `savings_charge_types` WRITE;
/*!40000 ALTER TABLE `savings_charge_types` DISABLE KEYS */;
INSERT INTO `savings_charge_types` VALUES (1,'Savings Activation','Savings Activation',1),(2,'Specified Due Date','Specified Due Date',1),(3,'Withdrawal Fee','Withdrawal Fee',1),(4,'Annual Fee','Annual Fee',1),(5,'Monthly Fee','Monthly Fee',1),(6,'Inactivity Fee','Inactivity Fee',1),(7,'Quarterly Fee','Quarterly Fee',1);
/*!40000 ALTER TABLE `savings_charge_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_charges`
--

DROP TABLE IF EXISTS `savings_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `savings_charge_type_id` bigint unsigned NOT NULL,
  `savings_charge_option_id` bigint unsigned NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(65,6) NOT NULL,
  `min_amount` decimal(65,6) DEFAULT NULL,
  `max_amount` decimal(65,6) DEFAULT NULL,
  `payment_mode` enum('regular','account_transfer') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'regular',
  `schedule` tinyint NOT NULL DEFAULT '0',
  `schedule_frequency` int DEFAULT NULL,
  `schedule_frequency_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint NOT NULL DEFAULT '0',
  `allow_override` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `savings_charges_currency_id_foreign` (`currency_id`) USING BTREE,
  KEY `savings_charges_savings_charge_type_id_foreign` (`savings_charge_type_id`) USING BTREE,
  KEY `savings_charges_savings_charge_option_id_foreign` (`savings_charge_option_id`) USING BTREE,
  KEY `savings_charges_created_by_id_foreign` (`created_by_id`) USING BTREE,
  CONSTRAINT `savings_charges_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `savings_charges_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `savings_charges_savings_charge_option_id_foreign` FOREIGN KEY (`savings_charge_option_id`) REFERENCES `savings_charge_options` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `savings_charges_savings_charge_type_id_foreign` FOREIGN KEY (`savings_charge_type_id`) REFERENCES `savings_charge_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_charges`
--

LOCK TABLES `savings_charges` WRITE;
/*!40000 ALTER TABLE `savings_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `savings_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_linked_charges`
--

DROP TABLE IF EXISTS `savings_linked_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_linked_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `savings_id` bigint unsigned DEFAULT NULL,
  `savings_charge_id` bigint unsigned DEFAULT NULL,
  `savings_charge_type_id` bigint unsigned DEFAULT NULL,
  `savings_charge_option_id` bigint unsigned DEFAULT NULL,
  `savings_transaction_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` decimal(65,6) NOT NULL,
  `calculated_amount` decimal(65,6) DEFAULT NULL,
  `paid_amount` decimal(65,6) DEFAULT NULL,
  `active` tinyint NOT NULL DEFAULT '0',
  `waived` tinyint NOT NULL DEFAULT '0',
  `is_paid` tinyint NOT NULL DEFAULT '0',
  `submitted_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_linked_charges`
--

LOCK TABLES `savings_linked_charges` WRITE;
/*!40000 ALTER TABLE `savings_linked_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `savings_linked_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_product_linked_charges`
--

DROP TABLE IF EXISTS `savings_product_linked_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_product_linked_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `savings_product_id` bigint unsigned NOT NULL,
  `savings_charge_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_product_linked_charges`
--

LOCK TABLES `savings_product_linked_charges` WRITE;
/*!40000 ALTER TABLE `savings_product_linked_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `savings_product_linked_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_products`
--

DROP TABLE IF EXISTS `savings_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `savings_reference_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `overdraft_portfolio_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `savings_control_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `interest_on_savings_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `write_off_savings_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_fees_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_penalties_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_interest_overdraft_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `decimals` int DEFAULT NULL,
  `savings_category` enum('voluntary','compulsory') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `auto_create` tinyint NOT NULL DEFAULT '0',
  `minimum_interest_rate` decimal(65,6) NOT NULL,
  `default_interest_rate` decimal(65,6) NOT NULL,
  `maximum_interest_rate` decimal(65,6) NOT NULL,
  `interest_rate_type` enum('day','week','month','year') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'year',
  `compounding_period` enum('daily','weekly','monthly','biannual','annually') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_posting_period_type` enum('daily','weekly','monthly','biannual','annually') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_calculation_type` enum('daily_balance','average_daily_balance') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'daily_balance',
  `automatic_opening_balance` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `minimum_balance_for_interest_calculation` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `lockin_period` int NOT NULL DEFAULT '0',
  `lockin_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'days',
  `minimum_balance` decimal(65,6) NOT NULL DEFAULT '0.000000',
  `allow_overdraft` tinyint NOT NULL DEFAULT '0',
  `overdraft_limit` decimal(65,6) DEFAULT NULL,
  `overdraft_interest_rate` decimal(65,6) DEFAULT NULL,
  `minimum_overdraft_for_interest` decimal(65,6) DEFAULT NULL,
  `days_in_year` enum('actual','360','365','364') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '365',
  `days_in_month` enum('actual','30','31') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '30',
  `accounting_rule` enum('none','cash') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `active` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_products`
--

LOCK TABLES `savings_products` WRITE;
/*!40000 ALTER TABLE `savings_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `savings_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_transaction_types`
--

DROP TABLE IF EXISTS `savings_transaction_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_transaction_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_transaction_types`
--

LOCK TABLES `savings_transaction_types` WRITE;
/*!40000 ALTER TABLE `savings_transaction_types` DISABLE KEYS */;
INSERT INTO `savings_transaction_types` VALUES (1,'Deposit','Deposit',1),(2,'Withdrawal','Withdrawal',1),(3,'Dividend','Dividend',1),(4,'Waive Interest','Waive Interest',1),(5,'Guarantee','Guarantee',1),(6,'Guarantee Restored','Guarantee Restored',1),(7,'Loan Repayment','Loan Repayment',1),(8,'Transfer','Transfer',1),(9,'Waive Charges','Waive Charges',1),(10,'Apply Charges','Apply Charges',1),(11,'Apply Interest','Apply Interest',1),(12,'Pay Charge','Pay Charge',1);
/*!40000 ALTER TABLE `savings_transaction_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings_transactions`
--

DROP TABLE IF EXISTS `savings_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savings_transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `savings_id` bigint unsigned NOT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `savings_linked_charge_id` bigint unsigned DEFAULT NULL,
  `payment_detail_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` decimal(65,6) NOT NULL,
  `credit` decimal(65,6) DEFAULT NULL,
  `debit` decimal(65,6) DEFAULT NULL,
  `balance` decimal(65,6) DEFAULT NULL,
  `savings_transaction_type_id` bigint unsigned NOT NULL,
  `reversed` tinyint NOT NULL DEFAULT '0',
  `reversible` tinyint NOT NULL DEFAULT '0',
  `submitted_on` date DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `reference` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `online_transaction` tinyint NOT NULL DEFAULT '0',
  `status` enum('pending','approved','declined') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `savings_transactions_savings_id_index` (`savings_id`) USING BTREE,
  KEY `savings_transactions_branch_id_index` (`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings_transactions`
--

LOCK TABLES `savings_transactions` WRITE;
/*!40000 ALTER TABLE `savings_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `savings_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setting_key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setting_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `order` int DEFAULT NULL,
  `category` enum('email','sms','general','system','update','other') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('text','textarea','number','select','radio','date','select_db','radio_db','select_multiple','select_multiple_db','checkbox','checkbox_db','file','info') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `class` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `required` tinyint NOT NULL DEFAULT '0',
  `db_columns` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `displayed` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `settings_setting_key_unique` (`setting_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'Company Name','core.company_name','Core','Salene MIS',NULL,'general','text','','','',1,'',NULL,1,NULL,'2021-02-26 08:53:32'),(2,'Company Address','core.company_address','Core',NULL,NULL,'general','textarea','','','',0,'',NULL,1,NULL,'2021-02-26 08:53:32'),(3,'Company Country','core.company_country','Core','113',NULL,'general','select_db','countries','','select2',0,'id,name',NULL,1,NULL,'2021-02-26 08:54:17'),(4,'Timezone','core.timezone','Core','204',NULL,'general','select_db','timezones','','select2',1,'id,zone_name',NULL,1,NULL,'2021-02-26 08:53:32'),(5,'System Version','core.system_version','Core','3.0',NULL,'update','info','','','',1,'',NULL,1,NULL,NULL),(6,'Company Email','core.company_email','Core','info@salenecredit.co.ke',NULL,'general','text','','','',1,'',NULL,1,NULL,'2021-03-08 00:36:54'),(7,'Company Logo','core.company_logo','Core','',NULL,'general','file','jpeg,jpg,bmp,png','nullable,file|mimes:jpeg,jpg,bmp,png','',0,'',NULL,1,NULL,NULL),(8,'Site Online','core.site_online','Core','yes',NULL,'system','select','yes,no','','',1,'',NULL,1,NULL,NULL),(9,'Console Last Run','core.console_last_run','Core','',NULL,'system','info','','','',1,'',NULL,1,NULL,NULL),(10,'Update Url','core.update_url','Core','http://webstudio.co.zw/ulm/update',NULL,'general','info','','','',1,'',NULL,0,NULL,NULL),(11,'Auto Download Update','core.auto_download_update','Core','no',NULL,'system','select','yes,no','','',1,'',NULL,1,NULL,NULL),(12,'Update last checked','core.update_last_checked','Core','',NULL,'system','info','','','',1,'',NULL,1,NULL,NULL),(13,'Extra Javascript','core.extra_javascript','Core','',NULL,'system','textarea','','','',0,'',NULL,1,NULL,NULL),(14,'Extra Styles','core.extra_styles','Core','',NULL,'system','textarea','','','',0,'',NULL,1,NULL,NULL),(15,'Demo Mode','core.demo_mode','Core','no',NULL,'system','select','yes,no','','',1,'',NULL,1,NULL,NULL),(16,'Purchase Code','core.purchase_code','Core','',NULL,'system','text','','','',0,'',NULL,1,NULL,NULL),(17,'Active Theme','core.active_theme','Core','AdminLTE',NULL,'system','text','','','',0,'',NULL,0,NULL,NULL),(18,'Registration Enabled','user.enable_registration','User','no',NULL,'system','select','yes,no',NULL,'',1,'',NULL,1,NULL,NULL),(19,'Enable Google recaptcha','user.enable_google_recaptcha','User','no',NULL,'system','select','yes,no',NULL,'',1,'',NULL,1,NULL,NULL),(20,'Google recaptcha site key','user.google_recaptcha_site_key','User','',NULL,'system','text','',NULL,'',0,'',NULL,1,NULL,NULL),(21,'Google recaptcha secret key','user.google_recaptcha_secret_key','User','',NULL,'system','text','',NULL,'',0,'',NULL,1,NULL,NULL),(22,'Client Reference Prefix','client.reference_prefix','Client','CL',NULL,'system','text','','','',0,'',NULL,1,NULL,NULL),(23,'Client Reference Format','client.reference_format','Client','YEAR/Sequence Number (SL/2014/001)',NULL,'system','select','YEAR/Sequence Number (SL/2014/001),YEAR/MONTH/Sequence Number (SL/2014/08/001),Sequence Number,Random Number,Branch Sequence Number','','',1,'',NULL,1,NULL,NULL),(24,'Savings Reference Prefix','savings.reference_prefix','Savings','',NULL,'system','text','','','',0,'',NULL,1,NULL,NULL),(25,'Savings Reference Format','savings.reference_format','Savings','YEAR/Sequence Number (SL/2014/001)',NULL,'system','select','YEAR/Sequence Number (SL/2014/001),YEAR/MONTH/Sequence Number (SL/2014/08/001),Sequence Number,Random Number,Branch Product Sequence Number','','',1,'',NULL,1,NULL,NULL),(26,'Loan Reference Prefix','loan.reference_prefix','Loan','L',NULL,'system','text','','','',0,'',NULL,1,NULL,NULL),(27,'Loan Reference Format','loan.reference_format','Loan','YEAR/Sequence Number (SL/2014/001)',NULL,'system','select','YEAR/Sequence Number (SL/2014/001),YEAR/MONTH/Sequence Number (SL/2014/08/001),Sequence Number,Random Number,Branch Product Sequence Number','','',1,'',NULL,1,NULL,NULL),(28,'SMS Enabled','communication.sms_enabled','Communication','no',NULL,'sms','select','yes,no','','',1,'',NULL,1,NULL,NULL),(29,'Active SMS Gateway','communication.active_sms_gateway','Communication','1',NULL,'sms','select_db','sms_gateways','','select2',0,'id,name',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_charge_options`
--

DROP TABLE IF EXISTS `share_charge_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_charge_options` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_charge_options`
--

LOCK TABLES `share_charge_options` WRITE;
/*!40000 ALTER TABLE `share_charge_options` DISABLE KEYS */;
INSERT INTO `share_charge_options` VALUES (1,'Flat','Flat',1,NULL,NULL),(2,'Percentage of amount','Percentage of amount',1,NULL,NULL);
/*!40000 ALTER TABLE `share_charge_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_charge_types`
--

DROP TABLE IF EXISTS `share_charge_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_charge_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_charge_types`
--

LOCK TABLES `share_charge_types` WRITE;
/*!40000 ALTER TABLE `share_charge_types` DISABLE KEYS */;
INSERT INTO `share_charge_types` VALUES (1,'Share Account Activation','Share Account Activation',1,NULL,NULL),(2,'Share Purchase','Share Purchase',1,NULL,NULL),(3,'Share Redeem','Share Redeem',1,NULL,NULL);
/*!40000 ALTER TABLE `share_charge_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_charges`
--

DROP TABLE IF EXISTS `share_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `share_charge_type_id` bigint unsigned NOT NULL,
  `share_charge_option_id` bigint unsigned NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(65,6) NOT NULL,
  `min_amount` decimal(65,6) DEFAULT NULL,
  `max_amount` decimal(65,6) DEFAULT NULL,
  `active` tinyint NOT NULL DEFAULT '0',
  `allow_override` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_charges`
--

LOCK TABLES `share_charges` WRITE;
/*!40000 ALTER TABLE `share_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `share_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_linked_charges`
--

DROP TABLE IF EXISTS `share_linked_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_linked_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `share_id` bigint unsigned DEFAULT NULL,
  `share_charge_id` bigint unsigned DEFAULT NULL,
  `share_charge_type_id` bigint unsigned DEFAULT NULL,
  `share_charge_option_id` bigint unsigned DEFAULT NULL,
  `share_transaction_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` decimal(65,6) NOT NULL,
  `calculated_amount` decimal(65,6) DEFAULT NULL,
  `paid_amount` decimal(65,6) DEFAULT NULL,
  `active` tinyint NOT NULL DEFAULT '0',
  `waived` tinyint NOT NULL DEFAULT '0',
  `is_paid` tinyint NOT NULL DEFAULT '0',
  `submitted_on` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_linked_charges`
--

LOCK TABLES `share_linked_charges` WRITE;
/*!40000 ALTER TABLE `share_linked_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `share_linked_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_market_periods`
--

DROP TABLE IF EXISTS `share_market_periods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_market_periods` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `share_product_id` bigint unsigned DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `nominal_price` decimal(65,6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_market_periods`
--

LOCK TABLES `share_market_periods` WRITE;
/*!40000 ALTER TABLE `share_market_periods` DISABLE KEYS */;
/*!40000 ALTER TABLE `share_market_periods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_product_linked_charges`
--

DROP TABLE IF EXISTS `share_product_linked_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_product_linked_charges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `share_product_id` bigint unsigned NOT NULL,
  `share_charge_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_product_linked_charges`
--

LOCK TABLES `share_product_linked_charges` WRITE;
/*!40000 ALTER TABLE `share_product_linked_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `share_product_linked_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_products`
--

DROP TABLE IF EXISTS `share_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `share_reference_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `share_suspense_control_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `equity_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `income_from_fees_chart_of_account_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `decimals` int DEFAULT NULL,
  `total_shares` int DEFAULT NULL,
  `shares_to_be_issued` int DEFAULT NULL,
  `nominal_price` decimal(65,6) DEFAULT NULL,
  `capital_value` decimal(65,6) DEFAULT NULL,
  `minimum_shares_per_client` decimal(65,6) DEFAULT NULL,
  `default_shares_per_client` decimal(65,6) DEFAULT NULL,
  `maximum_shares_per_client` decimal(65,6) DEFAULT NULL,
  `minimum_active_period` int DEFAULT NULL,
  `minimum_active_period_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'days',
  `lockin_period` int NOT NULL DEFAULT '0',
  `lockin_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'days',
  `allow_dividends_for_inactive_clients` tinyint NOT NULL DEFAULT '0',
  `accounting_rule` enum('none','cash') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `active` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_products`
--

LOCK TABLES `share_products` WRITE;
/*!40000 ALTER TABLE `share_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `share_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_transaction_types`
--

DROP TABLE IF EXISTS `share_transaction_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_transaction_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `translated_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_transaction_types`
--

LOCK TABLES `share_transaction_types` WRITE;
/*!40000 ALTER TABLE `share_transaction_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `share_transaction_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_transactions`
--

DROP TABLE IF EXISTS `share_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `share_transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `share_id` bigint unsigned NOT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `share_linked_charge_id` bigint unsigned DEFAULT NULL,
  `payment_detail_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` decimal(65,6) NOT NULL,
  `total_shares` decimal(65,6) DEFAULT NULL,
  `charge_amount` decimal(65,6) DEFAULT NULL,
  `credit` decimal(65,6) DEFAULT NULL,
  `debit` decimal(65,6) DEFAULT NULL,
  `balance` decimal(65,6) DEFAULT NULL,
  `share_transaction_type_id` bigint unsigned NOT NULL,
  `reversed` tinyint NOT NULL DEFAULT '0',
  `reversible` tinyint NOT NULL DEFAULT '0',
  `submitted_on` date DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `reference` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `online_transaction` tinyint NOT NULL DEFAULT '0',
  `status` enum('pending','approved','declined') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `share_transactions_share_id_index` (`share_id`) USING BTREE,
  KEY `share_transactions_branch_id_index` (`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_transactions`
--

LOCK TABLES `share_transactions` WRITE;
/*!40000 ALTER TABLE `share_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `share_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shares`
--

DROP TABLE IF EXISTS `shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shares` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `currency_id` bigint unsigned NOT NULL,
  `share_officer_id` bigint unsigned DEFAULT NULL,
  `share_product_id` bigint unsigned DEFAULT NULL,
  `savings_id` bigint unsigned DEFAULT NULL,
  `client_type` enum('client','group') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'client',
  `client_id` bigint unsigned DEFAULT NULL,
  `group_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `external_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decimals` int DEFAULT NULL,
  `total_shares` int DEFAULT NULL,
  `nominal_price` decimal(65,6) DEFAULT NULL,
  `minimum_active_period` int DEFAULT NULL,
  `minimum_active_period_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'days',
  `lockin_period` int NOT NULL DEFAULT '0',
  `lockin_type` enum('days','weeks','months','years') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'days',
  `allow_dividends_for_inactive_clients` tinyint NOT NULL DEFAULT '0',
  `submitted_on_date` date DEFAULT NULL,
  `application_date` date DEFAULT NULL,
  `submitted_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_on_date` date DEFAULT NULL,
  `approved_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `activated_on_date` date DEFAULT NULL,
  `activated_by_user_id` bigint unsigned DEFAULT NULL,
  `activated_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rejected_on_date` date DEFAULT NULL,
  `rejected_by_user_id` bigint unsigned DEFAULT NULL,
  `rejected_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `dormant_on_date` date DEFAULT NULL,
  `dormant_by_user_id` bigint unsigned DEFAULT NULL,
  `dormant_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `closed_on_date` date DEFAULT NULL,
  `closed_by_user_id` bigint unsigned DEFAULT NULL,
  `closed_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `inactive_on_date` date DEFAULT NULL,
  `inactive_by_user_id` bigint unsigned DEFAULT NULL,
  `inactive_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `withdrawn_on_date` date DEFAULT NULL,
  `withdrawn_by_user_id` bigint unsigned DEFAULT NULL,
  `withdrawn_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('pending','approved','active','withdrawn','rejected','closed','inactive','dormant','submitted') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'submitted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `shares_client_id_index` (`client_id`) USING BTREE,
  KEY `shares_branch_id_index` (`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shares`
--

LOCK TABLES `shares` WRITE;
/*!40000 ALTER TABLE `shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `shares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_gateways`
--

DROP TABLE IF EXISTS `sms_gateways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sms_gateways` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `to_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `msg_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` tinyint NOT NULL DEFAULT '0',
  `is_current` tinyint NOT NULL DEFAULT '0',
  `http_api` tinyint NOT NULL DEFAULT '1',
  `class_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_one` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_one_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_two` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_two_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_three` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_three_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_four` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `key_four_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sms_gateways_created_by_id_foreign` (`created_by_id`) USING BTREE,
  CONSTRAINT `sms_gateways_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_gateways`
--

LOCK TABLES `sms_gateways` WRITE;
/*!40000 ALTER TABLE `sms_gateways` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_gateways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_rates`
--

DROP TABLE IF EXISTS `tax_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tax_rates` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `code` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `amount` decimal(65,2) DEFAULT NULL,
  `type` enum('fixed','percentage') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'percentage',
  `active` tinyint NOT NULL DEFAULT '1',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_rates`
--

LOCK TABLES `tax_rates` WRITE;
/*!40000 ALTER TABLE `tax_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telescope_entries`
--

DROP TABLE IF EXISTS `telescope_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `telescope_entries` (
  `sequence` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sequence`) USING BTREE,
  UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`) USING BTREE,
  KEY `telescope_entries_batch_id_index` (`batch_id`) USING BTREE,
  KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`) USING BTREE,
  KEY `telescope_entries_family_hash_index` (`family_hash`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telescope_entries`
--

LOCK TABLES `telescope_entries` WRITE;
/*!40000 ALTER TABLE `telescope_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `telescope_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telescope_entries_tags`
--

DROP TABLE IF EXISTS `telescope_entries_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `telescope_entries_tags` (
  `entry_uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`) USING BTREE,
  KEY `telescope_entries_tags_tag_index` (`tag`) USING BTREE,
  CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telescope_entries_tags`
--

LOCK TABLES `telescope_entries_tags` WRITE;
/*!40000 ALTER TABLE `telescope_entries_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `telescope_entries_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telescope_monitoring`
--

DROP TABLE IF EXISTS `telescope_monitoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `telescope_monitoring` (
  `tag` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telescope_monitoring`
--

LOCK TABLES `telescope_monitoring` WRITE;
/*!40000 ALTER TABLE `telescope_monitoring` DISABLE KEYS */;
/*!40000 ALTER TABLE `telescope_monitoring` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timezones`
--

DROP TABLE IF EXISTS `timezones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `timezones` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `zone_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=426 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timezones`
--

LOCK TABLES `timezones` WRITE;
/*!40000 ALTER TABLE `timezones` DISABLE KEYS */;
INSERT INTO `timezones` VALUES (1,'1','AD','Europe/Andorra'),(2,'2','AE','Asia/Dubai'),(3,'3','AF','Asia/Kabul'),(4,'4','AG','America/Antigua'),(5,'5','AI','America/Anguilla'),(6,'6','AL','Europe/Tirane'),(7,'7','AM','Asia/Yerevan'),(8,'8','AO','Africa/Luanda'),(9,'9','AQ','Antarctica/McMurdo'),(10,'10','AQ','Antarctica/Casey'),(11,'11','AQ','Antarctica/Davis'),(12,'12','AQ','Antarctica/DumontDUrville'),(13,'13','AQ','Antarctica/Mawson'),(14,'14','AQ','Antarctica/Palmer'),(15,'15','AQ','Antarctica/Rothera'),(16,'16','AQ','Antarctica/Syowa'),(17,'17','AQ','Antarctica/Troll'),(18,'18','AQ','Antarctica/Vostok'),(19,'19','AR','America/Argentina/Buenos_Aires'),(20,'20','AR','America/Argentina/Cordoba'),(21,'21','AR','America/Argentina/Salta'),(22,'22','AR','America/Argentina/Jujuy'),(23,'23','AR','America/Argentina/Tucuman'),(24,'24','AR','America/Argentina/Catamarca'),(25,'25','AR','America/Argentina/La_Rioja'),(26,'26','AR','America/Argentina/San_Juan'),(27,'27','AR','America/Argentina/Mendoza'),(28,'28','AR','America/Argentina/San_Luis'),(29,'29','AR','America/Argentina/Rio_Gallegos'),(30,'30','AR','America/Argentina/Ushuaia'),(31,'31','AS','Pacific/Pago_Pago'),(32,'32','AT','Europe/Vienna'),(33,'33','AU','Australia/Lord_Howe'),(34,'34','AU','Antarctica/Macquarie'),(35,'35','AU','Australia/Hobart'),(36,'36','AU','Australia/Currie'),(37,'37','AU','Australia/Melbourne'),(38,'38','AU','Australia/Sydney'),(39,'39','AU','Australia/Broken_Hill'),(40,'40','AU','Australia/Brisbane'),(41,'41','AU','Australia/Lindeman'),(42,'42','AU','Australia/Adelaide'),(43,'43','AU','Australia/Darwin'),(44,'44','AU','Australia/Perth'),(45,'45','AU','Australia/Eucla'),(46,'46','AW','America/Aruba'),(47,'47','AX','Europe/Mariehamn'),(48,'48','AZ','Asia/Baku'),(49,'49','BA','Europe/Sarajevo'),(50,'50','BB','America/Barbados'),(51,'51','BD','Asia/Dhaka'),(52,'52','BE','Europe/Brussels'),(53,'53','BF','Africa/Ouagadougou'),(54,'54','BG','Europe/Sofia'),(55,'55','BH','Asia/Bahrain'),(56,'56','BI','Africa/Bujumbura'),(57,'57','BJ','Africa/Porto-Novo'),(58,'58','BL','America/St_Barthelemy'),(59,'59','BM','Atlantic/Bermuda'),(60,'60','BN','Asia/Brunei'),(61,'61','BO','America/La_Paz'),(62,'62','BQ','America/Kralendijk'),(63,'63','BR','America/Noronha'),(64,'64','BR','America/Belem'),(65,'65','BR','America/Fortaleza'),(66,'66','BR','America/Recife'),(67,'67','BR','America/Araguaina'),(68,'68','BR','America/Maceio'),(69,'69','BR','America/Bahia'),(70,'70','BR','America/Sao_Paulo'),(71,'71','BR','America/Campo_Grande'),(72,'72','BR','America/Cuiaba'),(73,'73','BR','America/Santarem'),(74,'74','BR','America/Porto_Velho'),(75,'75','BR','America/Boa_Vista'),(76,'76','BR','America/Manaus'),(77,'77','BR','America/Eirunepe'),(78,'78','BR','America/Rio_Branco'),(79,'79','BS','America/Nassau'),(80,'80','BT','Asia/Thimphu'),(81,'81','BW','Africa/Gaborone'),(82,'82','BY','Europe/Minsk'),(83,'83','BZ','America/Belize'),(84,'84','CA','America/St_Johns'),(85,'85','CA','America/Halifax'),(86,'86','CA','America/Glace_Bay'),(87,'87','CA','America/Moncton'),(88,'88','CA','America/Goose_Bay'),(89,'89','CA','America/Blanc-Sablon'),(90,'90','CA','America/Toronto'),(91,'91','CA','America/Nipigon'),(92,'92','CA','America/Thunder_Bay'),(93,'93','CA','America/Iqaluit'),(94,'94','CA','America/Pangnirtung'),(95,'95','CA','America/Atikokan'),(96,'96','CA','America/Winnipeg'),(97,'97','CA','America/Rainy_River'),(98,'98','CA','America/Resolute'),(99,'99','CA','America/Rankin_Inlet'),(100,'100','CA','America/Regina'),(101,'101','CA','America/Swift_Current'),(102,'102','CA','America/Edmonton'),(103,'103','CA','America/Cambridge_Bay'),(104,'104','CA','America/Yellowknife'),(105,'105','CA','America/Inuvik'),(106,'106','CA','America/Creston'),(107,'107','CA','America/Dawson_Creek'),(108,'108','CA','America/Fort_Nelson'),(109,'109','CA','America/Vancouver'),(110,'110','CA','America/Whitehorse'),(111,'111','CA','America/Dawson'),(112,'112','CC','Indian/Cocos'),(113,'113','CD','Africa/Kinshasa'),(114,'114','CD','Africa/Lubumbashi'),(115,'115','CF','Africa/Bangui'),(116,'116','CG','Africa/Brazzaville'),(117,'117','CH','Europe/Zurich'),(118,'118','CI','Africa/Abidjan'),(119,'119','CK','Pacific/Rarotonga'),(120,'120','CL','America/Santiago'),(121,'121','CL','America/Punta_Arenas'),(122,'122','CL','Pacific/Easter'),(123,'123','CM','Africa/Douala'),(124,'124','CN','Asia/Shanghai'),(125,'125','CN','Asia/Urumqi'),(126,'126','CO','America/Bogota'),(127,'127','CR','America/Costa_Rica'),(128,'128','CU','America/Havana'),(129,'129','CV','Atlantic/Cape_Verde'),(130,'130','CW','America/Curacao'),(131,'131','CX','Indian/Christmas'),(132,'132','CY','Asia/Nicosia'),(133,'133','CY','Asia/Famagusta'),(134,'134','CZ','Europe/Prague'),(135,'135','DE','Europe/Berlin'),(136,'136','DE','Europe/Busingen'),(137,'137','DJ','Africa/Djibouti'),(138,'138','DK','Europe/Copenhagen'),(139,'139','DM','America/Dominica'),(140,'140','DO','America/Santo_Domingo'),(141,'141','DZ','Africa/Algiers'),(142,'142','EC','America/Guayaquil'),(143,'143','EC','Pacific/Galapagos'),(144,'144','EE','Europe/Tallinn'),(145,'145','EG','Africa/Cairo'),(146,'146','EH','Africa/El_Aaiun'),(147,'147','ER','Africa/Asmara'),(148,'148','ES','Europe/Madrid'),(149,'149','ES','Africa/Ceuta'),(150,'150','ES','Atlantic/Canary'),(151,'151','ET','Africa/Addis_Ababa'),(152,'152','FI','Europe/Helsinki'),(153,'153','FJ','Pacific/Fiji'),(154,'154','FK','Atlantic/Stanley'),(155,'155','FM','Pacific/Chuuk'),(156,'156','FM','Pacific/Pohnpei'),(157,'157','FM','Pacific/Kosrae'),(158,'158','FO','Atlantic/Faroe'),(159,'159','FR','Europe/Paris'),(160,'160','GA','Africa/Libreville'),(161,'161','GB','Europe/London'),(162,'162','GD','America/Grenada'),(163,'163','GE','Asia/Tbilisi'),(164,'164','GF','America/Cayenne'),(165,'165','GG','Europe/Guernsey'),(166,'166','GH','Africa/Accra'),(167,'167','GI','Europe/Gibraltar'),(168,'168','GL','America/Godthab'),(169,'169','GL','America/Danmarkshavn'),(170,'170','GL','America/Scoresbysund'),(171,'171','GL','America/Thule'),(172,'172','GM','Africa/Banjul'),(173,'173','GN','Africa/Conakry'),(174,'174','GP','America/Guadeloupe'),(175,'175','GQ','Africa/Malabo'),(176,'176','GR','Europe/Athens'),(177,'177','GS','Atlantic/South_Georgia'),(178,'178','GT','America/Guatemala'),(179,'179','GU','Pacific/Guam'),(180,'180','GW','Africa/Bissau'),(181,'181','GY','America/Guyana'),(182,'182','HK','Asia/Hong_Kong'),(183,'183','HN','America/Tegucigalpa'),(184,'184','HR','Europe/Zagreb'),(185,'185','HT','America/Port-au-Prince'),(186,'186','HU','Europe/Budapest'),(187,'187','ID','Asia/Jakarta'),(188,'188','ID','Asia/Pontianak'),(189,'189','ID','Asia/Makassar'),(190,'190','ID','Asia/Jayapura'),(191,'191','IE','Europe/Dublin'),(192,'192','IL','Asia/Jerusalem'),(193,'193','IM','Europe/Isle_of_Man'),(194,'194','IN','Asia/Kolkata'),(195,'195','IO','Indian/Chagos'),(196,'196','IQ','Asia/Baghdad'),(197,'197','IR','Asia/Tehran'),(198,'198','IS','Atlantic/Reykjavik'),(199,'199','IT','Europe/Rome'),(200,'200','JE','Europe/Jersey'),(201,'201','JM','America/Jamaica'),(202,'202','JO','Asia/Amman'),(203,'203','JP','Asia/Tokyo'),(204,'204','KE','Africa/Nairobi'),(205,'205','KG','Asia/Bishkek'),(206,'206','KH','Asia/Phnom_Penh'),(207,'207','KI','Pacific/Tarawa'),(208,'208','KI','Pacific/Enderbury'),(209,'209','KI','Pacific/Kiritimati'),(210,'210','KM','Indian/Comoro'),(211,'211','KN','America/St_Kitts'),(212,'212','KP','Asia/Pyongyang'),(213,'213','KR','Asia/Seoul'),(214,'214','KW','Asia/Kuwait'),(215,'215','KY','America/Cayman'),(216,'216','KZ','Asia/Almaty'),(217,'217','KZ','Asia/Qyzylorda'),(218,'218','KZ','Asia/Qostanay'),(219,'219','KZ','Asia/Aqtobe'),(220,'220','KZ','Asia/Aqtau'),(221,'221','KZ','Asia/Atyrau'),(222,'222','KZ','Asia/Oral'),(223,'223','LA','Asia/Vientiane'),(224,'224','LB','Asia/Beirut'),(225,'225','LC','America/St_Lucia'),(226,'226','LI','Europe/Vaduz'),(227,'227','LK','Asia/Colombo'),(228,'228','LR','Africa/Monrovia'),(229,'229','LS','Africa/Maseru'),(230,'230','LT','Europe/Vilnius'),(231,'231','LU','Europe/Luxembourg'),(232,'232','LV','Europe/Riga'),(233,'233','LY','Africa/Tripoli'),(234,'234','MA','Africa/Casablanca'),(235,'235','MC','Europe/Monaco'),(236,'236','MD','Europe/Chisinau'),(237,'237','ME','Europe/Podgorica'),(238,'238','MF','America/Marigot'),(239,'239','MG','Indian/Antananarivo'),(240,'240','MH','Pacific/Majuro'),(241,'241','MH','Pacific/Kwajalein'),(242,'242','MK','Europe/Skopje'),(243,'243','ML','Africa/Bamako'),(244,'244','MM','Asia/Yangon'),(245,'245','MN','Asia/Ulaanbaatar'),(246,'246','MN','Asia/Hovd'),(247,'247','MN','Asia/Choibalsan'),(248,'248','MO','Asia/Macau'),(249,'249','MP','Pacific/Saipan'),(250,'250','MQ','America/Martinique'),(251,'251','MR','Africa/Nouakchott'),(252,'252','MS','America/Montserrat'),(253,'253','MT','Europe/Malta'),(254,'254','MU','Indian/Mauritius'),(255,'255','MV','Indian/Maldives'),(256,'256','MW','Africa/Blantyre'),(257,'257','MX','America/Mexico_City'),(258,'258','MX','America/Cancun'),(259,'259','MX','America/Merida'),(260,'260','MX','America/Monterrey'),(261,'261','MX','America/Matamoros'),(262,'262','MX','America/Mazatlan'),(263,'263','MX','America/Chihuahua'),(264,'264','MX','America/Ojinaga'),(265,'265','MX','America/Hermosillo'),(266,'266','MX','America/Tijuana'),(267,'267','MX','America/Bahia_Banderas'),(268,'268','MY','Asia/Kuala_Lumpur'),(269,'269','MY','Asia/Kuching'),(270,'270','MZ','Africa/Maputo'),(271,'271','NA','Africa/Windhoek'),(272,'272','NC','Pacific/Noumea'),(273,'273','NE','Africa/Niamey'),(274,'274','NF','Pacific/Norfolk'),(275,'275','NG','Africa/Lagos'),(276,'276','NI','America/Managua'),(277,'277','NL','Europe/Amsterdam'),(278,'278','NO','Europe/Oslo'),(279,'279','NP','Asia/Kathmandu'),(280,'280','NR','Pacific/Nauru'),(281,'281','NU','Pacific/Niue'),(282,'282','NZ','Pacific/Auckland'),(283,'283','NZ','Pacific/Chatham'),(284,'284','OM','Asia/Muscat'),(285,'285','PA','America/Panama'),(286,'286','PE','America/Lima'),(287,'287','PF','Pacific/Tahiti'),(288,'288','PF','Pacific/Marquesas'),(289,'289','PF','Pacific/Gambier'),(290,'290','PG','Pacific/Port_Moresby'),(291,'291','PG','Pacific/Bougainville'),(292,'292','PH','Asia/Manila'),(293,'293','PK','Asia/Karachi'),(294,'294','PL','Europe/Warsaw'),(295,'295','PM','America/Miquelon'),(296,'296','PN','Pacific/Pitcairn'),(297,'297','PR','America/Puerto_Rico'),(298,'298','PS','Asia/Gaza'),(299,'299','PS','Asia/Hebron'),(300,'300','PT','Europe/Lisbon'),(301,'301','PT','Atlantic/Madeira'),(302,'302','PT','Atlantic/Azores'),(303,'303','PW','Pacific/Palau'),(304,'304','PY','America/Asuncion'),(305,'305','QA','Asia/Qatar'),(306,'306','RE','Indian/Reunion'),(307,'307','RO','Europe/Bucharest'),(308,'308','RS','Europe/Belgrade'),(309,'309','RU','Europe/Kaliningrad'),(310,'310','RU','Europe/Moscow'),(311,'311','UA','Europe/Simferopol'),(312,'312','RU','Europe/Kirov'),(313,'313','RU','Europe/Astrakhan'),(314,'314','RU','Europe/Volgograd'),(315,'315','RU','Europe/Saratov'),(316,'316','RU','Europe/Ulyanovsk'),(317,'317','RU','Europe/Samara'),(318,'318','RU','Asia/Yekaterinburg'),(319,'319','RU','Asia/Omsk'),(320,'320','RU','Asia/Novosibirsk'),(321,'321','RU','Asia/Barnaul'),(322,'322','RU','Asia/Tomsk'),(323,'323','RU','Asia/Novokuznetsk'),(324,'324','RU','Asia/Krasnoyarsk'),(325,'325','RU','Asia/Irkutsk'),(326,'326','RU','Asia/Chita'),(327,'327','RU','Asia/Yakutsk'),(328,'328','RU','Asia/Khandyga'),(329,'329','RU','Asia/Vladivostok'),(330,'330','RU','Asia/Ust-Nera'),(331,'331','RU','Asia/Magadan'),(332,'332','RU','Asia/Sakhalin'),(333,'333','RU','Asia/Srednekolymsk'),(334,'334','RU','Asia/Kamchatka'),(335,'335','RU','Asia/Anadyr'),(336,'336','RW','Africa/Kigali'),(337,'337','SA','Asia/Riyadh'),(338,'338','SB','Pacific/Guadalcanal'),(339,'339','SC','Indian/Mahe'),(340,'340','SD','Africa/Khartoum'),(341,'341','SE','Europe/Stockholm'),(342,'342','SG','Asia/Singapore'),(343,'343','SH','Atlantic/St_Helena'),(344,'344','SI','Europe/Ljubljana'),(345,'345','SJ','Arctic/Longyearbyen'),(346,'346','SK','Europe/Bratislava'),(347,'347','SL','Africa/Freetown'),(348,'348','SM','Europe/San_Marino'),(349,'349','SN','Africa/Dakar'),(350,'350','SO','Africa/Mogadishu'),(351,'351','SR','America/Paramaribo'),(352,'352','SS','Africa/Juba'),(353,'353','ST','Africa/Sao_Tome'),(354,'354','SV','America/El_Salvador'),(355,'355','SX','America/Lower_Princes'),(356,'356','SY','Asia/Damascus'),(357,'357','SZ','Africa/Mbabane'),(358,'358','TC','America/Grand_Turk'),(359,'359','TD','Africa/Ndjamena'),(360,'360','TF','Indian/Kerguelen'),(361,'361','TG','Africa/Lome'),(362,'362','TH','Asia/Bangkok'),(363,'363','TJ','Asia/Dushanbe'),(364,'364','TK','Pacific/Fakaofo'),(365,'365','TL','Asia/Dili'),(366,'366','TM','Asia/Ashgabat'),(367,'367','TN','Africa/Tunis'),(368,'368','TO','Pacific/Tongatapu'),(369,'369','TR','Europe/Istanbul'),(370,'370','TT','America/Port_of_Spain'),(371,'371','TV','Pacific/Funafuti'),(372,'372','TW','Asia/Taipei'),(373,'373','TZ','Africa/Dar_es_Salaam'),(374,'374','UA','Europe/Kiev'),(375,'375','UA','Europe/Uzhgorod'),(376,'376','UA','Europe/Zaporozhye'),(377,'377','UG','Africa/Kampala'),(378,'378','UM','Pacific/Midway'),(379,'379','UM','Pacific/Wake'),(380,'380','US','America/New_York'),(381,'381','US','America/Detroit'),(382,'382','US','America/Kentucky/Louisville'),(383,'383','US','America/Kentucky/Monticello'),(384,'384','US','America/Indiana/Indianapolis'),(385,'385','US','America/Indiana/Vincennes'),(386,'386','US','America/Indiana/Winamac'),(387,'387','US','America/Indiana/Marengo'),(388,'388','US','America/Indiana/Petersburg'),(389,'389','US','America/Indiana/Vevay'),(390,'390','US','America/Chicago'),(391,'391','US','America/Indiana/Tell_City'),(392,'392','US','America/Indiana/Knox'),(393,'393','US','America/Menominee'),(394,'394','US','America/North_Dakota/Center'),(395,'395','US','America/North_Dakota/New_Salem'),(396,'396','US','America/North_Dakota/Beulah'),(397,'397','US','America/Denver'),(398,'398','US','America/Boise'),(399,'399','US','America/Phoenix'),(400,'400','US','America/Los_Angeles'),(401,'401','US','America/Anchorage'),(402,'402','US','America/Juneau'),(403,'403','US','America/Sitka'),(404,'404','US','America/Metlakatla'),(405,'405','US','America/Yakutat'),(406,'406','US','America/Nome'),(407,'407','US','America/Adak'),(408,'408','US','Pacific/Honolulu'),(409,'409','UY','America/Montevideo'),(410,'410','UZ','Asia/Samarkand'),(411,'411','UZ','Asia/Tashkent'),(412,'412','VA','Europe/Vatican'),(413,'413','VC','America/St_Vincent'),(414,'414','VE','America/Caracas'),(415,'415','VG','America/Tortola'),(416,'416','VI','America/St_Thomas'),(417,'417','VN','Asia/Ho_Chi_Minh'),(418,'418','VU','Pacific/Efate'),(419,'419','WF','Pacific/Wallis'),(420,'420','WS','Pacific/Apia'),(421,'421','YE','Asia/Aden'),(422,'422','YT','Indian/Mayotte'),(423,'423','ZA','Africa/Johannesburg'),(424,'424','ZM','Africa/Lusaka'),(425,'425','ZW','Africa/Harare');
/*!40000 ALTER TABLE `timezones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `titles`
--

DROP TABLE IF EXISTS `titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `titles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `titles`
--

LOCK TABLES `titles` WRITE;
/*!40000 ALTER TABLE `titles` DISABLE KEYS */;
INSERT INTO `titles` VALUES (1,'MR'),(2,'MISS'),(3,'MRS'),(4,'DOC'),(5,'PROF');
/*!40000 ALTER TABLE `titles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `created_by_id` int DEFAULT NULL,
  `branch_id` int DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_google2fa` tinyint NOT NULL DEFAULT '0',
  `google2fa_secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `otp` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_expiry_date` timestamp NULL DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `photo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  UNIQUE KEY `users_api_token_unique` (`api_token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,1,'Admin','admin','admin@salenecredit.co.ke','2021-02-25 17:09:27','$2y$10$dsiDC8zlxYX8gKtrNBQuLORhvqGOFd1rCl6Xm1uLt7TDWKWkPNgcO','6uupyw99mvTqTLdBnqmYPZHVqtiPORmwaqaaLYUicJ6m48byZlQTyhR5Au2F',NULL,'Admin','Admin',NULL,NULL,NULL,'male',0,'2BYQ5DC6UYNH7EXW',NULL,NULL,NULL,NULL,NULL,'2021-02-25 17:09:27','2021-05-10 13:00:22'),(2,NULL,NULL,'',NULL,'mercy.kirimi@salenecredit.co.ke','2021-02-26 08:56:52','$2y$10$yu/cZWFOwT1pLGab717rYuoyUqjTgbJQ8hZUtAjpShYBnkOW6V5kq',NULL,NULL,'MERCY','KIRIMI','0722689020',NULL,NULL,'female',0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-26 08:56:52','2021-03-08 14:37:59'),(4,NULL,NULL,'',NULL,'bmitabari@gmail.com','2021-03-08 14:25:24','$2y$10$esTpvjiOq57l/35MlLVNi.GxbLjcqgSNb4qDk216nYp/io1rUj1eK',NULL,NULL,'BONIFACE','ITABARI','0711320188',NULL,NULL,'male',0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 14:25:24','2021-03-08 14:25:24'),(5,NULL,NULL,'',NULL,'bernardwanjiku@gmail.com','2021-03-08 14:27:59','$2y$10$J/5MLh5QCRM44wgPhrCjuOk/5NhNqA.7yRxjlXj7nxok8/ZhVZ4DO',NULL,NULL,'BERNARD','GITAU','0729636499',NULL,NULL,'male',0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 14:27:59','2021-03-08 14:27:59'),(6,NULL,NULL,'',NULL,'alexpiusgori@salenecredit.co.ke','2021-03-08 14:32:40','$2y$10$vZZYvtoOiqn/p6H5jtjxLuwamyVTgnA1cLC/Ea478XHotRSr2cF06',NULL,NULL,'PIUS','GORI','0791251966',NULL,NULL,'male',0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 14:32:40','2021-03-08 14:32:40'),(7,NULL,NULL,'',NULL,'samuelmachariajubilee@gmail.com','2021-03-08 14:35:36','$2y$10$nHxe3A4bV5BT4dvQrBTutuIeiFtgHs/HYpfubEcHrMBeiDtXeuoH2',NULL,NULL,'SAMUEL','MACHARIA','0700062050',NULL,NULL,'male',0,NULL,NULL,NULL,NULL,NULL,NULL,'2021-03-08 14:35:36','2021-03-08 14:35:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallet_transactions`
--

DROP TABLE IF EXISTS `wallet_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wallet_transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `wallet_id` bigint unsigned NOT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `payment_detail_id` bigint unsigned DEFAULT NULL,
  `transaction_type` enum('deposit','withdrawal','savings_transfer','loan_transfer') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'deposit',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(65,6) NOT NULL,
  `credit` decimal(65,6) DEFAULT NULL,
  `debit` decimal(65,6) DEFAULT NULL,
  `balance` decimal(65,6) DEFAULT NULL,
  `reversed` tinyint NOT NULL DEFAULT '0',
  `reversible` tinyint NOT NULL DEFAULT '0',
  `submitted_on` date DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `reference` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `online_transaction` tinyint NOT NULL DEFAULT '0',
  `status` enum('pending','approved','declined') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wallet_transactions_wallet_id_index` (`wallet_id`) USING BTREE,
  KEY `wallet_transactions_branch_id_index` (`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallet_transactions`
--

LOCK TABLES `wallet_transactions` WRITE;
/*!40000 ALTER TABLE `wallet_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wallet_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallets`
--

DROP TABLE IF EXISTS `wallets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wallets` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `currency_id` bigint unsigned NOT NULL,
  `created_by_id` bigint unsigned DEFAULT NULL,
  `client_type` enum('client','group') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'client',
  `client_id` bigint unsigned DEFAULT NULL,
  `group_id` bigint unsigned DEFAULT NULL,
  `branch_id` bigint unsigned DEFAULT NULL,
  `status` enum('pending','active','inactive','closed','suspended','rejected','approved') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `balance` decimal(65,6) DEFAULT NULL,
  `decimals` int NOT NULL DEFAULT '2',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `submitted_on_date` date DEFAULT NULL,
  `submitted_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_on_date` date DEFAULT NULL,
  `approved_by_user_id` bigint unsigned DEFAULT NULL,
  `approved_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rejected_on_date` date DEFAULT NULL,
  `rejected_by_user_id` bigint unsigned DEFAULT NULL,
  `rejected_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `closed_on_date` date DEFAULT NULL,
  `closed_by_user_id` bigint unsigned DEFAULT NULL,
  `closed_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `activated_on_date` date DEFAULT NULL,
  `activated_by_user_id` bigint unsigned DEFAULT NULL,
  `activated_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `suspended_on_date` date DEFAULT NULL,
  `suspended_by_user_id` bigint unsigned DEFAULT NULL,
  `suspended_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `inactive_on_date` date DEFAULT NULL,
  `inactive_by_user_id` bigint unsigned DEFAULT NULL,
  `inactive_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `wallets_client_id_index` (`client_id`) USING BTREE,
  KEY `wallets_branch_id_index` (`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallets`
--

LOCK TABLES `wallets` WRITE;
/*!40000 ALTER TABLE `wallets` DISABLE KEYS */;
/*!40000 ALTER TABLE `wallets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `widgets` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `widgets` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `widgets_user_id_foreign` (`user_id`) USING BTREE,
  CONSTRAINT `widgets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,1,'{\"LoanStatistics\":{\"class\":\"Loan::LoanStatistics\",\"name\":\"Loan Statistics\",\"x\":0,\"y\":0,\"width\":12,\"height\":2},\"LoanStatusOverview\":{\"class\":\"Loan::LoanStatusOverview\",\"name\":\"Loan Status Overview\",\"x\":0,\"y\":2,\"width\":4,\"height\":4},\"LoanCollectionOverview\":{\"class\":\"Loan::LoanCollectionOverview\",\"name\":\"Loan Collection Overview\",\"x\":4,\"y\":2,\"width\":8,\"height\":6}}','2021-03-08 00:16:58','2021-03-08 00:17:19');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-10 15:31:40
